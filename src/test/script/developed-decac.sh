#! /bin/sh

# Test de l'interface en ligne de commande de decac.
# On ne met ici qu'un test trivial, a vous d'en ecrire de meilleurs.

cd "$(dirname "$0")"/../../..  || exit 1
PATH=./src/main/bin:"$PATH"

decac_moins_b=$(decac -b)

if [ "$?" -ne 0 ]; then
    echo "ERREUR: decac -b a termine avec un status different de zero."
    exit 1
fi

if [ "$decac_moins_b" = "" ]; then
    echo "ERREUR: decac -b n'a produit aucune sortie"
    exit 1
fi

if echo "$decac_moins_b" | grep -i -e "erreur" -e "error"; then
    echo "ERREUR: La sortie de decac -b contient erreur ou error"
    exit 1
fi

if echo "$decac_moins_b" | grep -i -e "Equipe gl41"; then
    echo "Pas de probleme detecte avec decac -b."
fi

# ... et ainsi de suite.

folder=src/test/deca/codegen/invalid/developed/noObject

decac_p_v=$(decac -p -v)
decac_d=$(decac -d)
d=$(decac)
decac_P=$(decac -P $folder/divEntierParZero.deca $folder/divFloatParZero.deca)
decac_file=$(decac $folder/divEntierParZero.deca)
decac_v=$(decac -v $folder/divEntierParZero.deca)
decac_r=$(decac -r 10 $folder/divEntierParZero.deca)
# decac_ov=$(decac StackOverflowParser.deca)
