#!/usr/bin/env python3
from codegen import doTest

if __name__ == '__main__':
    result_list = []
    doTest(True, "noObject", result_list)
    doTest(False, "noObject", result_list)
    doTest(True, "Object", result_list)
    doTest(False, "Object", result_list)
    print("\n\n")
    if len(result_list) != 0:
        print("Les tests qui ne sont pas passes sont:\n")
        for x in result_list:
            print(x)
            print("\n")
        exit(1)
