#!/usr/bin/env python3
# coding: utf8

import json
import os
import subprocess


def _setEnv():
    """
    Change directory to project root folder and
    set environment variable for decac and tests launchers
    """
    root_project_dirname = os.path.dirname(os.path.abspath(__file__)) + "/../../.."
    os.chdir(root_project_dirname)
    decac_dirname = os.getcwd() + "/src/main/bin"
    launchers_dirname = os.getcwd() + "/src/test/script/launchers"
    os.environ['PATH'] += ':' + decac_dirname
    os.environ['PATH'] += ':' + launchers_dirname


def doTest(valide, isObject, result_list):
    _setEnv()
    valide = "valid" if valide else "invalid"
    dirname = os.path.dirname(
        os.path.abspath("./src/test/deca/codegen/{}/developed/{}/expectedOutputs.json".format(valide, isObject)))
    with open("./src/test/deca/codegen/{}/developed/{}/expectedOutputs.json".format(valide, isObject), encoding='utf-8') as f:
        data = json.load(f)

    nb_tests = len(data)
    nb_tests_succeeded = 0
    print("************************** {} {} TESTS **************************".format(valide.upper(), isObject.upper()))
    for file in data:
        print('\n')
        filename = file['filename']
        deca_file = dirname + "/" + filename + ".deca"
        ass_file = dirname + "/" + filename + ".ass"

        if os.path.exists(ass_file):
            os.remove(ass_file)

        subprocess.run(["decac", deca_file])

        if not os.path.exists(ass_file):
            print("Fichier " + ass_file + " non genere.")
            print('\033[31mTEST {} FAILED\033[00m'.format(filename))
            continue

        result = subprocess.run(['ima', ass_file], stdout=subprocess.PIPE)
        os.remove(ass_file)

        print("Result: \"" + result.stdout.decode("utf-8") + "\"")
        print("Expected: \"" + file['expected'] + "\"")
        if result.stdout.decode("utf-8") == file['expected']:
            nb_tests_succeeded += 1
            print('\033[32mTEST {} SUCCESS\033[00m'.format(filename))
        else:
            print('\033[31mTEST {} FAILED\033[00m'.format(filename))
            result_list.append(deca_file)
    print("\nNumber of tests succeeded : " + str(nb_tests_succeeded) + "/" + str(nb_tests))
