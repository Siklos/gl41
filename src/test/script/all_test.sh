#!/bin/bash

# Auteur : gl41
# Version initiale : 01/01/2020



cd "$(dirname "$0")"/../../.. || exit 1
PATH=./src/test/script/launchers:"$PATH"

if [ -e results.tmp ]
then
  rm results.tmp
fi
touch results.tmp
./src/test/script/developed-syntax-lexer.sh
./src/test/script/developed-syntax-parser.sh
./src/test/script/developed-syntax-parser-object.sh
./src/test/script/developed-context.sh
./src/test/script/developed-context-object.sh
./src/test/script/developed-code-parse.sh

echo "Les tests qui ont échoué sont :"
failed=0
while read filename
do
  failed=1
  echo "$filename"
done < results.tmp

rm results.tmp

exit $failed