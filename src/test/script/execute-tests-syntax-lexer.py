#!/usr/bin/env python3

import json
import sys
import os
import subprocess

def setEnv():
    """
    Change directory to project root folder and
    set environment variable for decac and tests launchers
    """
    root_project_dirname = os.path.dirname(os.path.abspath(__file__)) + "/../../.."
    os.chdir(root_project_dirname)
    decac_dirname = os.getcwd() + "/src/main/bin"
    launchers_dirname = os.getcwd() + "/src/test/script/launchers"
    os.environ['PATH'] += ':' + decac_dirname
    os.environ['PATH'] += ':' + launchers_dirname

def main():
    setEnv()

    user_is_object_test = input("Voulez-vous tester pour les objets ? entrez : [N/y]")
    user_is_valid_test = input("Voulez vous tester les cas valides  ? entrez : [Y/n]")
    is_object_test = False
    is_valid_test = True

    if user_is_object_test.lower() == "y":
        is_object_test = True
    elif len(user_is_object_test) == 0 or user_is_object_test.lower() == "n":
        is_object_test = False
    else:
        print("Invalid option")
        exit(1)

    if len(user_is_valid_test) == 0 or user_is_valid_test.lower() == "y":
        is_valid_test = True
    elif user_is_valid_test.lower() == "n":
        is_valid_test = False
    else:
        print("Invalid option")
        exit(1)


    if is_object_test:
        object_folder = "Object"
    else:
        object_folder = "noObject"

    if is_valid_test:
        valid = "valid"
    else:
        valid = "invalid"

    directory = "src/test/deca/syntax/{}/developed/{}/Lexer".format(valid, object_folder)
    os.chdir(directory)
    files = os.listdir(os.getcwd())
    nbTests = len(list(filter(lambda x: x.endswith(".deca"), files)))
    nbTestsSucceeded = 0

    invalid_tests = []

    for filename in os.listdir(os.getcwd()):
        if filename.endswith(".deca"):
            print('\n')
            FNULL = open(os.devnull, 'w')
            code = subprocess.call(["test_lex", filename], stdout=FNULL)

            if (code == 0):
                if is_valid_test:
                    nbTestsSucceeded += 1
                    print('\033[32mTEST {} SUCCESS\033[30m'.format(filename))
                else:
                    invalid_tests.append(filename)
                    print('\033[31mTEST {} FAILED\033[00m'.format(filename))
            else:
                if not is_valid_test:
                    nbTestsSucceeded += 1
                    print('\033[32mTEST {} SUCCESS\033[30m'.format(filename))
                else:
                    invalid_tests.append(filename)
                    print('\033[31mTEST {} FAILED\033[00m'.format(filename))

    print("\nNumber of tests succeeded : " + str(nbTestsSucceeded) + "/" + str(nbTests))

    if (invalid_tests):
        print("Les tests qui ont échoué sont : ")
        for file in invalid_tests:
            print(file)



main()