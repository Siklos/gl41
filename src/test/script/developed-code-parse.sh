#!/bin/bash

# Auteur : gl41
# Version initiale : 01/01/2020

# Test minimaliste de la vérification contextuelle.
# Le principe et les limitations sont les mêmes que pour basic-synt.sh
cd "$(dirname "$0")"/../../.. || exit 1
compteurV=0
compteurVT=0
PATH=./src/test/script/launchers:./src/main/bin:"$PATH"


echo "************************************* Tests Code Parse Valid *************************************"

for filename in src/test/deca/context/valid/developed/noObject/*.deca
do
  ((compteurVT++))
  decac -p "$filename" > "$filename".tmp.deca || echo -e "\033[1;31m ERREUR : \033[0m Échec de la compilation d'un programme Deca simple avec -p" | exit 1
  if decac -p "$filename".tmp.deca 2>&1 | \
    grep -q -e "$filename"
  then
    echo -e "\033[31m Echec inattendu pour code parse \033[30m" $filename
    echo "$filename" >> results.tmp
  else
    echo -e "\033[32m Succes attendu de code parse \033[00m" $filename
    ((compteurV++))
  fi
  rm "$filename".tmp.deca
done

echo "************************************* Tests Code Object Parse Valid *************************************"

for filename in src/test/deca/context/valid/developed/Object/*.deca
do
  ((compteurVT++))
  decac -p "$filename" > "$filename".tmp.deca || echo -e "\033[1;31m ERREUR : \033[0m Échec de la compilation d'un programme Deca simple avec -p" | exit 1
  if decac -p "$filename".tmp.deca 2>&1 | \
    grep -q -e "$filename"
  then
    echo -e "\033[31m Echec inattendu pour code parse \033[30m" $filename
    echo "$filename" >> results.tmp
  else
    echo -e "\033[32m Succes attendu de code parse \033[00m" $filename
    ((compteurV++))
  fi
  rm "$filename".tmp.deca
done


