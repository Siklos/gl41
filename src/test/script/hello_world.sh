#! /bin/sh

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

# On ne teste qu'un fichier. Avec une boucle for appropriée, on
# pourrait faire bien mieux ...
rm -f ./src/test/deca/codegen/valid/developed/hello_world.ass 2>/dev/null
decac ./src/test/deca/codegen/valid/developed/hello_world.deca || exit 1

if [ ! -f ./src/test/deca/codegen/valid/developed/hello_world.ass ]; then
    echo "Fichier hello.ass non généré."
    exit 1
fi

resultat=$(ima ./src/test/deca/codegen/valid/developed/hello_world.ass) || exit 1
# rm -f ./src/test/deca/codegen/valid/developed/hello_world.ass

# On code en dur la valeur attendue.
attendu="Hello, world!"

if [ "$resultat" = "$attendu" ]; then
    echo "Tout va bien"
else
    echo "Résultat inattendu de ima:"
    echo "Resultat : $resultat vs. Attendu: $attendu"
    exit 1
fi

