#!/bin/bash

# Auteur : gl41
# Version initiale : 01/01/2020

# Test minimaliste de la vérification contextuelle.
# Le principe et les limitations sont les mêmes que pour basic-synt.sh
cd "$(dirname "$0")"/../../.. || exit 1
PATH=./src/test/script/launchers:"$PATH"

# Affiche le message en vert si c'est le résultat attendu. En rouge, sinon.
# La première branche correspond à l'echec attendu ou non du test.
# La seconde branche correspond au succès attendu ou non du test.

invalide=("" "in")

for i in 0 1
do
  echo "************************************* Tests parser ${invalide[(($i))]}valid objet *************************************"
  compteurTotal=0
  compteurValide=0
  for filename in src/test/deca/syntax/${invalide[$i]}valid/developed/Object/Parser/*.deca
  do
    ((compteurTotal++))
    if test_synt $filename 2>&1 | \
      grep -q -e $filename
    then
        echo -e "\033["$((31+$i))"m Echec ${invalide[((1-$i))]}attendu pour test_parser \033[00m" $filename
        out=("parser: $filename\n" "")
    else
        echo -e "\033["$((32-$i))"m Succes ${invalide[$i]}attendu de test_parser \033[00m" $filename
        ((compteurValide++))
        out=("" "parser: $filename\n")
    fi
    echo -n -e "${out[$i]}" >> results.tmp
  done
echo "Nombre de tests réussits est "$((compteurValide + $i * ($compteurTotal - $compteurValide))) "/" $compteurTotal
done