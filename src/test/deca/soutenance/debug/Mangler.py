#! /usr/bin/env python3

"""
This script takes in a deca file (any file really) and mangles it.
"""

import random, sys

def main():
    if len(sys.argv) < 2:
        raise ValueError("Please provide exactly one argument")

    filename = sys.argv[1]
        
    with open(filename, "r") as old:
        lines = [line for line in old.readlines()]
        old.close()

    with open(filename, "w") as new:
        for line in lines:
            if random.random() > 0.8:
                index = random.randint(0, len(line) - 2) # exclude line return
                newLine = line[:index] + line[index+1:]
            else:
                newLine = line
            new.write(newLine)
        new.close()

main()
