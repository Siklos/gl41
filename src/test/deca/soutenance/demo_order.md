# Compilation avec `decac`

```sh
cd src/test/deca/soutenance/
```

## Sans objet

Compiler avec `decac` et exécuter avec `ima` :

```sh
decac Factorielle.deca && \
ima Factorielle.ass
```

```sh
decac FactorielleRecursive.deca && \
ima FactorielleRecursive.ass
```

```sh
decac jeu_devin.deca && \
ima jeu_devin.ass
```

## Objet

Compiler avec `decac` et exécuter avec `ima` :

```sh
decac Zoo.deca && \
ima Zoo.ass
```

# Options du compilateur

## Banner `b`

```sh
decac -b
```

## Parsing/Decompilation `-p`

On décompile 2 fois afin de vérifier que la décompilation a réussie :

```sh
decac -p decompile.deca > decompile.tmp.deca && \
decac -p decompile.tmp.deca
```

On peut montrer le test de parsing qui teste tous les cas de contexte :
```sh
./src/test/script/developed-code-parse.sh
```


## Option `-v`

On execute `decac` jusqu'à la vérification contextuelle.

```sh
decac -v decompile.deca
```


## Option `-r X`

On execute `decac` avec une limitation de registres :

```sh
decac -r 4 limitedRegisters.deca && \
vim limitedRegisters.ass
```

## Option `-n`

On exécute `decac` en enlevant les vérifications d'overflows :

```sh
decac -n divisionByZero.deca
```


## Parallel `-P`

On teste la compilation parallelisé de multiples fichiers.

Generer les fichiers 1000 fichiers .deca :

```sh
cd src/test/deca/soutenance/parallel
./generate_files.sh
```

Tester le temps d'execution :

```sh
time decac -P *.deca
```

```sh
time decac *.deca
```

## Debugging

### Option `-d`

Montrer très rapidement ce que font les 4 niveaux de `-d`.

### Erreur dans un fichier

Executer le script python `Mangler.py`.

```sh
cd debug
./Mangler.py FactorielleRecursive.deca && cat FactorielleRecursive.deca
```

On démontre comment trouver la source d'erreur :

```sh
decac FactorielleRecursive.deca
ima FactorielleRecursive.ass
```

# Extension

Présenter puis exécuter `decac` :

```
decac trigo.deca && \
ima trigo.ass
```
