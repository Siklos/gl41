# 1 Compilation avec `decac`

## 1.1 Sans objet

## 1.2 Objet

# 2 Options du compilateur

## 2.1 Banner `b`

## 2.2 Parsing/Decompilation `-p`

## 2.3 Option `-v`

## 2.4 Option `-r X`

## 2.5 Option `-n`

## 2.6 Parallel `-P`

## 2.7 Debugging

### 2.7.1 Option `-d`

### 2.7.2 Erreur dans un fichier

# 3 Extension

