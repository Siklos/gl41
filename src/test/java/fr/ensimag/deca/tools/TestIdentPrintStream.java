package fr.ensimag.deca.tools;

import org.junit.Test;

import java.io.ByteArrayOutputStream;

import java.io.PrintStream;


public class TestIdentPrintStream {
	
	PrintStream s = new PrintStream(new ByteArrayOutputStream());
	
	@Test
	public void testIdent() {
		IndentPrintStream p = new IndentPrintStream(s);
		p.indent();
		p.println("Bonjour");
		p.print('a');
		p.println();
		p.unindent();
		p.print("bou");
		p.print("chou");
	}
	
}