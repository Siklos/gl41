package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.EnvironmentExp.DoubleDefException;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;


/**
 * Test for Environement Exp using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestEnvironementExpAdvanced {

    final Type INT = new IntType(null);

    final EnvironmentExp expv = new EnvironmentExp(null);
    
    final EnvironmentExp exp1 = new EnvironmentExp(null);
    
    final EnvironmentExp exp2 = new EnvironmentExp(exp1);

    final MethodDefinition expD =  new MethodDefinition(INT,null,null, 1);
    
    final MethodDefinition expV =  new MethodDefinition(INT,null,null, 3);


    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol vidSymbol = symbolTableFactory.create("vide");
    
    final SymbolTable.Symbol itSymbol = symbolTableFactory.create("viopide");
    
    final SymbolTable.Symbol foatSymbol = symbolTableFactory.create("viopideouh");

    DecacCompiler compiler;


    @Before
    public void setup() throws ContextualError, EnvironmentExp.DoubleDefException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        exp1.declare(foatSymbol, null);
        exp1.declare(itSymbol, expD); 
        
        assertEquals(exp2.getParentEnvironment(), exp1);
    }

    @Test
    public void testEnvExp() throws ContextualError, EnvironmentExp.DoubleDefException {
    	expv.declare(vidSymbol, expV);
        try {
            expv.declare(vidSymbol, expD);
        }catch (EnvironmentExp.DoubleDefException e){}
        assertEquals(expv.get(vidSymbol), expV);
    }
    
    @Test
    public void testUnion() throws DoubleDefException {
    	EnvironmentExp.union(exp1, expv).toString();
    }
    
    @Test
    public void testEmpilement() throws DoubleDefException {
    	expv.empilement(exp1);
    }
}
