package fr.ensimag.deca.context;

import org.junit.Test;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;

import static org.junit.Assert.*;

public class TestCompilerOptions {
	
	final String[] tabOptions = {"-b", "-p", "-v", "-r", "-n", "-d", "-P", "-a", "cool.deca"};  

	
	@Test
    public void testOptionBanner() throws CLIException {
		String[] tab = new String[1];
		tab[0] = "-b";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertTrue(options.getPrintBanner());
    }
	
	@Test
    public void testOptionParseVerifyIncompatible() throws CLIException {
		String[] tab = new String[3];
		tab[0] = "-p";
		tab[1] = "-v";
		tab[2] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
		tab[1] = "-p";
		tab[0] = "-v";
		options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
    }
	
	@Test
    public void testOptionParse() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-p";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertEquals(options.getStage(), 1);
    }
	
	@Test
    public void testOptionVerify() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-v";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertEquals(options.getStage(), 2);
    }
	
	@Test
    public void testOptionRegister() throws CLIException {
		String[] tab = new String[3];
		tab[0] = "-r";
		tab[1] = "8";
		tab[2] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertEquals(options.getUserNbReg(), 8);
    }
	
	@Test
    public void testOptionRegisterError() throws CLIException {
		String[] tab = new String[3];
		tab[0] = "-r";
		tab[1] = "1";
		tab[2] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch(CLIException e) {}
		tab[1] = "19";
		options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch(CLIException e) {}
		tab[1] = "e";
		options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch(CLIException e) {}
	}
	
	@Test
    public void testOptionParallel() throws CLIException {
		String[] tab = new String[4];
		tab[0] = "-P";
		tab[1] = "cool.deca";
		tab[2] = "cool1.deca";
		tab[3] = "cool2.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertTrue(options.getParallel());
    }
	
	@Test
    public void testOptionNoCheck() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-n";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertTrue(options.isNoCheck());
    }
	
	@Test
    public void testOptionDebug1() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-d";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertEquals(options.getDebug(), 1);
    }
	
	@Test
    public void testOptionDebug2() throws CLIException {
		String[] tab = new String[3];
		tab[0] = "-d";
		tab[1] = "-d";
		tab[2] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
    }
	
	@Test
    public void testOptionDebug3() throws CLIException {
		String[] tab = new String[4];
		tab[0] = "-d";
		tab[1] = "-d";
		tab[2] = "-d";
		tab[3] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
    }
	
	@Test
    public void testOptionDebug4() throws CLIException {
		String[] tab = new String[5];
		tab[0] = "-d";
		tab[1] = "-d";
		tab[2] = "-d";
		tab[3] = "-d";
		tab[4] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
    }
	
	@Test
    public void testOptionFile() throws CLIException {
		String[] tab = new String[1];
		tab[0] = "bonjour.deca";
		CompilerOptions options = new CompilerOptions();
		options.parseArgs(tab);
		assertEquals(options.getSourceFiles().get(0).getName(), tab[0]);
    }
	
	@Test
    public void testOptionNotFound() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-a";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
		
    }
	
	@Test
    public void testOptionEmpty() throws CLIException {
		String[] tab = new String[0];
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
    }
	
	@Test
    public void testNoOptionAfterFile() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "cool.deca";
		for (int i=0; i<8; i++) {
			tab[1] = tabOptions[i];
			CompilerOptions options = new CompilerOptions();
			try {
				options.parseArgs(tab);
			} catch (CLIException e) {}
		}	
    }
	
	@Test
    public void testOptionBannerIncompatible() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-b";
		for (int i=0; i<9; i++) {
			tab[1] = tabOptions[i];
			CompilerOptions options = new CompilerOptions();
			try {
				options.parseArgs(tab);
			} catch (CLIException e) {}
		}	
		tab[1] = "-b";
		for (int i=0; i<9; i++) {
			tab[0] = tabOptions[i];
			CompilerOptions options = new CompilerOptions();
			try {
				options.parseArgs(tab);
			} catch (CLIException e) {}
		}
    }
	
	@Test
    public void testOptionDebugError() throws CLIException {
		String[] tab = new String[2];
		tab[0] = "-dad";
		tab[1] = "cool.deca";
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
    }
	
	@Test
    public void testOptionFileError() throws CLIException {
		String[] tab = new String[1];
		tab[0] = "bonjour";
		CompilerOptions options = new CompilerOptions();
		try {
			options.parseArgs(tab);
		} catch (CLIException e) {}
    }
	
}
