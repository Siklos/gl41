package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;


/**
 * Test for class Type using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestClassTypeAdvanced {

    final Type INT = new IntType(null);

    final EnvironmentType env = new EnvironmentType(null);

    final TypeDefinition typeD = new TypeDefinition(INT, null);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("A");

    final SymbolTable.Symbol Symbol1 = symbolTableFactory.create("B");

    final ClassType c = new ClassType(Symbol, null, null);

    final ClassDefinition cd = new ClassDefinition(c,null, null);

    final ClassType cc = new ClassType(Symbol, null, cd);

    final ClassDefinition cd1 = new ClassDefinition(cc,null, null);

    final ClassType ccc = new ClassType(Symbol1, null, cd1);
    
    final NullType NULL  = new NullType(null);


    DecacCompiler compiler;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
    }

    @Test
    public void testClassType() throws ContextualError {
        ClassDefinition cd = c.getDefinition();
        assertEquals(c.asClassType(null, null), c);
        assertTrue(c.isClass());
        assertTrue(c.isClassOrNull());
        assertFalse(c.sameType(INT));
        assertFalse(c.sameType(cc));
        assertTrue(c.sameType(c));
        assertFalse(c.isAssignableFrom(null));
        assertTrue(c.isAssignableFrom(cc));
        assertTrue(c.isAssignableFrom(NULL));
        assertTrue(c.isAssignableFrom(ccc));
        assertTrue(c.isAssignableFrom(c));
        assertFalse(ccc.isAssignableFrom(c));
        assertTrue(NULL.isSubClassOf(c));
    }
}
