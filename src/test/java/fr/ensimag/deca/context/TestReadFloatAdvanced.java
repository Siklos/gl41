package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tree.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * Test for ReadFloat using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestReadFloatAdvanced {

    final Type FLOAT = new FloatType(null);

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
    }

    @Test
    public void testReadInt() throws ContextualError {
        ReadFloat t = new ReadFloat();
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isFloat());
    }
}