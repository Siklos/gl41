package fr.ensimag.deca.context;

import com.sun.tools.internal.ws.processor.model.AbstractType;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

/**
 * Test for Type node using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestTypeAdvanced {

    final Type VOID = new VoidType(null);

    final Type INT = new IntType(null);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("int1");

    final IntType intType = new IntType(Symbol);

    final Type FLOAT = new FloatType(null);

    final Type STRING = new StringType(null);

    final Type NULL  = new NullType(null);

    final Signature SIG = new Signature();

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        SIG.add(INT);
        SIG.add(STRING);
        SIG.add(VOID);
        SIG.add(NULL);
    }

    @Test
    public void testAssignIntInt() throws ContextualError {
        assertTrue(VOID.isVoid());
        assertFalse(VOID.isString());
        assertFalse(VOID.isClass());
        assertFalse(VOID.isNull());
        assertTrue(VOID.sameType(VOID));

        assertFalse(INT.isVoid());

        assertTrue(FLOAT.sameType(FLOAT));
        assertTrue(FLOAT.isAssignableFrom(INT));
        assertTrue(FLOAT.isAssignableFrom(FLOAT));

        assertTrue(STRING.isString());
        assertTrue(STRING.sameType(STRING));
        try {
            STRING.asClassType(null,null);
        }catch (ContextualError e){}

        assertTrue(SIG.paramNumber(0).isInt());
        assertTrue(SIG.paramNumber(1).isString());
        assertTrue(SIG.paramNumber(2).isVoid());
        assertTrue(SIG.paramNumber(3).isNull());
        assertEquals(SIG.size(), 4);

        assertTrue(NULL.sameType(NULL));
        assertTrue(NULL.isNull());
        assertTrue(NULL.isClassOrNull());

        assertTrue(intType.sameType(INT));
        assertNotNull(intType.toString());
    }
}
