package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;


/**
 * Test for Environement Type using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestEnvironementTypeAdvanced {

    final Type INT = new IntType(null);

    final EnvironmentType env = new EnvironmentType(null);

    final TypeDefinition typeD = new TypeDefinition(INT, null);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol voidSymbol = symbolTableFactory.create("id");

    DecacCompiler compiler;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
    }

    @Test
    public void testEnvType() throws ContextualError, EnvironmentType.DoubleDefException {
        env.declare(voidSymbol, typeD);
        assertEquals(typeD.getNature(), "type");
        try {
            env.declare(voidSymbol, typeD);
        }catch (EnvironmentType.DoubleDefException e){}
    }
}
