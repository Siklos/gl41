package fr.ensimag.deca.context;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestSignatureAdvanced {
	
	final Type VOID = new VoidType(null);

    final Type INT = new IntType(null);
    
    final Type FLOAT = new FloatType(null);
    
    @Test
    public void testSignature() {
    	Signature s1 = new Signature();
    	Signature s2 = new Signature();
    	Signature s3 = new Signature();
    	Signature s4 = new Signature();
    	s1.add(INT);
    	s1.add(VOID);
    	s2.add(INT);
    	s2.add(VOID);
    	s3.add(FLOAT);
    	s4.add(INT);
    	s4.add(FLOAT);
    	assertTrue(s1.equals(s1));
    	assertFalse(s1.equals(null));
    	assertFalse(s1.equals(1));
    	assertTrue(s1.equals(s2));
    	assertFalse(s1.equals(s3));
    	assertFalse(s1.equals(s4));
    }
}
