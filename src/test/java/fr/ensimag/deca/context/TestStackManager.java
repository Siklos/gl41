package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.deca.tree.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * Test for the Stack Manager, using mockito, using @Mock and @Before annotations.
 *
 * @author yaoe
 * @date 01/01/2020
 */
public class TestStackManager {
	final Type INT = new IntType(null);

	StackManager manager;
	
	DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        manager = new StackManager(compiler);
    }

    @Test
    public void test() throws ContextualError {
    	manager.allocate();
    	manager.allocate();
    	manager.deallocate();
    	assertEquals(manager.getMaxStackAllocation(), 2);
    	manager.deallocate();
    	manager.allocate();
    	manager.deallocate();
    	manager.deallocate();
    	manager.deallocate();
    	manager.deallocate();
    	manager.allocate();
    	VariableDefinition def = new VariableDefinition(INT, null);
    	assertTrue(manager.allocateDVal(def)!=null);
    	manager.emptyStackAllocation();
    	manager.allocate();
    	assertEquals(manager.getMaxStackAllocation(), 1);
    	
    }


}