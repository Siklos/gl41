package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.tree.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * Test for the Label Manager, using mockito, using @Mock and @Before annotations.
 *
 * @author yaoe
 * @date 01/01/2020
 */
public class TestLabelManagerAdvanced {

	final LabelManager manager = new LabelManager();

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test() throws ContextualError {
    	assertEquals(manager.useLabelCounter(), 0);
    	assertEquals(manager.useLabelCounter(), 1);
    	assertEquals(manager.useLabelCounter(), 2);
    	assertEquals(manager.useLabelCounter(), 3);
    }


}

