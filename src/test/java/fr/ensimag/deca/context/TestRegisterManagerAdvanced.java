package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * Test for the Register Manager, using mockito, using @Mock and @Before annotations.
 *
 * @author yaoe
 * @date 01/01/2020
 */
public class TestRegisterManagerAdvanced {

	RegisterManager manager;
	
	DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        manager = new RegisterManager();
    }

    @Test
    public void test() throws ContextualError {
    	assertEquals(manager.getCurrentTopRegister(), 2);
    	RegisterManager.setMaxUsableRegister(4);
    	assertEquals(manager.getCurrentTopRegister(), 2);
    	manager.decrementCurrentUsableRegister();
    	assertEquals(manager.getCurrentTopRegister(), 1);
        manager.incrementCurrentUsableRegister();
    	assertEquals(manager.getCurrentTopRegister(), 2);
    	assertTrue(manager.areRegistersAvailable());
    	for (int i=0; i<3; i++) manager.incrementCurrentUsableRegister();
    	assertFalse(manager.areRegistersAvailable());
    }


}