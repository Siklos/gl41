package fr.ensimag.deca.context;

import org.junit.Test;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;

import static org.junit.Assert.*;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Test for Instructions  using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestInstructionsAdvanced {
	
	final Label label = new Label("Bonjour");
	
	final GPRegister r = Register.R0;
	
	final DVal val = new ImmediateInteger(2);
	
	final RegisterOffset addr = new RegisterOffset(3, Register.GB);
	
	final DecacCompiler compiler = new DecacCompiler(null, null);
	
	final StackManager manager = new StackManager(compiler);
	
	@Test
    public void testADD() {
		BinaryInstructionDValToReg a = new ADD(val, r);
		assertEquals(r.getNumber(), 0);
		assertEquals(addr.getOffset(), 3);
		assertEquals(addr.getRegister(), Register.GB);
		assertEquals(addr.toString(), "3(GB)");
    }
	
	@Test
    public void testADDSP() {
		UnaryInstructionImmInt a = new ADDSP(new ImmediateInteger(3));
		new ADDSP(2);
    }
	
	@Test
    public void testBEQ() {
		BranchInstruction b = new BEQ(label);
    }
	
	@Test
    public void testBGE() {
		BranchInstruction b = new BGE(label);
    }
	
	@Test
    public void testBGT() {
		BranchInstruction b = new BGT(label);
    }
	
	@Test
    public void testBLE() {
		BranchInstruction b = new BLE(label);
    }
	
	@Test
    public void testBLT() {
		BranchInstruction b = new BLT(label);
    }
	
	@Test
    public void testBNE() {
		BranchInstruction b = new BNE(label);
    }
	
	@Test
    public void testBOV() {
		BranchInstruction b = new BOV(label);
    }
	
	@Test
    public void testBRA() {
		BranchInstruction b = new BRA(label);
    }
	
	@Test
    public void testBSR() {
		UnaryInstruction b = new BSR(label);
		new BSR(val);
    }
	
	@Test
    public void testCMP() {
		BinaryInstructionDValToReg a = new CMP(val, r);
		new CMP(1, r);
    }
	
	@Test
    public void testDEL() {
		UnaryInstructionToReg a = new DEL(r);
    }
	
	@Test
    public void testDIV() {
		BinaryInstructionDValToReg a = new DIV(val, r);
    }
	
	@Test
    public void testERROR() {
		NullaryInstruction a = new ERROR();
    }
	
	@Test
    public void testFLOAT() {
		BinaryInstructionDValToReg a = new FLOAT(val, r);
    }
	
	@Test
    public void testFMA() {
		BinaryInstructionDValToReg a = new FMA(val, r);
    }
	
	@Test
    public void testHALT() {
		NullaryInstruction a = new HALT();
    }
	
	@Test
    public void testINT() {
		BinaryInstructionDValToReg a = new INT(val, r);
    }
	
	@Test
    public void testLEA() {
		BinaryInstructionDAddrToReg ad = new LEA(addr, r);
    }
	
	@Test
    public void testLOAD() {
		BinaryInstructionDValToReg a = new LOAD(val, r);
		new LOAD(1, r);
    }
	
	@Test
    public void testMUL() {
		BinaryInstructionDValToReg a = new MUL(val, r);
    }
	
	@Test
    public void testNEW() {
		BinaryInstructionDValToReg a = new NEW(val, r);
		new NEW(1, r);
    }
	
	@Test
    public void testOPP() {
		BinaryInstructionDValToReg a = new OPP(val, r);
    }
	
	@Test
    public void testPEA() {
		UnaryInstruction u = new PEA(addr);
		assertEquals(u.getOperand(), addr);
    }
	
	@Test
    public void testPOP() {
		UnaryInstructionToReg a = new POP(r);
		new POP(r, manager);
    }
	
	@Test
    public void testPUSH() {
		UnaryInstruction u = new PUSH(r);
		new PUSH(r, manager);
		assertEquals(u.getOperand(), r);
    }
	
	@Test
    public void testQUO() {
		BinaryInstructionDValToReg a = new QUO(val, r);
    }
	
	@Test
    public void testREM() {
		BinaryInstructionDValToReg a = new REM(val, r);
    }
	
	@Test
    public void testRFLOAT() {
		NullaryInstruction a = new RFLOAT();
    }
	
	@Test
    public void testRINT() {
		NullaryInstruction a = new RINT();
    }
	
	@Test
    public void testRTS() {
		NullaryInstruction a = new RTS();
    }
	
	@Test
    public void testSEQ() {
		UnaryInstructionToReg a = new SEQ(r);
    }
	
	@Test
    public void testSETROUND_DOWNWARD() {
		NullaryInstruction a = new SETROUND_DOWNWARD();
    }
	
	@Test
    public void testSETROUND_TONEAREST() {
		NullaryInstruction a = new SETROUND_TONEAREST();
    }
	
	@Test
    public void testSETROUND_TOWARDZERO() {
		NullaryInstruction a = new SETROUND_TOWARDZERO();
    }
	
	@Test
    public void testSETROUND_UPWARD() {
		NullaryInstruction a = new SETROUND_UPWARD();
    }
	
	@Test
    public void testSGE() {
		UnaryInstructionToReg a = new SGE(r);
    }
	
	@Test
    public void testSGT() {
		UnaryInstructionToReg a = new SGT(r);
    }
	
	@Test
    public void testSHL() {
		UnaryInstructionToReg a = new SHL(r);
    }
	
	@Test
    public void testSHR() {
		UnaryInstructionToReg a = new SHR(r);
    }
	
	@Test
    public void testSLE() {
		UnaryInstructionToReg a = new SLE(r);
    }
	
	@Test
    public void testSLT() {
		UnaryInstructionToReg a = new SLT(r);
    }
	
	@Test
    public void testSNE() {
		UnaryInstructionToReg a = new SNE(r);
    }
	
	@Test
    public void testSOV() {
		UnaryInstructionToReg a = new SOV(r);
    }
	
	@Test
    public void testSTORE() {
		BinaryInstruction b = new STORE(r, addr);
		assertEquals(b.getOperand1(), r);
		assertEquals(b.getOperand2(), addr);
		
    }
	
	@Test
    public void testSUB() {
		BinaryInstructionDValToReg a = new SUB(val, r);
    }
	
	@Test
    public void testSUBSP() {
		UnaryInstructionImmInt a = new SUBSP(new ImmediateInteger(3));
		new SUBSP(2);
		a = new SUBSP(new ImmediateInteger(3), manager);
		a = new SUBSP((new ImmediateInteger(3)).getValue(), manager);
    }
	
	@Test
    public void testTSTO() {
		UnaryInstructionImmInt a = new TSTO(new ImmediateInteger(3));
		new TSTO(2);
    }
	
	@Test
    public void testWFLOAT() {
		NullaryInstruction a = new WFLOAT();
    }
	
	@Test
    public void testWFLOATX() {
		NullaryInstruction a = new WFLOATX();
    }
	
	@Test
    public void testWINT() {
		NullaryInstruction a = new WINT();
    }
	
	@Test
    public void testWNL() {
		NullaryInstruction a = new WNL();
		assertEquals(Register.getR(2).toString(), "R2");
    }
	
	@Test
    public void testWSTR() {
		UnaryInstruction u = new WSTR(new ImmediateString("Bonjour"));
		new WSTR("Bonjour");
    }
	
	@Test
    public void testNULL() {
		DVal d = new NullOperand();
		assertEquals(d.toString(), "#null");
    }
	
	@Test
    public void testLine() {
		Line l = new Line(null, null, null);
		Instruction inst = new WSTR("Bonjour");
		l.setLabel(label);
		l.setComment("Oui");
		l.setInstruction(inst);
		assertEquals(l.getLabel(), label);
		assertEquals(l.getComment(), "Oui");
		assertEquals(l.getInstruction(), inst);
		try {
			new Line("\n");
		} catch (IMAInternalError e) {}
		
		try {
			new Line("\r");
		} catch (IMAInternalError e) {}
		
		
    }
	
	@Test
    public void testLabelOperand() {
		LabelOperand op = new LabelOperand(label);
		assertEquals(op.getLabel(), label);
		assertEquals(op.toString(), "Bonjour");
		String s = "abcdefghijklmnopqrstuvwxyz";
		new Label(s);
		for (int i=0; i<10; i++) {
			s = s.concat(s);
		}
		try {
			new Label(s);
		} catch (java.lang.IllegalArgumentException e) {}
		
		try {
			new Label("a_a!@]]");
		} catch (java.lang.IllegalArgumentException e) {}
	}
	
	@Test
    public void testIMA() {
		Line l = new Line(null, null, null);
		Instruction inst = new WSTR("Bonjour");
		l.setLabel(label);
		l.setComment("Oui");
		l.setInstruction(inst);
		InlinePortion portion = new InlinePortion("PUSH R1");
		IMAProgram p1, p2;
		p1 = new IMAProgram();
		p2 = new IMAProgram();
		p1.add(l);
		p1.add(portion);
		p2.add(l);
		p1.append(p2);
		p1.addFirst(inst, "Yes");
		p1.addInstruction(inst, "Pourquoi pas");
		p1.addInstruction(new STORE(r, addr));
		p1.display();
		p1.add(0,  l);
		p1.addFirst(new STORE(r, addr));
		new IMAInternalError("Pas de soucis", new java.lang.IllegalArgumentException());
		compiler.add(new Line(""));
		compiler.addInstruction(new WNL(), "");
		compiler.addFirst(new WNL(), "");
		compiler.displayIMAProgram();
    }
	
	@Test
    public void testImmediateFloat() {
		DVal d = new ImmediateFloat((float)2.0);
		assertEquals(d.toString(), "#0x1.0p1");
    }
	
	@Test
    public void testImmediateInt() {
		DVal d = new ImmediateInteger(2);
		assertEquals(d.toString(), "#2");
    }

}
