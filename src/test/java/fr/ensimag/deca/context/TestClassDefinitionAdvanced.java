package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;


/**
 * Test for classDefinition using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestClassDefinitionAdvanced {

    final Type INT = new IntType(null);

    final EnvironmentType env = new EnvironmentType(null);

    final TypeDefinition typeD = new TypeDefinition(INT, null);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("A");

    final ClassType c = new ClassType(Symbol, null, null);

    final ClassDefinition cd = new ClassDefinition(c,null, null);

    final ClassDefinition cd1 = new ClassDefinition(c,null, cd);

    DecacCompiler compiler;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        cd.setNumberOfFields(1);
        cd.setNumberOfMethods(1);
    }

    @Test
    public void testClassType() throws ContextualError {
        assertEquals(cd.getNumberOfFields(), 1);
        cd.incNumberOfFields();
        assertEquals(cd.getNumberOfFields(), 2);

        assertEquals(cd.getNumberOfMethods(), 1);
        cd.incNumberOfMethods();
        assertEquals(cd.getNumberOfMethods(), 2);

        assertTrue(cd.isClass());
        ClassType ct = cd.getType();
        assertNull(cd.getSuperClass());
    }
}
