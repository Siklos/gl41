package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

/**
 * Test for the assign node using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestAssignAdvanced {

    final Type FLOAT = new FloatType(null);

    final Type INT = new IntType(null);

    final Type BOOLEAN = new BooleanType(null);

    @Mock
    AbstractExpr intexpr;

    @Mock
    AbstractExpr floatexpr;

    @Mock
    AbstractExpr booleanexpr;

    @Mock
    AbstractLValue lValueI;

    @Mock
    AbstractLValue lValueF;

    @Mock
    AbstractLValue lValueB;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        when(booleanexpr.verifyExpr(compiler, null, null)).thenReturn(BOOLEAN);
        when(lValueI.verifyExpr(compiler, null, null)).thenReturn(INT);
        when(lValueF.verifyExpr(compiler, null, null)).thenReturn(FLOAT);
        when(lValueB.verifyExpr(compiler, null, null)).thenReturn(BOOLEAN);
        ConvFloat convFloat = new ConvFloat(floatexpr);
        when(intexpr.verifyRValue(compiler, null, null, FLOAT)).thenReturn(convFloat);
        when(intexpr.verifyRValue(compiler, null, null, INT)).thenReturn(intexpr);
        when(floatexpr.verifyRValue(compiler, null, null, FLOAT)).thenReturn(floatexpr);
        when(booleanexpr.verifyRValue(compiler, null, null, BOOLEAN)).thenReturn(booleanexpr);
    }

    @Test
    public void testAssignIntInt() throws ContextualError {
        Assign t = new Assign(lValueI,intexpr);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isInt());
        // check that the mocks have been called properly.
        verify(lValueI).verifyExpr(compiler, null, null);
        verify(intexpr).verifyRValue(compiler, null, null, INT);
    }

    @Test
    public void testAssignFloatInt() throws ContextualError {
        Assign t = new Assign(lValueF,intexpr);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isFloat());
        // check that the mocks have been called properly.
        verify(lValueF).verifyExpr(compiler, null, null);
        verify(intexpr).verifyRValue(compiler, null, null, FLOAT);
        assertTrue(t.getRightOperand() instanceof ConvFloat);
    }

    @Test
    public void testAssignFloatFloat() throws ContextualError {
        Assign t = new Assign(lValueF,floatexpr);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isFloat());
        // check that the mocks have been called properly.
        verify(lValueF).verifyExpr(compiler, null, null);
        verify(floatexpr).verifyRValue(compiler, null, null, FLOAT);
    }

    @Test
    public void testAssignBoolBool() throws ContextualError {
        Assign t = new Assign(lValueB,booleanexpr);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(lValueB).verifyExpr(compiler, null, null);
        verify(booleanexpr).verifyRValue(compiler, null, null, BOOLEAN);
    }


}

