package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * Test for the boolean unary and binary operation  using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestBooleanOpUnBnAdvanced {

    final Type BOOLEAN = new BooleanType(null);

    final Type INT = new IntType(null);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("bool");

    final BooleanType bool = new BooleanType(Symbol);

    @Mock
    AbstractExpr boolexpr1;
    @Mock
    AbstractExpr boolexpr2;

    @Mock
    AbstractExpr intexpr1;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        when(boolexpr1.verifyExpr(compiler, null, null)).thenReturn(BOOLEAN);
        when(boolexpr2.verifyExpr(compiler, null, null)).thenReturn(BOOLEAN);
        when(intexpr1.verifyExpr(compiler, null, null)).thenReturn(INT);
        when(intexpr1.getType()).thenReturn(INT);
    }

    @Test
    public void testTypeBool() throws ContextualError {
        assertTrue(bool.sameType(BOOLEAN));
    }

    @Test
    public void testOrBool() throws ContextualError {
        Or t = new Or(boolexpr1, boolexpr2);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(boolexpr1).verifyExpr(compiler, null, null);
        verify(boolexpr2).verifyExpr(compiler, null, null);
    }

    @Test
    public void testAndBool() throws ContextualError {
        And t = new And(boolexpr1, boolexpr2);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(boolexpr1).verifyExpr(compiler, null, null);
        verify(boolexpr2).verifyExpr(compiler, null, null);
    }

    @Test
    public void testEqBool() throws ContextualError {
        Equals t = new Equals(boolexpr1, boolexpr2);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(boolexpr1).verifyExpr(compiler, null, null);
        verify(boolexpr2).verifyExpr(compiler, null, null);
    }

    @Test
    public void testNeqBool() throws ContextualError {
        NotEquals t = new NotEquals(boolexpr1, boolexpr2);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(boolexpr1).verifyExpr(compiler, null, null);
        verify(boolexpr2).verifyExpr(compiler, null, null);
    }

    @Test
    public void testNotBool() throws ContextualError {
        Not t = new Not(boolexpr1);
        // check the result
        assertTrue(t.verifyExpr(compiler, null, null).isBoolean());
        // check that the mocks have been called properly.
        verify(boolexpr1).verifyExpr(compiler, null, null);
    }

    @Test
    public void testNotNotBool() throws ContextualError {
        Not t = new Not(intexpr1);
        // check the result
        try {
            t.verifyExpr(compiler, null, null).isBoolean();
        }catch (ContextualError e){}
        // check that the mocks have been called properly.
        verify(intexpr1).verifyExpr(compiler, null, null);
    }
}