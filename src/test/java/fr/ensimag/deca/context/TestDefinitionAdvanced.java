package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tree.*;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

/**
 * Test for definition using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */
public class TestDefinitionAdvanced {

    final Type INT = new IntType(null);

    final TypeDefinition typeD = new TypeDefinition(INT, null);

    final ParamDefinition paraD = new ParamDefinition(INT, null);

    final MethodDefinition expD = new MethodDefinition(INT, null,null,1);

    final Label label = new Label("test");

    final FieldDefinition fieldD = new FieldDefinition(INT, null, Visibility.PUBLIC ,null, 0);

    @Mock
    AbstractIdentifier ident;

    DecacCompiler compiler;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        when(ident.getDefinition()).thenReturn(typeD);
        expD.setLabel(label);
        typeD.setLocation(null);
    }

    @Test
    public void testDefinition() throws ContextualError {
        assertFalse(ident.getDefinition().isExpression());
        assertEquals(paraD.getNature(), "parameter");
        assertTrue(paraD.isExpression());
        assertTrue(paraD.isParam());

        assertTrue(expD.isMethod());
        assertEquals(expD.getLabel(), label);
        assertEquals(expD.getIndex(), 1);
        assertEquals(expD.asMethodDefinition(null, null), expD);
        assertNull(expD.getSignature());
        assertEquals(expD.getNature(), "method");
        assertFalse(expD.isExpression());

        assertEquals(fieldD.getIndex(),0);
        assertTrue(fieldD.isField());
        assertEquals(fieldD.asFieldDefinition(null, null), fieldD);
        assertEquals(fieldD.getVisibility(), Visibility.PUBLIC);
        assertNull(fieldD.getContainingClass());
        assertEquals(fieldD.getNature(), "field");
        assertTrue(fieldD.isExpression());

        assertNull(typeD.getLocation());
        assertFalse(typeD.isField());
        assertFalse(typeD.isMethod());
        assertFalse(typeD.isClass());
        assertFalse(typeD.isParam());

        DAddr addr = expD.getOperand();
    }

    @Test
    public void testDefinition1() throws ContextualError {
        try {
            typeD.asMethodDefinition(null,null);
        }catch (ContextualError e){}
    }

    @Test
    public void testDefinition2() throws ContextualError {
        try {
            typeD.asFieldDefinition(null,null);
        }catch (ContextualError e){}
    }
}

