package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Test for the NotEquals, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeNotEqualsAdvanced {

    @Mock
    DVal dv;

    BooleanLiteral b1, b2;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        b1 = new BooleanLiteral(true);
        b2 = new BooleanLiteral(false);
    }

    @Test
    public void test() throws ContextualError {
        NotEquals neq = new NotEquals(b1, b2);
        neq.codeBooleanExpression(compiler, new RegisterManager(), false, new Label("false"));
        neq.codeBooleanExpression(compiler, new RegisterManager(), true, new Label("false"));
        GPRegister gpr = neq.codeGenOperator(compiler, Register.R0, dv);
    }
}