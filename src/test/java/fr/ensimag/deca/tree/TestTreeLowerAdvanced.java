package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

/**
 * Test for Lower, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeLowerAdvanced {

    AbstractExpr expr1, expr2;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        expr1 = new IntLiteral(2);
        expr2 = new IntLiteral(3);
    }

    @Test
    public void test() throws ContextualError {
        Lower l = new Lower(expr1, expr2);
        GPRegister gpr1 = l.codeGenOperator(compiler, Register.R0, Register.R0);
        l.codeBooleanExpression(compiler, new RegisterManager(), false, new Label("cc"));
        l.codeBooleanExpression(compiler, new RegisterManager(), true, new Label("cc"));
        assertEquals(l.getOperatorName(), "<");
    }


}
