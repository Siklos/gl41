package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for Plus, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreePlusAdvanced {

    @Mock
    AbstractExpr expr1;

    @Mock
    AbstractExpr expr2;

    DecacCompiler compiler1;

    @Mock
    DecacCompiler compiler2;

    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler1 = new DecacCompiler(null, null);
        when(compiler2.isNoCheck()).thenReturn(true);
    }

    @Test
    public void test() throws ContextualError {
        Plus plus = new Plus(expr1, expr2);
        GPRegister gpr1 = plus.codeGenOperator(compiler1, Register.R0, Register.R0);
        GPRegister gpr2 = plus.codeGenOperator(compiler2, Register.R0, Register.R0);
        assertEquals(plus.getOperatorName(), "+");

    }


}
