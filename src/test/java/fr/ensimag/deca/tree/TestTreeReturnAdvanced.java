package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.Label;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Test for Return, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeReturnAdvanced {

    @Mock
    TreeFunction f;

    @Mock
    MethodDefinition methodDefinition;

    AbstractExpr exp;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("void");


    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        exp = new IntLiteral(0);
        when(methodDefinition.getLabel()).thenReturn(new Label("testLabel"));
    }

    @Test
    public void test() throws ContextualError {
        Return r = new Return(exp);
        try {
            r.verifyInst(compiler, null, null, new VoidType(null));
        } catch (ContextualError e) {

        }

        compiler.getLabelManager().setCurrentMethod(methodDefinition);
        r.verifyInst(compiler, new EnvironmentExp(null), null, new IntType(Symbol));
        r.codeGenInst(compiler);
        r.decompile(s);
        assertNotNull(r.getExpression());
        r.prettyPrintChildren(s, null);
        r.iterChildren(f);
    }

}