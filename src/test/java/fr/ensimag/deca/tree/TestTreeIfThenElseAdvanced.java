package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for IfThenElse, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeIfThenElseAdvanced {

    @Mock
    Label l;

    @Mock
    TreeFunction f;

    AbstractInst i, j;

    DecacCompiler compiler;

    AbstractExpr condition;

    ListInst thenBranch;

    ListInst elseBranch;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        condition = new BooleanLiteral(true);
        thenBranch = new ListInst();
        elseBranch = new ListInst();
        i = new IfThenElse(new BooleanLiteral(false), new ListInst(), new ListInst());
        j = new BooleanLiteral(false);
    }

    @Test
    public void test() throws ContextualError {
        IfThenElse exp = new IfThenElse(condition, thenBranch, elseBranch);
        assertTrue(exp.isIfThenElse());

        elseBranch.add(j); // (!false, false) = (true, false)
        exp.codeGenInst(compiler);
        elseBranch.set(0, i); // (false, false )
        exp.codeGenInst(compiler);
        elseBranch.set(0,j); // (true, true)
        elseBranch.add(i);
        exp.codeGenInst(compiler);
        elseBranch.set(0, j); // (false, true)
        exp.codeGenInst(compiler);
        exp.decompile(s);
        exp.iterChildren(f);
    }
}
