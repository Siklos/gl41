package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for Minus, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeUnaryMinusAdvanced {

    @Mock
    DecacCompiler compiler;

    AbstractExpr expr1, expr2, expr3;



    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        expr1 = new BooleanLiteral(true);
        expr2 = new IntLiteral(0);
        expr3 = new FloatLiteral(0.0f);
    }

    @Test
    public void test() throws ContextualError {
        UnaryMinus ub = new UnaryMinus(expr1);
        UnaryMinus ui = new UnaryMinus(expr2);
        UnaryMinus uf = new UnaryMinus(expr3);
        GPRegister gpr1 = ub.codeGenExpr(compiler,new RegisterManager());
        try {
            ui.verifyExpr(compiler, null, null);
            uf.verifyExpr(compiler, null, null);
            ub.verifyExpr(compiler, null, null);
        }catch (ContextualError e){}
        assertEquals(ub.getOperatorName(), "-");
    }
}