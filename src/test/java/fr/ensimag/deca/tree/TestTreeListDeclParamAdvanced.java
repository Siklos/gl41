package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * Test for ListDesclParam, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeListDeclParamAdvanced {

    @Mock
    AbstractIdentifier i, j;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Mock
    AbstractDeclParam a;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        doNothing().when(a).codeGenDeclParam(compiler, -3);

    }

    @Test
    public void Test() throws ContextualError {
        ListDeclParam ldp = new ListDeclParam();
        ldp.add(a);
        ldp.decompile(s);
        ldp.codeGenListDeclParam(compiler);
    }
}
