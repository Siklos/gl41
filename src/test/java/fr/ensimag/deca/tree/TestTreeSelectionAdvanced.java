/*
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

*/
/**
 * Test for Minus, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 *//*



public class TestTreeSelectionAdvanced {

    @Mock
    TreeFunction f;

    @Mock
    ExpDefinition exp;

    DecacCompiler compiler;

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("A");

    final ClassType c = new ClassType(Symbol, null, null);

    final ClassDefinition cd = new ClassDefinition(c,null, null);

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    SymbolTable t = new SymbolTable();

    SymbolTable.Symbol s1 = t.create("foo");

    AbstractExpr abs = new Identifier(s1);

    AbstractIdentifier abi = new Identifier(s1);

    EnvironmentExp env = new EnvironmentExp(null);


    @Before
    public void setup() throws ContextualError, EnvironmentExp.DoubleDefException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        env.declare(s1, exp);
        env.get()

    }

    @Test
    public void test() throws ContextualError {
       Selection sel = new Selection(abs, abi );
       sel.setExpression(abs);
       sel.setIdentifier(abi);

       sel.verifyExpr(compiler, env, null);

    }
}*/
