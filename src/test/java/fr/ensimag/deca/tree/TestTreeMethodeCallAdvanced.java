package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for MethodCall, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeMethodeCallAdvanced {

    @Mock
    TreeFunction f;

    @Mock
    MethodDefinition methodDefinition;

    @Mock
    Label l;

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("A");

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    AbstractExpr exp1;
    AbstractIdentifier exp2;
    ListExpr exp3;

    DecacCompiler compiler;

    @Mock
    DecacCompiler compiler1;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        exp1 = new This(false);
        exp2 = new Identifier(Symbol);
        exp3 = new ListExpr();
        when(methodDefinition.getIndex()).thenReturn(1);
        exp2.setDefinition(methodDefinition);

    }

    @Test
    public void test() throws ContextualError {
        MethodCall mc = new MethodCall(exp1, exp2, exp3);
        assertNotNull(mc.codeGenExpr(compiler, new RegisterManager()));
        mc.decompile(s);
        exp1 = new This(true);
        mc = new MethodCall(exp1, exp2, exp3);
        mc.decompile(s);
        mc.iterChildren(f);
        mc.codeBooleanExpression(compiler, compiler.getRegisterManager(), true, l);
        mc.codeBooleanExpression(compiler, compiler.getRegisterManager(), false, l);
        mc.prettyPrintChildren(s, null);
    }

}
