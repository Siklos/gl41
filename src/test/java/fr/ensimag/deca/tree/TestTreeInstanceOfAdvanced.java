package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for List InstanceOf, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeInstanceOfAdvanced {

    @Mock
    TreeFunction f;

    @Mock
    AbstractExpr i;


    @Mock
    AbstractIdentifier identifier;

    @Mock
    Label l, ll;


    ClassDefinition classDefinition;

    DecacCompiler compiler;

    ClassType c = new ClassType(null, null, null);

    NullType n = new NullType(null);

    Type flo = new FloatType(null);

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        identifier.setType(c);
        classDefinition = new ClassDefinition(null, null, null);
        when(identifier.getClassDefinition()).thenReturn(classDefinition);
        when(i.codeGenExpr(compiler)).thenReturn(Register.R0);
    }

    @Test
    public void Test() throws ContextualError {


        when(i.verifyExpr(compiler, null, null)).thenReturn(flo);
        when(identifier.verifyType(compiler)).thenReturn(c);
        final InstanceOf iofExceptNonObjectFC = new InstanceOf(i, identifier);
        assertThrows(ContextualError.class, () -> iofExceptNonObjectFC.verifyExpr(compiler, null, null));

        when(i.verifyExpr(compiler, null, null)).thenReturn(flo);
        when(identifier.verifyType(compiler)).thenReturn(flo);
        final InstanceOf iofExceptNonObjectFF = new InstanceOf(i, identifier);
        assertThrows(ContextualError.class, () -> iofExceptNonObjectFF.verifyExpr(compiler, null, null));


        when(i.verifyExpr(compiler, null, null)).thenReturn(c);
        when(identifier.verifyType(compiler)).thenReturn(flo);
        final InstanceOf iofExceptNonObjectCF = new InstanceOf(i, identifier);
        assertThrows(ContextualError.class, () -> iofExceptNonObjectCF.verifyExpr(compiler, null, null));

        when(i.verifyExpr(compiler, null, null)).thenReturn(n);
        when(identifier.verifyType(compiler)).thenReturn(c);
        InstanceOf iof = new InstanceOf(i, identifier);
        iof.verifyExpr(compiler, null, null);
        
        compiler.getStackManager().allocateClassDefinition(classDefinition);
        identifier.setDefinition(classDefinition);
        assertNotNull(iof.codeGenExpr(compiler));
        iof.decompile(s);
        iof.iterChildren(f);
        iof.codeBooleanExpression(compiler, compiler.getRegisterManager(), false, l);
        iof.codeBooleanExpression(compiler, compiler.getRegisterManager(), true, l);
        iof.doAssembly(compiler,Register.R0, classDefinition, l, ll, 0);
        iof.prettyPrintChildren(s, null);

    }
}
