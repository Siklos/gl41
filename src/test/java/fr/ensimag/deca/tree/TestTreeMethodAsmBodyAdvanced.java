package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for MethodAsmBody, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeMethodAsmBodyAdvanced {

    @Mock
    TreeFunction f;

    DecacCompiler compiler;

    AbstractStringLiteral str;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        str = new StringLiteral("test");
    }

    @Test
    public void Test() throws ContextualError {
        MethodAsmBody asm = new MethodAsmBody(str);
        asm.verifyMethodBody(compiler, null, null, null);
        asm.codeGenMethodBody(compiler, null);
        asm.decompile(s);
        asm.prettyPrintChildren(s, null);
        asm.iterChildren(f);
        assertEquals(asm.getNumberOfVariables(), 0);
    }

}
