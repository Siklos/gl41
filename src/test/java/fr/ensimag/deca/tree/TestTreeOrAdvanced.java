package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.Label;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Test for Or, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeOrAdvanced {

    @Mock
    AbstractOpBool bool1;

    @Mock
    AbstractOpBool bool2;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
    }

    @Test
    public void test() throws ContextualError {
        Or or = new Or(bool1, bool2);
        or.codeBooleanExpression(compiler, new RegisterManager(), false, new Label("false"));
        or.codeBooleanExpression(compiler, new RegisterManager(), true, new Label("true"));
        assertEquals(or.getOperatorName(), "||");
    }

}
