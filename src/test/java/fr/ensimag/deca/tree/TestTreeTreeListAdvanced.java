package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for TreeList, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeTreeListAdvanced {

    Type INT = new IntType(null);

    @Mock
    AbstractExpr a;

    @Mock
    TreeFunction f;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test() throws ContextualError {
        ListExpr l = new ListExpr();
        l.add(a);
        Tree t = l.set(0, new IntLiteral(0));
        assertFalse(l.isEmpty());
        Iterator<AbstractExpr> iter = l.iterator();
        int len = l.size();
        l.checkLocation();
        String str = l.prettyPrintNode();
        l.prettyPrintChildren(s, null);
        l.iterChildren(f);
        l.add(a);
        assertEquals(l.size(), 2);
        l.prettyPrintChildren(s, null);
    }
}
