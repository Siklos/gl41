package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for New, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeNewAdvanced {

    @Mock
    TreeFunction f;

    @Mock
    AbstractIdentifier abs, abs1;

    @Mock
    ClassDefinition classDefinition;

    //AbstractIdentifier abs;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("void");

    final SymbolTable.Symbol SymbolClass = symbolTableFactory.create("A");

    ClassType c = new ClassType(SymbolClass, null, null);

    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        //abs = new Identifier(Symbol);
        when(abs.verifyType(compiler)).thenReturn(new VoidType(null));
        when(abs1.verifyType(compiler)).thenReturn(c);
        when(abs1.getType()).thenReturn(c);
        when(abs1.getClassDefinition()).thenReturn(classDefinition);
        when(classDefinition.getNumberOfFields()).thenReturn(0);
    }

    @Test
    public void test() throws ContextualError {
        final New n = new New(abs);
        assertThrows(ContextualError.class, () -> n.verifyExpr(compiler, null, null));
        // cas de classe
        compiler.getStackManager().allocateClassDefinition(classDefinition);
        New n2 = new New(abs1);
        n2.verifyExpr(compiler, null, null);
        assertNotNull(n2.codeGenExpr(compiler, null));
        n2.decompile();
        n2.prettyPrintChildren(s, null);
        n2.iterChildren(f);


    }

}