package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for MethodBody, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeMethodBodyAdvanced {

    VoidType voidType = new VoidType(null);
    IntType intType = new IntType(null);

    @Mock
    TreeFunction f;

    @Mock
    MethodDefinition methodDefinition;

    @Mock
    DecacCompiler compiler1;

    @Mock
    AbstractDeclVar i;

    @Mock
    AbstractInst inst;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    ListDeclVar d;
    ListInst l;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        compiler.getLabelManager().setCurrentMethod(methodDefinition);
        when(methodDefinition.getLabel()).thenReturn(new Label("testLabel"));
        d = new ListDeclVar();
        d.add(i);
        l = new ListInst();
        l.add(inst);
        when(compiler1.getRegisterManager()).thenReturn(new RegisterManager());
        when(compiler1.isNoCheck()).thenReturn(true);
    }

    @Test
    public void testVoid() throws ContextualError {
        MethodBody b = new MethodBody(d,l);
        b.codeGenMethodBody(compiler, voidType);
        b.decompile(s);
        b.iterChildren(f);
    }

    @Test
    public void testNonVoid() throws ContextualError {
        MethodBody b = new MethodBody(d,l);
        b.codeGenMethodBody(compiler, intType);
        b.decompile(s);
        b.iterChildren(f);
        b.getNumberOfVariables();
        b.prettyPrintChildren(s, null);
    }

    @Test
    public void test() throws ContextualError {
        MethodBody b = new MethodBody(d,l);
        b.codeGenMethodBody(compiler, intType);
    }

}
