package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for ReadInt, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeStringLiteralAdvanced {

    @Mock
    TreeFunction f;

    StringLiteral str;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);


    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        str = new StringLiteral("test");
    }

    @Test
    public void test() throws ContextualError {
        try {
            str.codeGenExpr(compiler, new RegisterManager());
        }catch (UnsupportedOperationException e){}
        str.verifyExpr(compiler,null, null);
        str.codeGenPrint(compiler);
        str.decompile(s);
        str.iterChildren(f);
        str.prettyPrintChildren(s, null);
        str.prettyPrintNode();
        assertEquals(str.getValue(), "test");
    }
}

