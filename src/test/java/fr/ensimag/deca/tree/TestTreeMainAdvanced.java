package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * Test for Main, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeMainAdvanced {

    @Mock
    ListDeclVar lv;

    @Mock
    ListInst li;

    @Mock
    TreeFunction f;


    DecacCompiler compiler;

    @Mock
    PrintStream p;

    DecacCompiler compiler1;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler1 = new DecacCompiler(null, null);
        compiler = new DecacCompiler(null, null);
        // Initialiser le champ noCheck à true.
        Whitebox.setInternalState(compiler, "noCheck", true);
    }

    @Test
    public void test() throws ContextualError {
        Main m = new Main(lv, li);
        m.verifyMain(compiler1);
        m.codeGenMain(compiler1);
        m.codeGenMain(compiler);
        m.decompile(new IndentPrintStream(p));
        m.iterChildren(f);
        m.prettyPrintChildren(null, null);
    }
}