package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for Tree, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeTreeAdvanced {

    Type INT = new IntType(null);

    @Mock
    Location l;

    @Mock
    TreeFunction f;

    @Mock
    AbstractIdentifier a1;

    @Mock
    AbstractInitialization a2;

    DecacCompiler compiler, compiler1;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    SymbolTable t = new SymbolTable();

    SymbolTable.Symbol s1 = t.create("foo");


    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(new CompilerOptions(), null);
        String[] tab = new String[3];
        tab[0] = "-d";
        tab[1] = "-dd";
        tab[2] = "cool.deca";
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(tab);
        compiler1 = new DecacCompiler(options, null);
    }

    // le test suivant est pour couvrir toutes les lignes de fr.ensimag.deca.tree.Tree

    @Test
    public void test() throws ContextualError {
        IntLiteral i = new IntLiteral(0);
        Identifier ii = new Identifier(s1);
        DeclVar v = new DeclVar(a1, a1, a2);
        ii.setDefinition(new ParamDefinition(INT, l));
        ii.setLocation(new Location(1,1,"filename"));

        i.setLocation(0,0,"filename");
        i.printNodeLine(s, null, false, false);

        // Couvrir le premier if, else if, else de la fonction printNodeLine
        i.printNodeLine(s, null, true, true, "nodeName");
        i.printNodeLine(s, null, false, true, "nodeName");
        i.printNodeLine(s, null, false, false, "nodeName");
        // Couvrir le deuxiemme if
        i.printNodeLine(s, null, false, false, "nodeName");
        i.setLocation(null);
        i.printNodeLine(s, null, false, false, "nodeName");
        // Couvrir le troisiemme if
        i.printNodeLine(s, null, true, true, "nodeName");
        i.printNodeLine(s, null, true, false, "nodeName");
        i.printNodeLine(s, null, false, true, "nodeName");
        i.printNodeLine(s, null, false, true, "nodeName");
        i.printNodeLine(s, null, false, false, "nodeName");

        v.prettyPrintType(s, null);
        i .prettyPrintDefinition(s, null);
        v.prettyPrintNode();
        i.prettyPrint(s);
        String str = i.prettyPrint();
        i.iter(f);
        try {
            v.checkDecoration();
        }catch (DecacInternalError e){}

        assertTrue(ii.checkAllDecorations());
        try {
            ii.checkLocation();
            i.checkLocation();
        }catch (DecacInternalError e){}

        assertTrue(ii.checkAllLocations());
        str = ii.decompileIfDebug(compiler1);
    }
}
