package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for ListDescMethod, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeListDeclMethodAdvanced {

    @Mock
    AbstractIdentifier i, j;

    @Mock
    ListDeclParam p1;

    @Mock
    AbstractMethodBody body;

    @Mock
    DeclMethod a, b;

    @Mock
    ClassType c;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Mock
    ClassDefinition sc;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
//        sc = new ClassDefinition(c, null, null);
        when(sc.getNumberOfMethods()).thenReturn(0);
        when(a.verifyDeclMethodSignature(compiler, null, sc, 1)).thenReturn(true);
        when(b.verifyDeclMethodSignature(compiler, null, sc, 2)).thenReturn(false);
    }

    @Test
    public void Test() throws ContextualError {
        ListDeclMethod ldm = new ListDeclMethod();
        ldm.add(a);
        ldm.add(b);
        ldm.decompile(s);
        ldm.codeGenListDeclMethod(compiler);
        assertEquals(ldm.verifyListDeclMethodSignature(compiler, null, sc),1);
    }
}
