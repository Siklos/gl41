package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for Minus, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeMinusAdvanced {

    @Mock
    DecacCompiler compiler1;

    AbstractExpr expr1, expr2;

    DecacCompiler compiler2;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler2 = new DecacCompiler(null, null);
        expr1 = new IntLiteral(1);
        expr2 = new IntLiteral(2);
        when(compiler1.isNoCheck()).thenReturn(true);
    }

    @Test
    public void test() throws ContextualError {
        Minus m = new Minus(expr1, expr2);
        GPRegister gpr1 = m.codeGenOperator(compiler1, Register.R0, Register.R0);
        GPRegister gpr2 = m.codeGenOperator(compiler2, Register.R0, Register.R0);
        assertEquals(m.getOperatorName(), "-");
    }
}