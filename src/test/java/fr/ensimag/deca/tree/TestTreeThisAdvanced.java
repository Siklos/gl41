package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for This, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeThisAdvanced {

    @Mock
    TreeFunction f;

    DecacCompiler compiler;


    final SymbolTable symbolTableFactory = new SymbolTable();

    final SymbolTable.Symbol Symbol = symbolTableFactory.create("A");

    final ClassType c = new ClassType(Symbol, null, null);

    final ClassDefinition cd = new ClassDefinition(c,null, null);

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        compiler.getStackManager().allocateClassDefinition(cd);
    }

    @Test
    public void test() throws ContextualError {
        This t = new This(true);
        assertTrue(t.isTokenHidden());
        t.verifyExpr(compiler, null, cd);

        try{
            t.verifyExpr(compiler, null, null);
        }catch (ContextualError e){}
        GPRegister gpr = t.codeGenExpr(compiler);
        t = new This(false);
        t.decompile();
        assertEquals(t.prettyPrintNode(), "This");
        t.prettyPrintChildren(s, null);
        t.iterChildren(f);
    }
}