package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Test for Not, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeNotAdvanced {

    @Mock
    AbstractOpBool bool1;

    @Mock
    AbstractOpBool bool2;

    DecacCompiler compiler;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
    }

    @Test
    public void test() throws ContextualError {
        Not n = new Not(bool1);
        // de la lige 51 à 54, on traite les deux cas : areRegistersAvailable() soit true ou false
        RegisterManager rgm = new RegisterManager();
        GPRegister gpr = n.codeGenExpr(compiler, rgm);
        rgm.resetRegisterManager();
        for(int i = 0; i < 13 ; i++ ){ rgm.incrementCurrentUsableRegister();}
        GPRegister gpr1 = n.codeGenExpr(compiler, rgm);

        n.codeBooleanExpression(compiler, new RegisterManager(), true, new Label("true"));
        assertEquals(n.getOperatorName(), "!");
    }

}
