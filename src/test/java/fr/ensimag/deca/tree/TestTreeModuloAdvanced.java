package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for Modulo, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeModuloAdvanced {

    @Mock
    DecacCompiler compiler1;

    AbstractExpr expr1, expr2, expr3, expr4;

    DecacCompiler compiler2;


    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler2 = new DecacCompiler(null, null);
        expr1 = new IntLiteral(2);
        expr2 = new IntLiteral(4);
        expr3 = new FloatLiteral((float) 4.0);
        when(compiler1.isNoCheck()).thenReturn(true);
    }

    @Test
    public void test() throws ContextualError {
        Modulo m21 = new Modulo(expr2, expr1);
        Modulo m32 = new Modulo(expr3, expr1);
        Modulo m13 = new Modulo(expr1, expr3);
        Type t1 = m21.verifyExpr(compiler1, null, null);
        // Invalid type : Integers are required for modulo operation
        try {
            Type t3 = m32.verifyExpr(compiler1, null, null);
            Type t4 = m13.verifyExpr(compiler1, null, null);
        }catch (ContextualError e){}
        GPRegister gpr1 = m21.codeGenOperator(compiler1, Register.R0, Register.R0);
        GPRegister gpr2 = m21.codeGenOperator(compiler2, Register.R0, Register.R0);
        assertEquals(m21.getOperatorName(), "%");
    }
}