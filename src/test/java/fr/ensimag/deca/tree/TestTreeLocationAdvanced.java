package fr.ensimag.deca.tree;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.CompilerOptions;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.when;

/**
 * Test for Location, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeLocationAdvanced {

    final String NO_SOURCE_NAME = "<no source file>";

    final Location BUILTIN = new Location(-1, -1, NO_SOURCE_NAME);

    @Before
    public void setup() throws ContextualError, CLIException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test() throws ContextualError {
        Location l = new Location(1,1,"filename");
        assertEquals(l.toString(), "[1, 1]");
        assertEquals(l.BUILTIN.toString(), "[builtin]");
    }


}