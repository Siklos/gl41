package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test for ListDeclField, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */


public class TestTreeListDeclField {

    @Mock
    AbstractIdentifier a,b;

    @Mock
    AbstractInitialization initialization;

    @Mock
    ClassDefinition superClass;

    ClassDefinition currentClass;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    DecacCompiler compiler;

    @Mock
    DeclField field;

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
//        field = new DeclField(Visibility.PUBLIC, a, b, initialization);
        doNothing().when(field).codeGenDeclField(compiler);
        when(superClass.getSuperClass()).thenReturn(null);
        when(superClass.getSuperClass()).thenReturn(null);
    }

    @Test
    public void Test() throws ContextualError {
        SymbolTable symbolTable = new SymbolTable();
        ClassType classType = new ClassType(symbolTable.create("Object"), Location.BUILTIN, superClass);
        currentClass = new ClassDefinition(classType, Location.BUILTIN, superClass);
        ListDeclField f = new ListDeclField();
        f.add(field);
        f.decompile(s);
        f.codeGenListDeclField(compiler, currentClass, superClass);
    }
}
