package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.ima.pseudocode.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Test for Initialization, using mockito, using @Mock and @Before annotations.
 *
 * @author benkerre
 * @date 01/01/2020
 */

public class TestTreeInitializationAdvanced {

    @Mock
    DAddr d;

    @Mock
    TreeFunction f;

    @Mock
    AbstractExpr expression;

    DecacCompiler compiler;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    PrintStream s = new PrintStream(out);

    @Before
    public void setup() throws ContextualError {
        MockitoAnnotations.initMocks(this);
        compiler = new DecacCompiler(null, null);
        when(expression.verifyRValue(compiler, null, null, null)).thenReturn(new IntLiteral(0));
        when(expression.codeGenExpr(compiler)).thenReturn(Register.R0);
    }

    @Test
    public void test() throws ContextualError {
        Initialization initialization = new Initialization(expression);
        initialization.setExpression(expression);
        assertEquals(initialization.getExpression(), expression);
        initialization.codeGenInitialization(compiler, d);
        initialization.decompile(s);
        initialization.iterChildren(f);
        initialization.prettyPrintChildren(s, null);
        initialization.verifyInitialization(compiler, null, null, null);
    }
}
