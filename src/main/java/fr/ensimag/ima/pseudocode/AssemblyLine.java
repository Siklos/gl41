package fr.ensimag.ima.pseudocode;

import java.io.PrintStream;

/**
 * The Assembly "Line" is a block of code of assembly for the ima assembly.
 */
public class AssemblyLine extends AbstractLine {
    private String code;

    /**
     * Instantiates a new Assembly line.
     *
     * @param code the assembly code (can have line returns and tabulations within it)
     */
    public AssemblyLine(String code) {
        this.code = code;
    }

    @Override
    void display(PrintStream s) {
        s.print(this.code);
    }
}
