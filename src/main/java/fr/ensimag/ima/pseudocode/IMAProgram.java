package fr.ensimag.ima.pseudocode;

import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.TSTO;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;

/**
 * Abstract representation of an IMA program, i.e. set of Lines.
 *
 * @author Ensimag
 * @date 01 /01/2020
 */
public class IMAProgram {
    private final LinkedList<AbstractLine> lines = new LinkedList<AbstractLine>();

    private Line ADDSPLine;
    private Line TSTOLine;
    private Line BOVLine;


    /**
     * Add a new line to an index
     *
     * @param i    the
     * @param line the line
     */
    public void add(int i, AbstractLine line) {
        lines.add(i, line);
    }

    /**
     * Add a line.
     *
     * @param line the line
     */
    public void add(AbstractLine line) {
        lines.add(line);
    }

    /**
     * Add comment.
     *
     * @param s the s
     */
    public void addComment(String s) {
        lines.add(new Line(s));
    }

    /**
     * Add comment to an index.
     *
     * @param index   the index
     * @param comment the comment
     */
    public void addComment(int index, String comment) {
        lines.add(index, new Line(comment));
    }

    /**
     * Add label.
     *
     * @param l the l
     */
    public void addLabel(Label l) {
        lines.add(new Line(l));
    }

    /**
     * Add instruction.
     *
     * @param i the
     */
    public void addInstruction(Instruction i) {
        lines.add(new Line(i));
    }

    /**
     * Add instruction to an index.
     *
     * @param index the index
     * @param i     the
     */
    public void addInstruction(int index, Instruction i) {
        lines.add(index, new Line(i));
    }

    /**
     * Add instruction with a comment.
     *
     * @param i the
     * @param s the s
     */
    public void addInstruction(Instruction i, String s) {
        lines.add(new Line(null, i, s));
    }

    /**
     * Add an ADDSP line. This method must be used with setADDSP to set its value.
     */
    public void addADDSPLine() {
        this.ADDSPLine = new Line(new ADDSP(0));
        this.add(ADDSPLine);;
    }

    /**
     * Update the value of the ADDSP instruction of the line previously added with addADDSPLine.
     *
     * @param value the value
     */
    public void setADDSP(int value) {
        assert(this.ADDSPLine != null);
        assert(value >= 0);
        if (value == 0) {
            this.ADDSPLine.setInstruction(null);
            this.ADDSPLine.setComment("Pas besoin de ADDSP");
        } else {
            this.ADDSPLine.setInstruction(new ADDSP(value));
        }
    }

    /**
     * Add an TSTO line. This method must be used with setTSTO to set its value.
     */
    public void addTSTOLine() {
        this.TSTOLine = new Line(new TSTO(0));
        this.add(TSTOLine);
        this.BOVLine = new Line(new BOV(new Label("stack_overflow")));
        this.add(BOVLine);
    }

    /**
     * Update the value of the TSTO instruction of the line previously added with addTSTOLine.
     *
     * @param value the value
     */
    public void setTSTO(int value) {
        assert(this.TSTOLine != null);
        assert(value >= 0);
        if (value == 0) {
            this.TSTOLine.setInstruction(null);
            this.TSTOLine.setComment("Pas besoin de TSTO");
            this.BOVLine.setInstruction(null);
        } else {
            this.TSTOLine.setInstruction(new TSTO(value));
        }
    }

    /**
     * Append the content of program p to the current program. The new program
     * and p may or may not share content with this program, so p should not be
     * used anymore after calling this function.
     *
     * @param p the p
     */
    public void append(IMAProgram p) {
        lines.addAll(p.lines);
    }

    /**
     * Add a line at the front of the program.
     *
     * @param l the l
     */
    public void addFirst(Line l) {
        lines.addFirst(l);
    }

    /**
     * Display the program in a textual form readable by IMA to stream s.
     *
     * @param s the s
     */
    public void display(PrintStream s) {
        for (AbstractLine l: lines) {
            l.display(s);
        }
    }

    /**
     * Return the program in a textual form readable by IMA as a String.
     *
     * @return the string
     */
    public String display() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream s = new PrintStream(out);
        display(s);
        return out.toString();
    }

    /**
     * Add an instruction to the top of the program.
     *
     * @param i the
     */
    public void addFirst(Instruction i) {
        addFirst(new Line(i));
    }

    /**
     * Add an instruction with a comment to the top of the program.
     *
     * @param i       the
     * @param comment the comment
     */
    public void addFirst(Instruction i, String comment) {
        addFirst(new Line(null, i, comment));
    }

    /**
     * Return the number of instructions.
     *
     * @return the int
     */
    public int size() {
        return lines.size();
    }
}
