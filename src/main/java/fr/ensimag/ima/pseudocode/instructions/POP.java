package fr.ensimag.ima.pseudocode.instructions;

import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.UnaryInstructionToReg;

/**
 *
 * @author Ensimag
 * @date 01/01/2020
 */
public class POP extends UnaryInstructionToReg {

    public POP(GPRegister op) {
        super(op);
    }

    public POP(GPRegister op, StackManager stackManager) {
        super(op);
        stackManager.deallocate();
    }
}
