package fr.ensimag.ima.pseudocode.instructions;

import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.UnaryInstructionImmInt;

/**
 *
 * @author Ensimag
 * @date 01/01/2020
 */
public class SUBSP extends UnaryInstructionImmInt {

    public SUBSP(ImmediateInteger operand) {
        super(operand);
    }

    public SUBSP(int i) {
        super(i);
    }

    public SUBSP(ImmediateInteger operand, StackManager stackManager) {
        super(operand);
        stackManager.deallocate(operand.getValue());
    }

    public SUBSP(int i, StackManager stackManager) {
        super(i);
        stackManager.deallocate(i);
    }

}
