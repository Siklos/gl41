package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;

import java.util.HashMap;

/**
 * The Stack manager.
 * Save the stack pointer, the address of the differents classes in the stack.
 * Has a nextVariableOffset when declaring variables and building the method table after an ADDSP.
 */
public class StackManager {
    private DecacCompiler compiler;
    private int nextAllocationOffset; // plus petite case non allouee du stack // SP+1
    private int nextVariableOffset; // la case dans laquelle allouer la variable suivante
    private int maxStackAllocation; // Nombre maximum de cases allouées dans l'environnement (ie max de SP)
    private HashMap<ClassDefinition, RegisterOffset> classMap;

    /**
     * Manage the allocation of the stack
     *
     * @param compiler the compiler
     */
    public StackManager(DecacCompiler compiler) {
        this.compiler = compiler;
        this.nextVariableOffset = 1;
        this.nextAllocationOffset = 1;
        this.maxStackAllocation = 0;
        this.classMap = new HashMap<>();
    }

    /**
     * Allocate and assign a variable into a register into the stack and return the offset
     *
     * @param def the def
     * @return register offset
     */
    public RegisterOffset allocateDVal(VariableDefinition def) {
        RegisterOffset reg = new RegisterOffset(this.nextVariableOffset, Register.LB);
        def.setOperand(reg);
        this.allocateVariableOffset();
        return reg;
    }

    /**
     * Return an address from the stack for a class definition.
     *
     * @param classDefinition the class definition
     * @return the register offset
     */
    public RegisterOffset allocateClassDefinition(ClassDefinition classDefinition) {
        RegisterOffset reg = new RegisterOffset(this.nextVariableOffset, Register.GB);
        this.classMap.put(classDefinition, reg);
        this.nextVariableOffset++;
        return reg;
    }

    /**
     * Gets the class address corresponding in the stack.
     *
     * @param classDefinition the class definition
     * @return the class register offset
     */
    public RegisterOffset getClassRegisterOffset(ClassDefinition classDefinition) {
        return this.classMap.get(classDefinition);
    }

    /**
     * Increment the stack pointer.
     */
    public void allocate() {
        if (maxStackAllocation < nextAllocationOffset) {
            maxStackAllocation = nextAllocationOffset;
        }
        this.nextAllocationOffset++;
    }

    /**
     * Increment the variable offset.
     */
    public void allocateVariableOffset() {
        this.allocateVariableOffset(1);
    }

    /**
     * Increment n the stack pointer.
     *
     * @param n the n
     */
    public void allocate(int n) {
        assert(n >= 0);
        if (nextAllocationOffset + n - 1 > maxStackAllocation) {
            maxStackAllocation = nextAllocationOffset + n - 1;
        }
        this.nextAllocationOffset += n;
    }

    /**
     * Increment n the variable offset.
     *
     * @param n the n
     */
    public void allocateVariableOffset(int n) {
        assert(n >= 0);
        this.nextVariableOffset += n;
    }

    /**
     * Deallocate.
     */
    public void deallocate() {
        this.nextAllocationOffset--;
    }

    /**
     * Deallocate.
     *
     * @param n the n
     */
    public void deallocate(int n) {
        assert(n >= 0);
        this.nextAllocationOffset -= n;
    }

    /**
     * Reset the indexes.
     */
    public void emptyStackAllocation() {
        this.nextAllocationOffset = 1;
        this.maxStackAllocation = 0;
    }

    /**
     * Gets max stack allocation.
     *
     * @return the max stack allocation
     */
    public int getMaxStackAllocation() {
        return maxStackAllocation;
    }

    /**
     * Gets next allocation offset.
     *
     * @return the next allocation offset
     */
    public int getNextAllocationOffset() {
        return nextAllocationOffset;
    }


    /**
     * Sets max stack allocation.
     *
     * @param maxStackAllocation the max stack allocation
     */
    public void setMaxStackAllocation(int maxStackAllocation) {
        this.maxStackAllocation = maxStackAllocation;
    }

    /**
     * Sets next allocation offset.
     *
     * @param nextAllocationOffset the next allocation offset
     */
    public void setNextAllocationOffset(int nextAllocationOffset) {
        this.nextAllocationOffset = nextAllocationOffset;
    }

    /**
     * Gets next variable offset.
     *
     * @return the next variable offset
     */
    public int getNextVariableOffset() {
        return nextVariableOffset;
    }

    /**
     * Sets next variable offset.
     *
     * @param nextVariableOffset the next variable offset
     */
    public void setNextVariableOffset(int nextVariableOffset) {
        this.nextVariableOffset = nextVariableOffset;
    }

    /**
     * Reset next variable offset.
     */
    public void resetNextVariableOffset() {
        this.setNextVariableOffset(1);
    }

    /**
     * Class map to string string.
     *
     * @return the string
     */
    public String classMapToString() {
        return this.classMap.toString();
    }
}
