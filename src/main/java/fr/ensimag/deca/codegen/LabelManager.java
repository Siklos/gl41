package fr.ensimag.deca.codegen;

import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.ima.pseudocode.Label;

import java.util.HashMap;
import java.util.TreeSet;

/**
 * The Label manager.
 * Used as a method to keep uniqueness of a label.
 */
public class LabelManager {
    private int labelCounter;
    private MethodDefinition currentMethod;

    /**
     * Instantiates a new Label manager.
     */
    public LabelManager() {
        this.labelCounter = 0;
    }


    /**
     * Generate a label with "code." as prefix of a method label
     *
     * @param methodDefinition the method definition
     * @return the code label
     */
    public static Label getCodeLabel(MethodDefinition methodDefinition) {
        return new Label(methodDefinition.getLabel().toString());
    }

    /**
     * Generate a label with "fin." as prefix of a method label
     *
     * @param methodDefinition the method definition
     * @return the fin label
     */
    public static Label getFinLabel(MethodDefinition methodDefinition) {
        String methodString = methodDefinition.getLabel().toString();
        int size = methodString.length();
        return new Label("fin." + methodString.substring(5, size));
    }

    /**
     * Sets current method.
     *
     * @param currentMethod the current method
     */
    public void setCurrentMethod(MethodDefinition currentMethod) {
        this.currentMethod = currentMethod;
    }

    /**
     * Gets current method.
     *
     * @return the current method
     */
    public MethodDefinition getCurrentMethod() {
        return currentMethod;
    }

    /**
     * Return the label counter value then increment it.
     *
     * @return the int
     */
    public int useLabelCounter() {
        return this.labelCounter++;
    }
}
