package fr.ensimag.deca.codegen;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

/**
 * The Register manager.
 * It keep the currentTopRegister as the lowest usable register.
 * It also saves the maximum index of the registers that was used. Useful to know how many registers will used in an function.
 */
public class RegisterManager {
    private int currentTopRegister;
    private int maxUsedRegister;
    private static int maxUsableRegister = 15;

    /**
     * Instantiates a new Register manager.
     * currentTopRegister is 2 by default.
     * maxUsedRegister is 1 by default, but is also usable. Use this counter when you want to use a GPRegister other than R0 and R1.
     */
    public RegisterManager() {
        this.currentTopRegister = 2; // R2 est le premier registre courant
        this.maxUsedRegister = 1; // mais il n'est pas utilise
    }

    /**
     * Indique si le registre courant n'est pas le dernier registre autorise.
     *
     * @return the boolean
     */
    public boolean areRegistersAvailable() {
        return currentTopRegister < RegisterManager.maxUsableRegister;
    }

    /**
     * Increment the current usable register.
     */
    public void incrementCurrentUsableRegister() {
        this.currentTopRegister++;
        if (maxUsedRegister < currentTopRegister) {
            maxUsedRegister = currentTopRegister;
        }
    }

    /**
     * Decrement the current usable register.
     */
    public void decrementCurrentUsableRegister() {
        this.currentTopRegister--;
    }

    /**
     * Gets current top register.
     *
     * @return the current top register
     */
    public int getCurrentTopRegister() {
        if (currentTopRegister > maxUsedRegister) {
            maxUsedRegister = currentTopRegister;
        }
        return currentTopRegister;
    }

    /**
     * Sets max usable register.
     *
     * @param maxUsableRegister the max usable register
     */
    public static void setMaxUsableRegister(int maxUsableRegister) {
        RegisterManager.maxUsableRegister = maxUsableRegister;
    }

    /**
     * Sets current top register.
     *
     * @param currentTopRegister the current top register
     */
    public void setCurrentTopRegister(int currentTopRegister) {
        this.currentTopRegister = currentTopRegister;
    }

    /**
     * Gets max used register.
     *
     * @return the max used register
     */
    public int getMaxUsedRegister() {
        return maxUsedRegister;
    }

    /**
     * Reset max used register.
     */
    public void resetMaxUsedRegister() {
        this.maxUsedRegister = 1;
    }

    /**
     * Verify if the registerManager was previously used.
     *
     * @return the boolean
     */
    public boolean isActive() {
        return this.currentTopRegister >= 2;
    }

    /**
     * Reset the register manager to its default value. (cf. its constructor)
     */
    public void resetRegisterManager() {
        this.currentTopRegister = 2;
        this.maxUsedRegister = 2;
    }

    /**
     * Save and restore registers by using pushing and popping them to the stack.
     *
     * @param compiler  the compiler
     * @param lineIndex the line index
     */
    public void saveAndRestoreRegisters(DecacCompiler compiler, int lineIndex) {
        saveRegisters(compiler, lineIndex);
        restoreRegisters(compiler);
    }

    /**
     * Save the registers by pushing them to the stack.
     *
     * @param compiler  the compiler
     * @param lineIndex the line index
     */
    public void saveRegisters(DecacCompiler compiler, int lineIndex) {
        if (this.getMaxUsedRegister() < 2) {
            compiler.addComment(lineIndex,"Pas de registre a sauvegarder");
        } else {
            for (int i = this.getMaxUsedRegister(); i >= 2; i--) {
                compiler.addInstruction(lineIndex, new PUSH(Register.getR(i)));
            }
            compiler.addComment(lineIndex, "Sauvegarde des registres");
        }

    }

    /**
     * Restore registers by popping them from the stack.
     *
     * @param compiler the compiler
     */
    public void restoreRegisters(DecacCompiler compiler) {
        // Restaurer les reg
        for (int i = this.getMaxUsedRegister(); i >= 2; i--) {
            compiler.addInstruction(new POP(Register.getR(i)));
        }
    }


}
