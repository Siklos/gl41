package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tools.SymbolTable.Symbol;

import java.util.*;

/**
 * Dictionary associating identifier's ExpDefinition to their names.
 * 
 * This is actually a linked list of dictionaries: each EnvironmentExp has a
 * pointer to a parentEnvironment, corresponding to superblock (eg superclass).
 * 
 * The dictionary at the head of this list thus corresponds to the "current" 
 * block (eg class).
 * 
 * Searching a definition (through method get) is done in the "current" 
 * dictionary and in the parentEnvironment if it fails. 
 * 
 * Insertion (through method declare) is always done in the "current" dictionary.
 * 
 * @author gl41
 * @date 01/01/2020
 */
public class EnvironmentExp {
    // A FAIRE : implémenter la structure de donnée représentant un
    // environnement (association nom -> définition, avec possibilité
    // d'empilement).

    EnvironmentExp parentEnvironment;
    private TreeMap<String, ExpDefinition> symbolExpDefinition;

    public EnvironmentExp(EnvironmentExp parentEnvironment) {
        this.symbolExpDefinition = new TreeMap<>();
        this.parentEnvironment = parentEnvironment;
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     */
    public ExpDefinition get(Symbol key) {
        return this.symbolExpDefinition.getOrDefault(key.getName(), null);
    }

    public ExpDefinition getRecursive(Symbol key) {
        ExpDefinition expDefinition = this.symbolExpDefinition.getOrDefault(key.getName(), null);
        if (parentEnvironment != null) {
            if (expDefinition == null) {
                expDefinition = this.parentEnvironment.getRecursive(key);
            }
        }
        return expDefinition;
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     * 
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary 
     * - or, hides the previous declaration otherwise.
     * 
     * @param name
     *            Name of the symbol to define
     * @param def
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(Symbol name, ExpDefinition def) throws DoubleDefException {
        if (this.symbolExpDefinition.containsKey(name.getName())) {
            throw new DoubleDefException();
        }
        this.symbolExpDefinition.put(name.getName(), def);
    }

    protected void declareWhenNotContains(Symbol name, ExpDefinition def) {
        if (!this.symbolExpDefinition.containsKey(name.getName())) {
            this.symbolExpDefinition.put(name.getName(), def);
        }
    }

    protected void declarePredef(Symbol name, ExpDefinition def) {
        this.symbolExpDefinition.put(name.getName(), def);
    }

    public EnvironmentExp getParentEnvironment() {
        return parentEnvironment;
    }

    public TreeMap<String, ExpDefinition> getSymbolExpDefinition() {
        return symbolExpDefinition;
    }

    public static EnvironmentExp union(EnvironmentExp env1, EnvironmentExp env2) throws DoubleDefException {
        EnvironmentExp environmentExp = new EnvironmentExp(null);
        TreeMap<String, ExpDefinition> env1Map = env1.getSymbolExpDefinition();
        TreeMap<String, ExpDefinition> env2Map = env2.getSymbolExpDefinition();
        environmentExp.getSymbolExpDefinition().putAll(env2Map);
        SymbolTable fact = new SymbolTable();

        for (Map.Entry<String, ExpDefinition> entry : env2Map.entrySet()) {
            String key = entry.getKey();
            ExpDefinition def = entry.getValue();
            environmentExp.declare(fact.create(key), def);
        }
        return environmentExp;
    }

    public void empilement (EnvironmentExp env2) {
        TreeMap<String, ExpDefinition> env2Map = env2.getSymbolExpDefinition();
        SymbolTable fact = new SymbolTable();
        for (Map.Entry<String, ExpDefinition> entry : env2Map.entrySet()) {
            String key = entry.getKey();
            ExpDefinition def = entry.getValue();
            this.declareWhenNotContains(fact.create(key), def);
        }
    }

    public void setParentEnvironment(EnvironmentExp parentEnvironment) {
        this.parentEnvironment = parentEnvironment;
    }

    @Override
    public String toString() {
        return symbolExpDefinition.toString();
    }

    public List<MethodDefinition> getMethodDefinitions() {
        ArrayList<MethodDefinition> list = new ArrayList<>();
        for (ExpDefinition expDef : symbolExpDefinition.values()) {
            if (expDef.isMethod())
                list.add((MethodDefinition) expDef);
        }
        return list;
    }
}
