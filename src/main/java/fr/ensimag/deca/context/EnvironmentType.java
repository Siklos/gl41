package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;

import java.util.HashMap;

/**
 * Dictionary associating identifier's TypeDefinition to their names.
 *
 */
public class EnvironmentType {
    // A FAIRE : implémenter la structure de donnée représentant un
    // environnement (association nom -> définition, avec possibilité
    // d'empilement).

    EnvironmentType parentEnvironment;
    private HashMap<String, TypeDefinition> env_types;

    public EnvironmentType(EnvironmentType parentEnvironment) {
        this.env_types = new HashMap<>();
        SymbolTable symbolTableFactory = new SymbolTable();
        Symbol voidSymbol = symbolTableFactory.create("void");
        Symbol intSymbol = symbolTableFactory.create("int");
        Symbol floatSymbol = symbolTableFactory.create("float");
        Symbol booleanSymbol = symbolTableFactory.create("boolean");
        Symbol classSymbol = symbolTableFactory.create("Object");
        Symbol equalsSymbol = symbolTableFactory.create("equals");
        this.declarePredef(voidSymbol, new TypeDefinition(new VoidType(voidSymbol), Location.BUILTIN));
        this.declarePredef(intSymbol, new TypeDefinition(new IntType(intSymbol), Location.BUILTIN));
        this.declarePredef(floatSymbol, new TypeDefinition(new FloatType(floatSymbol), Location.BUILTIN));
        this.declarePredef(booleanSymbol, new TypeDefinition(new BooleanType(booleanSymbol), Location.BUILTIN));

        ClassType objectType = new ClassType(classSymbol, Location.BUILTIN, null);
        this.declarePredef(classSymbol, new ClassDefinition(objectType, Location.BUILTIN, null));
        ClassDefinition objectClass = (ClassDefinition) this.get(classSymbol);
        EnvironmentExp env_exp_object = objectClass.getMembers();
        Signature equalsParams = new Signature();
        equalsParams.add(objectType);
        env_exp_object.declarePredef(equalsSymbol, new MethodDefinition(new BooleanType(booleanSymbol), Location.BUILTIN, equalsParams, 1));
        objectClass.incNumberOfMethods();
        this.parentEnvironment = parentEnvironment;
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     */
    public TypeDefinition get(Symbol key) {
        return this.env_types.getOrDefault(key.getName(), null);
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     *
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary
     * - or, hides the previous declaration otherwise.
     *
     * @param name
     *            Name of the symbol to define
     * @param def                                                                 `
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(Symbol name, TypeDefinition def) throws DoubleDefException {
        if (this.env_types.containsKey(name.getName())) {
            throw new DoubleDefException();
        }
        this.declarePredef(name, def);
    }

    public void declarePredef(Symbol name, TypeDefinition def) {
        this.env_types.put(name.getName(), def);
    }

}
