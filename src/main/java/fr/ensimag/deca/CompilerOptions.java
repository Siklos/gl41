package fr.ensimag.deca;

import java.io.File;
import java.io.PrintStream;
import java.util.*;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;



/**
 * User-specified options influencing the compilation.
 *
 * @author gl41
 * @date 01/01/2020
 */
public class CompilerOptions {
    public static final int QUIET = 0;
    public static final int INFO  = 1;
    public static final int DEBUG = 2;
    public static final int TRACE = 3;
    public static final int PARSE = 1;
    public static final int VERIFY = 2;
    public int getDebug() {
        return debug;
    }

    public boolean getParallel() {
        return parallel;
    }

    public boolean getPrintBanner() {
        return printBanner;
    }
    
    public List<File> getSourceFiles() {
        return Collections.unmodifiableList(new ArrayList<>(sourceFiles));
    }

    public int getStage() {
        return stage;
    }

    public int getUserNbReg() {
        return userNbReg;
    }

    public boolean isNoCheck() {
        return noCheck;
    }

    private int debug = 0;
    private boolean parallel = false;
    private boolean printBanner = false;
    private int stage = 0;
    private int userNbReg = -1;
    private HashSet<File> sourceFiles = new HashSet<File>();
    private boolean noCheck = false;
    
    public void parseArgs(String[] args) throws CLIException {
        // A FAIRE : parcourir args pour positionner les options correctement.
        if (args.length == 0) {
            throw new CLIException("");
        }

        boolean needFile = true;
        boolean haveFiles = false;
        this.debug = 0;
        Logger logger = Logger.getRootLogger();
        List<String> options = Arrays.asList(args);
        Iterator<String> iterator = options.iterator();
        while(iterator.hasNext()) {
            String arg = iterator.next();
            switch (arg) {
            
            
                case "-b":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                	if (options.size() > 1) {
                		throw new CLIException("Option -b is incompatible with any other option");
                	}
                    this.printBanner = true;
                    needFile = false;
                    break;
                    
                    
                case "-p":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                    if (this.stage != 0) {
                        throw new CLIException("Option -v is incompatible with the option -p");
                    }
                    this.stage = CompilerOptions.PARSE;
                    needFile = true;
                    break;
                    
                    
                case "-v":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                    if (this.stage != 0) {
                        throw new CLIException("Option -v is incompatible with the option -p");
                    }
                    this.stage = CompilerOptions.DEBUG;
                    needFile = true;
                    break;
                    
                    
                case "-r":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                    String nbReg = iterator.next();
                    try {
                    	this.userNbReg = Integer.parseInt(nbReg);
                    } catch (NumberFormatException e) {
                    	throw new CLIException("Option -r require a number");
                    }
                    
                    if (this.userNbReg < 4 || this.userNbReg > 16) {
                        throw new CLIException("The number of registers must be between 4 and 16");
                    }
                    needFile = true;
                    break;
                    
                    
                case "-P":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                    this.parallel = true;
                    needFile = true;
                    break;
                    
                    
                case "-n":
                	if (haveFiles) {
                		throw new CLIException("Options are before the list of files");
                	}
                    this.noCheck = true;
                    needFile = true;
                    break;
                    
                    
                default:
                	if (arg.matches(".*[.]deca")) {
                	    try {
                            File file = new File(arg).getCanonicalFile();
                            this.sourceFiles.add(file);
                        } catch (Exception e) {
                	        throw new CLIException(e.getMessage());
                        }

                		haveFiles = true;
                	} else {
                		if (arg.charAt(0) == '-') {
                			if (haveFiles) {
                        		throw new CLIException("Options are before the list of files");
                        	}
                            if (arg.length() >= 2 && arg.charAt(1) == 'd') {
                            	char c;
                                for (int i = 1; i < arg.length(); i++){
                                    c = arg.charAt(i);
                                    if (c == 'd') {
                                        this.debug += 1;
                                    }
                                    else {
                                    	throw new CLIException("Invalid argument option "+arg);
                                    }
                                }
                            } else {
                            	throw new CLIException("Invalid argument option "+arg);
                            }
                            needFile = true;
                        } else {
                        	 throw new CLIException("Invalid name of deca file : "+ arg);
                        }
                	}
                    
                    break;
            }
        }
        
        if (needFile && !haveFiles) {
        	throw new CLIException("Chosen options need a list of files");
        }
        if (!needFile && haveFiles) {
        	throw new CLIException("Chosen options are incompatible with a list of files");
        }

        // map command-line debug option to log4j's level.
        switch (getDebug()) {
        case QUIET: break; // keep default
        case INFO:
            logger.setLevel(Level.INFO); break;
        case DEBUG:
            logger.setLevel(Level.DEBUG); break;
        case TRACE:
            logger.setLevel(Level.TRACE); break;
        default:
            logger.setLevel(Level.ALL); break;
        }
        logger.info("Application-wide trace level set to " + logger.getLevel());

        boolean assertsEnabled = false;
        assert assertsEnabled = true; // Intentional side effect!!!
        if (assertsEnabled) {
            logger.info("Java assertions enabled");
        } else {
            logger.info("Java assertions disabled");
        }



    }

    protected void displayUsage() {
        System.out.println("decac [[-p | -v] [-n] [-r X] [-d]* [-P] <fichier deca>...] | [-b]");
    }
}
