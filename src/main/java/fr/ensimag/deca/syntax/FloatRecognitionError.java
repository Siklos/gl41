package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Syntax error for a FLOAT that failed to be parsed, most likely because
 * it is too large.
 *
 * @author Team 41
 * @date 20/01/2020
 */
public class FloatRecognitionError extends DecaRecognitionException {

    private String tokenText;

    public FloatRecognitionError(DecaParser recognizer, ParserRuleContext ctx, String tokenText) {
        super(recognizer, ctx);
        this.tokenText = tokenText;
    }

    @Override
    public String getMessage() {
        return "Token " + tokenText + " could not be parsed to Float.";
    }
}
