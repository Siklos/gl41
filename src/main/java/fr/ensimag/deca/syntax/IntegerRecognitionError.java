package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Syntax error for an INT that failed to be parsed, most likely because
 * it is too small or too large
 *
 * @author Andre
 * @date 20/01/2020
 */
public class IntegerRecognitionError extends DecaRecognitionException {

    private String tokenText;

    public IntegerRecognitionError(DecaParser recognizer, ParserRuleContext ctx, String tokenText) {
        super(recognizer, ctx);
        this.tokenText = tokenText;
    }

    @Override
    public String getMessage() {
        return "Token " + tokenText + " could not be parsed to Integer.";
    }
}
