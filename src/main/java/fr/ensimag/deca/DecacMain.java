package fr.ensimag.deca;

import java.io.File;
import java.util.concurrent.*;

import com.sun.tools.javac.resources.compiler;
import org.apache.log4j.Logger;

/**
 * Main class for the command-line Deca compiler.
 *
 * @author gl41
 * @date 01/01/2020
 */
public class DecacMain {
    private static Logger LOG = Logger.getLogger(DecacMain.class);
    
    public static void main(String[] args) throws InterruptedException {
        // example log4j message.
        LOG.info("Decac compiler started");
        boolean error = false;
        final CompilerOptions options = new CompilerOptions();
        try {
            options.parseArgs(args);
        } catch (CLIException e) {
            System.err.println("Error during option parsing:\n"
                    + e.getMessage());
            options.displayUsage();
            System.exit(1);
        }
        if (options.getPrintBanner()) {
            System.out.println("Equipe gl41");
            System.exit(0);
        }
        if (options.getSourceFiles().isEmpty()) {
            System.exit(0);
            throw new UnsupportedOperationException("decac without argument not yet implemented");
        }
        if (options.getParallel()) {
            LOG.info(java.lang.Thread.activeCount());
            ExecutorService executorService = new ThreadPoolExecutor(4, 4, 60,
                    TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
            try {
                Future future = null;
                for (File source : options.getSourceFiles()) {
                    future = executorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            LOG.info("Debut tache " + Thread.currentThread().getName());
                            DecacCompiler compiler = new DecacCompiler(options, source);
                            compiler.compile();
                            LOG.info("Fin tache " + Thread.currentThread().getName());
                        }
                    });
                }
                future.get();
            } catch(Exception e) {
                e.printStackTrace();
            }
        } else {
            for (File source : options.getSourceFiles()) {
                DecacCompiler compiler = new DecacCompiler(options, source);
                if (compiler.compile()) {
                    error = true;
                }
            }
        }
        System.exit(error ? 1 : 0);
    }
}
