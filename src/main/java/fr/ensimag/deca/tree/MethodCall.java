package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;
import java.util.AbstractList;
import java.util.List;

public class MethodCall extends AbstractExpr implements BooleanExpression {
    private AbstractExpr thisObject;
    private AbstractIdentifier methodName;
    private ListExpr methodParams;

    public MethodCall(AbstractExpr thisObject, AbstractIdentifier methodName, ListExpr methodParams) {
        Validate.notNull(thisObject);
        Validate.notNull(methodName);
        Validate.notNull(methodParams);
        this.thisObject = thisObject;
        this.methodName = methodName;
        this.methodParams = methodParams;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type type2 = this.thisObject.verifyExpr(compiler, localEnv, currentClass);
        ClassType classType2 = type2.asClassType("Caller expression must be a class definition (3.71)", getLocation());
        Definition definition2 = compiler.getEnvironmentType().get(classType2.getName());

        if (definition2 == null) {
            throw new ContextualError("Caller class is not declared in environment (3.71)", getLocation());
        }

        if (!definition2.isClass()) {
            throw new ContextualError("Caller must be defined as a class (3.71)", getLocation());
        }
        ClassDefinition classDefinition2 = (ClassDefinition) definition2;
        EnvironmentExp environmentExp2 = classDefinition2.getMembers();

        Type type = this.methodName.verifyExpr(compiler, environmentExp2, currentClass);
        MethodDefinition methodDefinition = this.methodName.getMethodDefinition();

        Signature sig = methodDefinition.getSignature();

        List<AbstractExpr> list = methodParams.getList();

        if (sig.size() != list.size()) {
            throw new ContextualError("Actual and formal argument lists of method differ in length (3.74)", getLocation());
        }

        for (int i = 0; i < list.size(); i++) {
            AbstractExpr expr = list.get(i);
            AbstractExpr newExpr = expr.verifyRValue(compiler, localEnv, currentClass, sig.paramNumber(i));
            methodParams.set(i, newExpr);
        }
        setType(type);
        return type;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int nbParams = methodParams.size();
        compiler.addInstruction(new ADDSP(nbParams + 1));
        compiler.getStackManager().allocate(nbParams + 1);

        GPRegister register;
        register = thisObject.codeGenExpr(compiler);
        compiler.addInstruction(new STORE(register, new RegisterOffset(0, Register.SP)));


        // Empile params
        int cpt = -1;
        for (AbstractExpr expr : methodParams.getList()) {
            GPRegister reg = expr.codeGenExpr(compiler);
            compiler.addInstruction(new STORE(reg, new RegisterOffset(cpt, Register.SP)));
            cpt--;
        }

        // Dereferencement null
        if (!compiler.isNoCheck()) {
            compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.SP), Register.R1));
            compiler.addInstruction(new CMP(new NullOperand(), Register.R1));
            compiler.addInstruction(new BEQ(new Label("null_dereferencing")));
        }

        int methodIndex = methodName.getMethodDefinition().getIndex();
        compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.R1), Register.R1));
        compiler.addInstruction(new BSR(new RegisterOffset(methodIndex, Register.R1)));
        compiler.getStackManager().allocate(2); // BSR empile PC et old LB
        compiler.addInstruction(new SUBSP(nbParams + 1));
        compiler.getStackManager().deallocate(nbParams + 3);
        return Register.R0;
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        GPRegister register = codeGenExpr(compiler, registerManager);
        compiler.addInstruction(new CMP(0, register));
        if (expectedValue) {
            compiler.addInstruction(new BNE(branchLabel));
        } else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }

    @Override
    public void decompile(IndentPrintStream s) {
        if (this.thisObject instanceof This) {
            if (!((This) this.thisObject).isTokenHidden()) {
                this.thisObject.decompile(s);
                s.print(".");
            }
        }

        this.methodName.decompile(s);
        s.print("(");
        this.methodParams.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
    	thisObject.prettyPrint(s, prefix, false);
    	methodName.prettyPrint(s, prefix, false);
        methodParams.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.thisObject.iter(f);
        this.methodName.iter(f);
        this.methodParams.iter(f);
    }
}
