package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.AssemblyLine;
import fr.ensimag.ima.pseudocode.Line;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

public class MethodAsmBody extends AbstractMethodBody {
    private final AbstractStringLiteral assemblyCode;

    public MethodAsmBody(AbstractStringLiteral assemblyCode) {
        Validate.notNull(assemblyCode);
        this.assemblyCode = assemblyCode;
    }

    @Override
    protected void verifyMethodBody(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass, Type returnType) throws ContextualError {
        this.assemblyCode.verifyExpr(compiler, localEnv, currentClass);
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler, Type returnType) {
        String beginComment = "; Begin asm code body";
        String endComment = "; End asm code body";
        compiler.add(new AssemblyLine( beginComment + "\n" + assemblyCode.getValue() + "\n" + endComment + "\n"));
    }

    @Override
    public int getNumberOfVariables() {
        return 0;
    }

    @Override
    public void decompile(IndentPrintStream s) {
    	s.print("asm(");
        assemblyCode.decompile(s);
        s.println(");");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        assemblyCode.prettyPrintChildren(s, prefix);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        assemblyCode.iter(f);
    }
}
