package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

public class Null extends AbstractExpr implements AbstractTerminalExpr {
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        SymbolTable fact = new SymbolTable();
        NullType nullType = new NullType(fact.create("null"));
        this.setType(nullType);
        return nullType;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int index = registerManager.getCurrentTopRegister();
        compiler.addInstruction(new LOAD(new NullOperand(), Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager) {
        return new NullOperand();
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("null");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        //TODO
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }
}
