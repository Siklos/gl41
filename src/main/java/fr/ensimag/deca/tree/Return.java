package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class Return extends AbstractInst {
    private static final Logger LOG = Logger.getLogger(Main.class);

    private AbstractExpr expression;

    public Return(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass, Type returnType) throws ContextualError {
        if (returnType.isVoid()) {
            throw new ContextualError("Return expression is not expected at the end of void function (3.24)", getLocation());
        }
        this.expression = this.expression.verifyRValue(compiler, localEnv, currentClass, returnType);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        GPRegister register = this.expression.codeGenExpr(compiler);
        compiler.addInstruction(new LOAD(register, Register.R0));
        compiler.addInstruction(new BRA(LabelManager.getFinLabel(compiler.getLabelManager().getCurrentMethod())));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("return ");
        this.getExpression().decompile(s);
        s.println(";");
        
    }

    public AbstractExpr getExpression() {
		return expression;
	}

	@Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expression.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.getExpression().iter(f);
    }
}
