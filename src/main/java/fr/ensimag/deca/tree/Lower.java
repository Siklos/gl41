package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.*;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Lower extends AbstractOpIneq {

    public Lower(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }


    @Override
    protected String getOperatorName() {
        return "<";
    }

    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExpressionRegister, DVal rightExpr) {
        compiler.addInstruction(new SLT(leftExpressionRegister));
        return leftExpressionRegister;
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        super.codeBooleanExpression(compiler, registerManager, expectedValue, branchLabel);
        if (expectedValue)
            compiler.addInstruction(new BLT(branchLabel));
        else
            compiler.addInstruction(new BGE(branchLabel));
    }
}
