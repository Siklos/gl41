package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class Selection extends AbstractLValue {
    private static final Logger LOG = Logger.getLogger(Selection.class);

    private AbstractExpr expression;
    private AbstractIdentifier identifier;

    public Selection(AbstractExpr expression, AbstractIdentifier identifier) {
        Validate.notNull(expression);
        Validate.notNull(identifier);
        this.setExpression(expression);
        this.setIdentifier(identifier);
    }

    public void setExpression(AbstractExpr expression) {
		this.expression = expression;
	}

	public void setIdentifier(AbstractIdentifier identifier) {
		this.identifier = identifier;
	}

	@Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify Selection : start");
        Type typeExpr = this.expression.verifyExpr(compiler, localEnv, currentClass);
        ClassType classType2 = typeExpr.asClassType("Caller expression must be a class but is "+ typeExpr.getName().getName() + " (3.65, 3.66)", getLocation());
        EnvironmentType environmentType = compiler.getEnvironmentType();
        Definition definition2 = environmentType.get(classType2.getName());

        if (definition2 == null) {
            throw new ContextualError("Caller class is not declared in environment (3.65, 3.66)", getLocation());
        }

        if (!definition2.isClass()) {
            throw new ContextualError("Class is not declared as a Class definition (3.65, 3.66)", getLocation());
        }
        ClassDefinition classDefinition2 = (ClassDefinition) definition2;
        EnvironmentExp environmentExp2 = classDefinition2.getMembers();

        Type typeField = this.identifier.verifyExpr(compiler, environmentExp2, currentClass);
        FieldDefinition fieldDefinition = this.identifier.getFieldDefinition();
        Visibility visibility = fieldDefinition.getVisibility();

        if (visibility == Visibility.PROTECTED) {
            if (currentClass == null) {
                throw new ContextualError("Cannot call a protected attributes when not in a class (3.66)", getLocation());
            }


            ClassType classType1 = currentClass.getType();
            if (!classType1.sameType(classType2)) {
                if (!classType2.isSubClassOf(classType1)) {
                    throw new ContextualError("Caller class must be a subclass of this class to call its protected attributes (3.66)", getLocation());
                }
            }

            ClassDefinition classField = fieldDefinition.getContainingClass();
            if (!classType1.sameType(classField.getType())) {
                if (!classType1.isSubClassOf(classField.getType())) {
                    // Savoir si on est dans le main
                    if (classType1.getName().getName() == "Object") {
                        throw new ContextualError("Invalid access to a protected field (3.66)", getLocation());
                    }
                    throw new ContextualError("The class " + classType1.getName().getName() +" must be a subclass of the field's class to use a protected attribute (3.66)", getLocation());
                }
            }
        } else if (visibility != Visibility.PUBLIC) {
            throw new ContextualError("Field visibility is undefined (3.65, 3.66)", getLocation());
        }
        setType(typeField);
        LOG.trace("verify Selection : end");
        return typeField;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        RegisterOffset fieldAddress = (RegisterOffset) this.codeGenExprDAddr(compiler, registerManager);
        GPRegister register = (GPRegister) fieldAddress.getRegister();
        compiler.addInstruction(new LOAD(fieldAddress, register));
        return register;
    }

    @Override
    public DAddr codeGenExprDAddr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = this.expression.codeGenExpr(compiler, registerManager);
//        if (register == null) {
//            return this.identifier.codeGenExprDAddr(compiler, registerManager);
//        } else {
        FieldDefinition fieldDefinition = this.identifier.getFieldDefinition();
        int index = fieldDefinition.getIndex();
        if (!(this.expression instanceof This)) { // this cannot be null if you managed to call a method on it
            if (!compiler.isNoCheck()) {
                compiler.addInstruction(new CMP(new NullOperand(), register));
                compiler.addInstruction(new BEQ(new Label("null_dereferencing")));
            }
        }
        RegisterOffset fieldAddress = new RegisterOffset(index, register);
        return fieldAddress;
//        }

    }

    @Override
    public void decompile(IndentPrintStream s) {
        this.getExpression().decompile(s);
        s.print(".");
        this.getIdentifier().decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expression.prettyPrint(s, prefix, false);
        identifier.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.expression.iter(f);
        this.identifier.iter(f);
    }

	public AbstractExpr getExpression() {
		return expression;
	}

	public AbstractIdentifier getIdentifier() {
		return identifier;
	}
}
