package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * @author gl41
 * @date 01/01/2020
 */
public class DeclVar extends AbstractDeclVar {
    private static final Logger LOG = Logger.getLogger(Main.class);
    
    final private AbstractIdentifier type;
    final private AbstractIdentifier varName;
    final private AbstractInitialization initialization;


    public DeclVar(AbstractIdentifier type, AbstractIdentifier varName, AbstractInitialization initialization) {
        Validate.notNull(type);
        Validate.notNull(varName);
        Validate.notNull(initialization);
        this.type = type;
        this.varName = varName;
        this.initialization = initialization;
    }

    @Override
    protected void verifyDeclVar(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        LOG.trace("verify DeclVar: start");
        Type type = this.type.verifyType(compiler);
        if (type.isVoid()) {
            throw new ContextualError("Expected type other than void (3.17)", getLocation());
        }
        SymbolTable.Symbol name = this.varName.getName();
        this.initialization.verifyInitialization(compiler, type, localEnv, currentClass);
        VariableDefinition variableDefinition = new VariableDefinition(type, getLocation());

        if (localEnv.get(name) != null) {
            throw new ContextualError("Variable is already defined (3.17)", getLocation());
        }

        try {
            localEnv.declare(name, variableDefinition);
            this.varName.setDefinition(variableDefinition);
            LOG.debug("New variable declared :" + name.getName());
        } catch (EnvironmentExp.DoubleDefException e) {
            throw new ContextualError("Variable is already defined (3.17)", getLocation());
        }
        LOG.trace("verify DeclVar: end");
    }


    protected void codeGenDeclVar(DecacCompiler compiler) {
    	DAddr GBOffset = compiler.getStackManager().allocateDVal(this.varName.getVariableDefinition());
        this.initialization.codeGenInitialization(compiler, GBOffset);
    }

    @Override
    public void decompile(IndentPrintStream s) {

        this.type.decompile(s);
        s.print(' ');
        this.varName.decompile(s);
        this.initialization.decompile(s);
        s.println(";");
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        type.iter(f);
        varName.iter(f);
        initialization.iter(f);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        varName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }
}
