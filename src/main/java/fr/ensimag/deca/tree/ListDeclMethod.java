package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.log4j.Logger;

import java.util.List;

public class ListDeclMethod extends TreeList<AbstractDeclMethod> {
    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclMethod method : getList()) {
            method.decompile(s);
        }
    }

    /**
     * Pass 2 of [SyntaxeContextuelle]
     */
    int verifyListDeclMethodSignature(DecacCompiler compiler, EnvironmentExp classEnv,
                                ClassDefinition superClass) throws ContextualError {
        LOG.trace("verify ListDeclMethod: start");
        Integer index = superClass.getNumberOfMethods() + 1;
        for (AbstractDeclMethod method : getList()) {
            boolean isInherited = method.verifyDeclMethodSignature(compiler, classEnv, superClass, index);
            index += isInherited ? 0 : 1;
        }
        LOG.trace("verify ListDeclMethod: end");
        return index - 1;
    }

    /**
     * Pass 3 of [SyntaxeContextuelle]
     */
    void verifyListDeclMethod(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition superClass) throws ContextualError {
        LOG.trace("verify ListDeclMethod: start");
        for (AbstractDeclMethod method : getList()) {
            method.verifyDeclMethod(compiler, localEnv, superClass);
        }
        LOG.trace("verify ListDeclMethod: end");
    }

    void codeGenVirtualMethodTable(DecacCompiler compiler, ClassDefinition currentClass) {
        RegisterOffset classRegisterOffset = compiler.getStackManager().allocateClassDefinition(currentClass);
        compiler.addInstruction(new STORE(Register.R0, classRegisterOffset));

        List<MethodDefinition> methodDefinitions = currentClass.getMethodDefinitions();
        for (MethodDefinition def : methodDefinitions) {
            Label label = LabelManager.getCodeLabel(def);
            compiler.addInstruction(new LOAD(new LabelOperand(label), Register.R0));
            RegisterOffset methodReg = new RegisterOffset(def.getIndex() + classRegisterOffset.getOffset(), Register.GB);
            compiler.addInstruction(new STORE(Register.R0, methodReg));
        }
        compiler.getStackManager().allocateVariableOffset(methodDefinitions.size());
    }

    void codeGenListDeclMethod(DecacCompiler compiler) {
        for (AbstractDeclMethod method : getList()) {
            method.codeGenDeclMethod(compiler);
        }
    }
}
