package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.OPP;

/**
 * @author gl41
 * @date 01/01/2020
 */
public class UnaryMinus extends AbstractUnaryExpr {

    public UnaryMinus(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        Type type = this.getOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!(type.isInt() || type.isFloat())) {
            throw new ContextualError("Invalid type : expected arithmetical type (3.37)", getLocation());
        }
        setType(type);
        return type;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
    	GPRegister r = this.getOperand().codeGenExpr(compiler);
    	compiler.addInstruction(new OPP(r, (GPRegister) r));
    	return r;
    }

    @Override
    protected String getOperatorName() {
        return "-";
    }

}
