package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.log4j.Logger;

public class ListDeclField extends TreeList<AbstractDeclField> {
    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclField field : getList()) {
            field.decompile(s);
        }
    }

    int verifyListDeclFieldMembers(DecacCompiler compiler, EnvironmentExp classEnv,
                             ClassDefinition superClass, ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify ListDeclField: start");
        int index = superClass.getNumberOfFields();
        for (AbstractDeclField field : getList()) {
            index++;
            field.verifyDeclFieldMembers(compiler, classEnv, superClass, currentClass, index);
        }
        LOG.trace("verify ListDeclField: end");
        return index;
    }

    void verifyListDeclField(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify ListDeclField: start");
        for (AbstractDeclField field : getList()) {
            field.verifyDeclField(compiler, localEnv, currentClass);
        }
        LOG.trace("verify ListDeclField: end");
    }

    void codeGenListDeclField(DecacCompiler compiler, ClassDefinition currentClass, ClassDefinition superClass) {
        compiler.addLabel(new Label("init." + currentClass.getType().getName()));
//        compiler.newProgram();

        if (!compiler.isNoCheck())
            compiler.addTSTOLine();

        StackManager stackManager = compiler.getStackManager();
        int oldCurrentSp = stackManager.getNextAllocationOffset();
        stackManager.setMaxStackAllocation(oldCurrentSp - 1);

        RegisterManager registerManager = compiler.getRegisterManager();
        registerManager.resetMaxUsedRegister();

        int lineIndex = compiler.getLinesSize();

        // Appelle le constructeur du parent si le parent n'est pas la classe Object
        if (currentClass.getSuperClass().getSuperClass() != null && currentClass.getSuperClass().getNumberOfFields() > 0) {
            RegisterOffset objectAddress = new RegisterOffset(-2, Register.LB);
            compiler.addInstruction(new LOAD(objectAddress, Register.R0));
            compiler.addInstruction(new PUSH(Register.R0, compiler.getStackManager()));
            compiler.addInstruction(new BSR(new Label("init." + superClass.getType().getName())));
            compiler.addInstruction(new SUBSP(1, compiler.getStackManager()));
        }

        for (AbstractDeclField field : getList()) {
            field.codeGenDeclField(compiler);
        }

        registerManager.saveAndRestoreRegisters(compiler, lineIndex);

        if (!compiler.isNoCheck()) {
            int maxSP = compiler.getStackManager().getMaxStackAllocation() + registerManager.getMaxUsedRegister() - 1;
            compiler.setTSTO(maxSP - (oldCurrentSp - 1));
        }
        stackManager.setNextAllocationOffset(oldCurrentSp);

        compiler.addInstruction(new RTS());
    }
}
