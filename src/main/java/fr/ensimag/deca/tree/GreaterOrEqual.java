package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.*;

/**
 * Operator "x >= y"
 * 
 * @author gl41
 * @date 01/01/2020
 */
public class GreaterOrEqual extends AbstractOpIneq {

    public GreaterOrEqual(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExpressionRegister, DVal rightExpr) {
        compiler.addInstruction(new SGE(leftExpressionRegister));
        return leftExpressionRegister;
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        super.codeBooleanExpression(compiler, registerManager, expectedValue, branchLabel);
        if (expectedValue)
            compiler.addInstruction(new BGE(branchLabel));
        else
            compiler.addInstruction(new BLT(branchLabel));
    }

    @Override
    protected String getOperatorName() {
        return ">=";
    }

}
