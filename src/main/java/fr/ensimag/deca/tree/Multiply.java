package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.MUL;

/**
 * @author gl41
 * @date 01/01/2020
 */
public class Multiply extends AbstractOpArith {
    public Multiply(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExprRegister, DVal rightExpr) {
        compiler.addInstruction(new MUL(rightExpr, leftExprRegister));
        if (!compiler.isNoCheck())
            compiler.addInstruction(new BOV(new Label("overflow")));
        return leftExprRegister;
    }

    @Override
    protected String getOperatorName() {
        return "*";
    }

}
