package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.Label;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class And extends AbstractOpBool {

    public And(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "&&";
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        if (expectedValue) {
            int idCond = compiler.getLabelManager().useLabelCounter();
            Label labelE_End = new Label("E_End." + idCond);
            ((BooleanExpression) getLeftOperand()).codeBooleanExpression(compiler, registerManager, false, labelE_End);
            ((BooleanExpression) getRightOperand()).codeBooleanExpression(compiler, registerManager, true, branchLabel);
            compiler.addLabel(labelE_End);
        } else {
            ((BooleanExpression) getLeftOperand()).codeBooleanExpression(compiler, registerManager, false, branchLabel);
            ((BooleanExpression) getRightOperand()).codeBooleanExpression(compiler, registerManager, false, branchLabel);
        }
    }
}
