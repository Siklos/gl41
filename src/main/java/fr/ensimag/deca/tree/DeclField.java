package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class DeclField extends AbstractDeclField {
    private static final Logger LOG = Logger.getLogger(Main.class);

    final private Visibility visibility;
    final private AbstractIdentifier type;
    final private AbstractIdentifier fieldName;
    final private AbstractInitialization initialization;


    public DeclField(Visibility visibility, AbstractIdentifier type, AbstractIdentifier fieldName, AbstractInitialization initialization) {
        Validate.notNull(visibility);
        Validate.notNull(type);
        Validate.notNull(fieldName);
        Validate.notNull(initialization);
        this.visibility = visibility;
        this.type = type;
        this.fieldName = fieldName;
        this.initialization = initialization;
    }

    @Override
    protected void verifyDeclFieldMembers(DecacCompiler compiler,
                                 EnvironmentExp localEnv, ClassDefinition superClass, ClassDefinition currentClass, int index)
            throws ContextualError {
        LOG.trace("verify DeclField: start");
        Type type = this.type.verifyType(compiler);
        fieldName.setType(type);
        if (type.isVoid()) {
            throw new ContextualError("Expected type other than void", getLocation());
        }
        SymbolTable.Symbol name = this.fieldName.getName();

        if (localEnv.get(name) != null) {
            throw new ContextualError("Variable is already defined", getLocation());
        }

        ClassDefinition classDefinition = (ClassDefinition) compiler.getEnvironmentType().get(superClass.getType().getName());
        // On vérifie que la superClasse est correctement définie
        if (classDefinition != null) {
            // On vérifie que la superClasse ne possèderais pas un membre du même nom
            ExpDefinition superDefinition = classDefinition.getMembers().get(name);
            if (superDefinition != null) {
                // Si il existe une définition du même nom, on vérifie qu'elle possède un définition de champ
                if (!(superDefinition.isField())) {
                    throw new ContextualError("Field name has been already declared in superclass but is not a field definition", getLocation());
                }
            }
        }

        FieldDefinition fieldDefinition = new FieldDefinition(type, getLocation(), this.visibility, currentClass, index);
        try {
            localEnv.declare(name, fieldDefinition);
            this.fieldName.setDefinition(fieldDefinition);
            LOG.debug("New field declared :" + name.getName());
        } catch (EnvironmentExp.DoubleDefException e) {
            throw new ContextualError("Field is already defined", getLocation());
        }
        LOG.trace("verify DeclField: end");
    }

    @Override
    protected void verifyDeclField(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type type = this.type.verifyType(compiler);
        this.initialization.verifyInitialization(compiler, type, localEnv, currentClass);
    }

    protected void codeGenDeclField(DecacCompiler compiler) {
        compiler.addComment("Initialisation de " + fieldName.getName().getName());

        RegisterManager registerManager = compiler.getRegisterManager();
        int index = registerManager.getCurrentTopRegister();
        GPRegister register = Register.getR(index);
        RegisterOffset objectAddress = new RegisterOffset(-2, Register.LB);
        compiler.addInstruction(new LOAD(objectAddress, register));
        int fieldIndex = fieldName.getFieldDefinition().getIndex();
        RegisterOffset registerOffset = new RegisterOffset(fieldIndex, register);
        if (this.initialization instanceof NoInitialization) {
            DVal defaultVal = new ImmediateInteger(0);
            Type type = fieldName.getType();
            if (type.isFloat()) {
                defaultVal = new ImmediateFloat(0.0f);
            } else if(type.isInt() || type.isBoolean()) {
                defaultVal = new ImmediateInteger(0);
            } else if(type.isClass()) {
                defaultVal = new NullOperand();
            }
            compiler.addInstruction(new LOAD(defaultVal, Register.R0));
            compiler.addInstruction(new STORE(Register.R0, registerOffset));
        }
        if (registerManager.areRegistersAvailable()) {
            registerManager.incrementCurrentUsableRegister();
            this.initialization.codeGenInitialization(compiler, registerOffset);
            registerManager.decrementCurrentUsableRegister();
        } else {
            compiler.addInstruction(new PUSH(register, compiler.getStackManager()));
            this.initialization.codeGenInitialization(compiler, registerOffset);
            compiler.addInstruction(new POP(register, compiler.getStackManager()));
        }
        RegisterOffset fieldOffset = new RegisterOffset(-2, Register.LB);
        this.fieldName.getFieldDefinition().setOperand(fieldOffset);
    }

    @Override
    public void decompile(IndentPrintStream s) {

        if (visibility == Visibility.PROTECTED) {
            s.print("protected ");
        }
        this.type.decompile(s);
        s.print(' ');
        this.fieldName.decompile(s);
        this.initialization.decompile(s);
        s.println(";");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        fieldName.iter(f);
        initialization.iter(f);
    }

    @Override
    protected String prettyPrintNode() {
        return "[visibility=" + visibility + "] DeclField";
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        fieldName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }
}
