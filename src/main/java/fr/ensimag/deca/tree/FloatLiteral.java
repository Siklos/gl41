package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;

import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.ImmediateFloat;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.apache.commons.lang.Validate;

/**
 * Single precision, floating-point literal
 *
 * @author gl41
 * @date 01/01/2020
 */
public class FloatLiteral extends AbstractExpr implements AbstractTerminalExpr {

    public float getValue() {
        return value;
    }

    private float value;

    @Override
    public boolean isFloatLiteral() {
        return true;
    }

    public FloatLiteral(float value) {
        Validate.isTrue(!Float.isInfinite(value),
                "literal values cannot be infinite");
        Validate.isTrue(!Float.isNaN(value),
                "literal values cannot be NaN");
        this.value = value;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        SymbolTable fact = new SymbolTable();
        Type _float = new FloatType(fact.create("float"));
        this.setType(_float);
        return _float;        
    }


    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int index = registerManager.getCurrentTopRegister();
        compiler.addInstruction(new LOAD(new ImmediateFloat(this.getValue()), Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager) {
        return new ImmediateFloat(this.getValue());
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(java.lang.Float.toHexString(value));
    }
    
    @Override
    protected void codeGenPrint(DecacCompiler compiler) {
        compiler.addInstruction(new LOAD(new ImmediateFloat(getValue()), 
                                                                   Register.R1));
        compiler.addInstruction(new WFLOAT());
    }

    @Override
    protected void codeGenPrintx(DecacCompiler compiler) {
        compiler.addInstruction(new LOAD(new ImmediateFloat(getValue()),
                Register.R1));
        compiler.addInstruction(new WFLOATX());
    }

    @Override
    String prettyPrintNode() {
        return "Float (" + getValue() + ")";
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
