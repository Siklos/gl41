package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;

/**
 * Conversion of an int into a float. Used for implicit conversions.
 * 
 * @author gl41
 * @date 01/01/2020
 */
public class ConvFloat extends AbstractUnaryExpr {
    public ConvFloat(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        SymbolTable fact = new SymbolTable();
        Type floatType = new FloatType(fact.create("float"));
        this.setType(floatType);
        return floatType;
    }


    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = this.getOperand().codeGenExpr(compiler, registerManager);
        compiler.addInstruction(new FLOAT(register, register));
        return register;
    }

    public static Type verifyDomainOpArith(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass, Type type1, Type type2, AbstractBinaryExpr expr) throws  ContextualError {
        if (type1.isFloat() && type2.isInt()) {
            ConvFloat convFloat = new ConvFloat(expr.getRightOperand());
            convFloat.verifyExpr(compiler, localEnv, currentClass);
            expr.setRightOperand(convFloat);
            return new FloatType(type1.getName());
        }
        if (type2.isFloat() && type1.isInt()) {
            ConvFloat convFloat = new ConvFloat(expr.getLeftOperand());
            convFloat.verifyExpr(compiler, localEnv, currentClass);
            expr.setLeftOperand(convFloat);
            return new FloatType(type2.getName());
        }
        if (type1.isInt() && type2.isInt()) {
            Type type = new IntType(type1.getName());
            return type;
        }
        if (type2.isFloat() && type1.isFloat()) {
            Type type = new FloatType(type2.getName());
            return type;
        }
        throw new ContextualError("Type must be an Integer to be converted to Float)", expr.getLocation());
    }

    @Override
    protected String getOperatorName() {
        return "/* conv float */";
    }

}
