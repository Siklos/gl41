package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.log4j.Logger;

/**
 * Assignment, i.e. lvalue = expr.
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Assign extends AbstractBinaryExpr {
    private static final Logger LOG = Logger.getLogger(Main.class);

    @Override
    public AbstractLValue getLeftOperand() {
        // The cast succeeds by construction, as the leftOperand has been set
        // as an AbstractLValue by the constructor.
        return (AbstractLValue)super.getLeftOperand();
    }

    public Assign(AbstractLValue leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        LOG.trace("verify Assign : start");
        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        this.setRightOperand(this.getRightOperand().verifyRValue(compiler, localEnv, currentClass, type1));
        setType(type1);
        LOG.trace("verify Assign : end");
        return type1;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        DAddr dAddr = this.getLeftOperand().codeGenExprDAddr(compiler, registerManager);
        GPRegister leftExprRegister = null;
        if (dAddr instanceof RegisterOffset) {
            Register register = ((RegisterOffset) dAddr).getRegister();
            if (register instanceof GPRegister) {
                leftExprRegister = (GPRegister) register;
            }
        } else {
            throw new DecacInternalError("Operand of Left Hand Side of Assign (variable, field or param) should be a RegisterOffset.");
        }
//        GPRegister register = this.getRightOperand().codeGenExpr(compiler);
        GPRegister register = this.getdVal(compiler, registerManager, leftExprRegister, this.getRightOperand());
        compiler.addInstruction(new STORE(register, dAddr));
        return register;
    }

    @Override
    protected String getOperatorName() {
        return "=";
    }

}
