package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.REM;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Modulo extends AbstractOpArith {

    public Modulo(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        setType(type1);
        if (type1.isInt() && type2.isInt()) {
            return type1;
        }
        throw new ContextualError("Invalid type : Integers are required for modulo operation. (3.33)", getLocation());
    }


    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExprRegister, DVal rightExpr) {
        compiler.addInstruction(new REM(rightExpr, leftExprRegister));
        if (!compiler.isNoCheck())
            compiler.addInstruction(new BOV(new Label("rest_division_by_zero")));
        return leftExprRegister;
    }

    @Override
    protected String getOperatorName() {
        return "%";
    }

}
