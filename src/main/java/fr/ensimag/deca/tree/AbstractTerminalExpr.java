package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;

/**
 * Permet de declarer des methodes communes aux classes representant des feuilles d'expressions, comme IntLiteral,
 * Identifier ou BooleanLiteral.
 *
 * Created by ensimag on 1/14/20.
 */
public interface AbstractTerminalExpr {
    DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager);
}
