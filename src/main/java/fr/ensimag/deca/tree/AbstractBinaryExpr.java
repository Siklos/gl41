package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;

import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import org.apache.commons.lang.Validate;

/**
 * Binary expressions.
 *
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractBinaryExpr extends AbstractExpr {

    public AbstractExpr getLeftOperand() {
        return leftOperand;
    }

    public AbstractExpr getRightOperand() {
        return rightOperand;
    }

    protected void setLeftOperand(AbstractExpr leftOperand) {
        Validate.notNull(leftOperand);
        this.leftOperand = leftOperand;
    }

    protected void setRightOperand(AbstractExpr rightOperand) {
        Validate.notNull(rightOperand);
        this.rightOperand = rightOperand;
    }

    private AbstractExpr leftOperand;
    private AbstractExpr rightOperand;

    public AbstractBinaryExpr(AbstractExpr leftOperand,
            AbstractExpr rightOperand) {
        Validate.notNull(leftOperand, "left operand cannot be null");
        Validate.notNull(rightOperand, "right operand cannot be null");
        Validate.isTrue(leftOperand != rightOperand, "Sharing subtrees is forbidden");
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }


    /**
     * @param compiler the compiler
     * @param registerManager the compiler's registerManager (redundant, code should be refactored)
     * @param leftExprRegister the register containing the result of the evaluation of the
     *                         left hand operand, or null if no such register is required
     * @param rightOp the right operand of the expression (redundant, code should be refactored)
     */
    protected GPRegister getdVal(DecacCompiler compiler, RegisterManager registerManager, GPRegister leftExprRegister, AbstractExpr rightOp) {
        GPRegister rightExpr;
        if (leftExprRegister == null) { // Ie leftExpr is in a known offset from LB, which only occurs when assigning to a variable
            rightExpr = rightOp.codeGenExpr(compiler, registerManager);
        } else if (registerManager.areRegistersAvailable()) {
            registerManager.incrementCurrentUsableRegister();
            rightExpr = rightOp.codeGenExpr(compiler, registerManager);
            registerManager.decrementCurrentUsableRegister();
        } else {
            compiler.addInstruction(new PUSH(leftExprRegister, compiler.getStackManager()));
            rightExpr = rightOp.codeGenExpr(compiler, registerManager);
            compiler.addInstruction(new LOAD(rightExpr, Register.R0));
            compiler.addInstruction(new POP(leftExprRegister, compiler.getStackManager()));
            rightExpr = Register.R0;
        }
        return rightExpr;
    }


    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        getLeftOperand().decompile(s);
        s.print(" " + getOperatorName() + " ");
        getRightOperand().decompile(s);
        s.print(")");
    }

    abstract protected String getOperatorName();

    @Override
    protected void iterChildren(TreeFunction f) {
        leftOperand.iter(f);
        rightOperand.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        leftOperand.prettyPrint(s, prefix, false);
        rightOperand.prettyPrint(s, prefix, true);
    }

}
