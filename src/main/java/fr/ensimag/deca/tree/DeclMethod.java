package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Line;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class DeclMethod extends AbstractDeclMethod {
    private static final Logger LOG = Logger.getLogger(Main.class);

    final private AbstractIdentifier type;
    final private AbstractIdentifier methodName;
    final private ListDeclParam parameters;
    final private AbstractMethodBody body;

    public DeclMethod(AbstractIdentifier type, AbstractIdentifier methodName, ListDeclParam parameters, AbstractMethodBody body) {
        Validate.notNull(type);
        Validate.notNull(methodName);
        Validate.notNull(parameters);
        Validate.notNull(body);
        this.type = type;
        this.methodName = methodName;
        this.parameters = parameters;
        this.body = body;
    }

    @Override
    protected boolean verifyDeclMethodSignature(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition superClass, int index) throws ContextualError{
        LOG.trace("verify DeclMethodSignature : start");

        Type returnType = this.type.verifyType(compiler);
        methodName.setType(returnType);
        Signature signature = this.parameters.verifyListDeclParamSignature(compiler);
        // On vérifie que la superClasse est correctement définie
        if (superClass != null) {
            // On vérifie que la superClasse ne possèderais pas un membre du même nom
            ExpDefinition superDefinition = superClass.getMembers().get(methodName.getName());
            if (superDefinition != null) {
                // Si il existe une définition du même nom, on vérifie qu'elle possède un définition de champ
                if (!(superDefinition.isMethod())) {
                    throw new ContextualError("Method name has been already declared in superclass but is not a method definition (2.7)", getLocation());
                }

                MethodDefinition superMethodDefinition = (MethodDefinition) superDefinition;
                Signature signature2 = superMethodDefinition.getSignature();
                if (!signature.equals(signature2)) {
                    throw new ContextualError("Signature of the method of the superclass is different " +
                            "than the signature of the current class. Please verify types and number of arguments " +
                            "of the method (2.7)", getLocation());
                }
                Type type2 = superMethodDefinition.getType();
                if (returnType.sameType(type2)) {
                    if (returnType.isClass() && type2.isClass()) {
                        ClassType classType = (ClassType) returnType;
                        ClassType classType2 = (ClassType) type2;
                        if (!classType.isSubClassOf(classType2)) {
                            throw new ContextualError("Return class is not a subclass of the method defined in superclass (2.7)", getLocation());
                        }
                    }
                    int indexSuperClassMethod = superMethodDefinition.getIndex();
                    MethodDefinition methodDefinition = new MethodDefinition(returnType, getLocation(), signature, indexSuperClassMethod);
                    try {
                        localEnv.declare(this.methodName.getName(), methodDefinition);
                    } catch (EnvironmentExp.DoubleDefException e) {
                        throw new ContextualError("The method "+ methodName.getName() + " is already defined (2.7)", getLocation());
                    }
                    this.methodName.setDefinition(methodDefinition);
                    LOG.trace("verify DeclMethodSignature : end");
                    return true;
                } else {
                    throw new ContextualError("Return type does not match the expected return type (2.7)", getLocation());
                }
            }
        }
        MethodDefinition methodDefinition = new MethodDefinition(returnType, getLocation(), signature, index);
        try {
            localEnv.declare(this.methodName.getName(), methodDefinition);
            LOG.debug("New method declared ");
        } catch (EnvironmentExp.DoubleDefException e) {
            throw new ContextualError("The method "+ methodName.getName() + " is already defined ", getLocation());
        }
        this.methodName.setDefinition(methodDefinition);
        LOG.trace("verify DeclMethodSignature : end");
        return false;
    }

    public AbstractIdentifier getMethodName() {
        return methodName;
    }

    @Override
    protected void verifyDeclMethod(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type returnType = this.type.verifyType(compiler);
        EnvironmentExp environmentExpParams = this.parameters.verifyListDeclParam(compiler);
        environmentExpParams.setParentEnvironment(localEnv);
        this.body.verifyMethodBody(compiler, environmentExpParams, currentClass, returnType);
    }

    @Override
    protected void codeGenDeclMethod(DecacCompiler compiler) {
        MethodDefinition methodDefinition = methodName.getMethodDefinition();
        compiler.getLabelManager().setCurrentMethod(methodDefinition);
        compiler.addLabel(LabelManager.getCodeLabel(methodDefinition));


        // Code gen Param
        this.parameters.codeGenListDeclParam(compiler);

        // Method body
        this.body.codeGenMethodBody(compiler, type.getDefinition().getType());


    }

    @Override
    public void decompile(IndentPrintStream s) {
        this.type.decompile(s);
        s.print(' ');
        this.methodName.decompile(s);
        s.print('(');
        this.parameters.decompile(s);
        s.print(") ");
        this.body.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        methodName.prettyPrint(s, prefix, false);
        parameters.prettyPrint(s, prefix, false);
        body.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        methodName.iter(f);
        parameters.iter(f);
        body.iter(f);
    }
}
