package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentType;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.instructions.*;
import fr.ensimag.ima.pseudocode.*;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * Deca complete program (class definition plus main block)
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Program extends AbstractProgram {
    private static final Logger LOG = Logger.getLogger(Program.class);
    
    public Program(ListDeclClass classes, AbstractMain main) {
        Validate.notNull(classes);
        Validate.notNull(main);
        this.classes = classes;
        this.main = main;
    }
    public ListDeclClass getClasses() {
        return classes;
    }
    public AbstractMain getMain() {
        return main;
    }
    private ListDeclClass classes;
    private AbstractMain main;

    @Override
    public void verifyProgram(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify program: start");
        EnvironmentType env_types_predef = new EnvironmentType(null);
        this.classes.verifyListClass(compiler);
        this.classes.verifyListClassMembers(compiler);
        this.classes.verifyListClassBody(compiler);
        this.main.verifyMain(compiler);
        LOG.trace("verify program: end");
    }

    @Override
    public void codeGenProgram(DecacCompiler compiler) {
        // A FAIRE: compléter ce squelette très rudimentaire de code
        compiler.addComment("Main program");
        if (!compiler.isNoCheck())
            compiler.addTSTOLine();
        classes.codeGenListDeclClassPasse1(compiler);
        main.codeGenMain(compiler);
        if (!compiler.isNoCheck())
            compiler.setTSTO(compiler.getStackManager().getMaxStackAllocation());
        compiler.addInstruction(new HALT());
        classes.codeGenListDeclClassPasse2(compiler);
        compiler.addComment("Error messages");
        this.addLabelError("stack_overflow", "Error: Stack overflow", compiler);
        this.addLabelError("division_by_zero", "Error: Integer division by zero", compiler);
        this.addLabelError("float_division_by_zero", "Error: Float division by zero", compiler);
        this.addLabelError("rest_division_by_zero", "Error: Rest of integer division by zero", compiler);
        this.addLabelError("overflow", "Error: Overflow during arithmetic operation", compiler);
        this.addLabelError("impossible_convert_to_float", "Error: Impossible to convert into float", compiler);
        this.addLabelError("readInt_error", "Error: You must write an integer which don't create arithmetic overflow", compiler);
        this.addLabelError("readFloat_error", "Error: You must write a float which don't create arithmetic overflow", compiler);
        this.addLabelError("cast_error", "Error: Illegal downcast is not allowed", compiler);
        this.addLabelError("non_void_return_error", "Error: return must be called at the end of a non-void function", compiler);
        this.addLabelError("null_dereferencing", "Error: null pointer exception", compiler);
        this.addLabelError("heap_overflow", "Error: Heap overflow", compiler);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        getClasses().decompile(s);
        getMain().decompile(s);
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        classes.iter(f);
        main.iter(f);
    }
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        classes.prettyPrint(s, prefix, false);
        main.prettyPrint(s, prefix, true);
    }
    
    private void addLabelError(String labelName, String errorMessage, DecacCompiler compiler) {
    	compiler.addLabel(new Label(labelName));
        compiler.addInstruction(new WSTR(errorMessage));
        compiler.addInstruction(new WNL());
        compiler.addInstruction(new ERROR());
    }
}
