package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.Label;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Or extends AbstractOpBool {

    public Or(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        if (expectedValue) {
            ((BooleanExpression) getLeftOperand()).codeBooleanExpression(compiler, registerManager, true, branchLabel);
            ((BooleanExpression) getRightOperand()).codeBooleanExpression(compiler, registerManager, true, branchLabel);

        } else {
            int idCond = compiler.getLabelManager().useLabelCounter();
            Label labelE_End = new Label("E_End." + idCond);
            ((BooleanExpression) getLeftOperand()).codeBooleanExpression(compiler, registerManager, true, labelE_End);
            ((BooleanExpression) getRightOperand()).codeBooleanExpression(compiler, registerManager, false, branchLabel);
            compiler.addLabel(labelE_End);
        }
    }

    @Override
    protected String getOperatorName() {
        return "||";
    }


}
