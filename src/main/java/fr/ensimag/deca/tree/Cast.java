package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;
import fr.ensimag.ima.pseudocode.instructions.INT;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class Cast extends AbstractExpr {
    private static final Logger LOG = Logger.getLogger(Cast.class);

    private AbstractIdentifier type;
    private AbstractExpr expression;

    public Cast(AbstractIdentifier type, AbstractExpr expression) {
        Validate.notNull(type);
        Validate.notNull(expression);
        this.type = type;
        this.expression = expression;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify Cast : start");
        Type type2 = this.type.verifyType(compiler);
        Type type1 = this.expression.verifyExpr(compiler, localEnv, currentClass);
        setType(type2);
        if (type1.isVoid()) {
            throw new ContextualError("Void type is not castable (cast_compatible, 3.39)", getLocation());
        }
        if (type1.isAssignableFrom(type2) || type2.isAssignableFrom(type1)) {
            LOG.trace("verify Cast : end");
            return type2;
        }
        throw new ContextualError("Object is not castable (3.39)", getLocation());
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = expression.codeGenExpr(compiler);
        Type type2 = this.getType();
        Type type1 = expression.getType();
        if (type2.isInt() && type1.isFloat()) {
            compiler.addInstruction(new INT(register, register));
        } else if (type2.isFloat() && type1.isInt()) {
            compiler.addInstruction(new FLOAT(register, register));
        } else if (type2.isClass() && type1.isClass()) {
            ClassType classType2 = (ClassType) type2;
            ClassType classType1 = (ClassType) type1;
            if (!(classType2.isAssignableFrom(classType1))) { // Le type statique de expr n'hérite pas le type de cast

                Label labelFalse = new Label("cast_error");
                int indexLabel = compiler.getLabelManager().useLabelCounter();
                Label labelTrue = new Label("cast." + indexLabel);
                InstanceOf.doAssembly(compiler, register, type.getClassDefinition(), labelTrue, labelFalse, indexLabel);
                compiler.addLabel(labelTrue);
            }
        }


        return register;
    }

    @Override
    public void decompile(IndentPrintStream s) {
    	s.print("(");
        this.type.decompile(s);
        s.print(") (");
        this.expression.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        expression.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.type.iter(f);
        this.expression.iter(f);
    }
}
