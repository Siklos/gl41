package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

public class DeclParam extends AbstractDeclParam {
    final private AbstractIdentifier type;
    final private AbstractIdentifier paramName;

    public DeclParam(AbstractIdentifier type, AbstractIdentifier paramName) {
        Validate.notNull(type);
        Validate.notNull(paramName);
        this.type = type;
        this.paramName = paramName;
    }

    @Override
    protected Type verifyDeclParamSignature(DecacCompiler compiler) throws ContextualError {
        Type type = this.type.verifyType(compiler);
        if (type.isVoid()) {
            throw new ContextualError("Expected type other than void (2.9)", getLocation());
        }
        return type;
    }

    @Override
    protected void verifyDeclParam(DecacCompiler compiler, EnvironmentExp environmentParams) throws ContextualError {
        Type type = this.type.verifyType(compiler);
        ParamDefinition paramDefinition = new ParamDefinition(type, getLocation());
        try {
            paramName.setDefinition(paramDefinition);
            environmentParams.declare(paramName.getName(), paramDefinition);
        } catch (EnvironmentExp.DoubleDefException exception) {
            throw new ContextualError("Parameter is already defined", getLocation());
        }
    }

    @Override
    protected void codeGenDeclParam(DecacCompiler compiler, int index) {
        this.paramName.getParamDefinition().setOperand(new RegisterOffset(index, Register.LB));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        this.type.decompile(s);
        s.print(' ');
        this.paramName.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        paramName.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        paramName.iter(f);
    }
}
