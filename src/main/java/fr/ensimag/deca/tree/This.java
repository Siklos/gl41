package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

public class This extends AbstractExpr{

    // Pour This, l’attribut est vrai si et seulement si le nœud a été ajouté pendant l’analyse
    // syntaxique sans que le programme source ne contienne le mot clé this (par exemple, m(); pour dire
    // this.m();). Cet attribut n’est pas strictement nécessaire, mais il est utilisé dans la décompilation
    // (cf. section [Decompilation]).
    private boolean tokenHidden;
    private ClassDefinition classDefinition;

    public This(boolean tokenHidden) {
        this.tokenHidden = tokenHidden;
    }

    public boolean isTokenHidden() {
        return tokenHidden;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        if (currentClass == null) {
            throw new ContextualError("Class must exist to use the keyword : this (3.43)", getLocation());
        }
        ClassType classType = currentClass.getType();
        setType(classType);
        setClassDefinition(currentClass);
        return classType;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int index = registerManager.getCurrentTopRegister();
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        if (!isTokenHidden()) {
        	s.print("this");
        }
    }
    
    @Override
    String prettyPrintNode() {
        return "This";
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
    	// leaf node => nothing to do
    }

    public void setClassDefinition(ClassDefinition classDefinition) {
        this.classDefinition = classDefinition;
    }

    public ClassDefinition getClassDefinition() {
        return classDefinition;
    }
}
