package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.OPP;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Not extends AbstractUnaryExpr implements BooleanExpression {

    public Not(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = getOperand().verifyExpr(compiler, localEnv, currentClass);
        if (type.isBoolean()) {
            setType(type);
            return type;
        }
        throw new ContextualError("Invalid type : expected boolean (3.37)", getLocation());
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = Register.getR(registerManager.getCurrentTopRegister());
        compiler.addInstruction(new LOAD(0, register));
        int idCond = compiler.getLabelManager().useLabelCounter();
        Label labelEnd = new Label("E_Not_End." + idCond);
        if (registerManager.areRegistersAvailable()) {
            registerManager.incrementCurrentUsableRegister();
            this.codeBooleanExpression(compiler, registerManager, false, labelEnd);
            registerManager.decrementCurrentUsableRegister();
        } else {
            compiler.addInstruction(new PUSH(register, compiler.getStackManager()));
            this.codeBooleanExpression(compiler, registerManager, false, labelEnd);
            compiler.addInstruction(new POP(register, compiler.getStackManager()));
        }
        compiler.addInstruction(new LOAD(1, register));
        compiler.addLabel(labelEnd);
        return register;
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        ((BooleanExpression) getOperand()).codeBooleanExpression(compiler, registerManager, !expectedValue, branchLabel);
    }

    @Override
    protected String getOperatorName() {
        return "!";
    }
}
