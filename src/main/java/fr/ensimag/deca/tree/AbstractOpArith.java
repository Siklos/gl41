package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import org.apache.log4j.Logger;

/**
 * Arithmetic binary operations (+, -, /, ...)
 * 
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractOpArith extends AbstractBinaryExpr {
    private static final Logger LOG = Logger.getLogger(Main.class);

    public AbstractOpArith(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify OpArith : start");
        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);

        Type convertedType = ConvFloat.verifyDomainOpArith(compiler, localEnv, currentClass, type1, type2, this);
        this.setType(convertedType);
        return convertedType;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister leftExprRegister = this.getLeftOperand().codeGenExpr(compiler, registerManager);
        DVal rightExpr;
        AbstractExpr rightOp = this.getRightOperand();
        if (rightOp.isIdentifier() || rightOp.isIntLiteral() || rightOp.isFloatLiteral()) {
            rightExpr = ((AbstractTerminalExpr) rightOp).codeGenExprDVal(compiler, registerManager);
        } else {
            rightExpr = this.getdVal(compiler, registerManager, leftExprRegister, rightOp);
        }
        return this.codeGenOperator(compiler, leftExprRegister, rightExpr);
    }



    protected abstract GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExpressionRegister,
                                                  DVal rightExpr);
}
