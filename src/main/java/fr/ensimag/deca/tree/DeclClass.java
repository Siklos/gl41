package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

/**
 * Declaration of a class (<code>class name extends superClass {members}<code>).
 * 
 * @author gl41
 * @date 01/01/2020
 */
public class DeclClass extends AbstractDeclClass {
    private static final Logger LOG = Logger.getLogger(DeclClass.class);

    private AbstractIdentifier className;
    private AbstractIdentifier superClassName;
    private ListDeclField fields;
    private ListDeclMethod methods;

    public DeclClass(AbstractIdentifier className, AbstractIdentifier superClassName, ListDeclField fields, ListDeclMethod methods) {
        Validate.notNull(className);
        Validate.notNull(fields);
        Validate.notNull(methods);
        this.className = className;
        if (superClassName == null) {
            SymbolTable fact = new SymbolTable();
            SymbolTable.Symbol objectSymbol = fact.create("Object");
            superClassName = new Identifier(objectSymbol);
        }
        this.superClassName = superClassName;
        this.fields = fields;
        this.methods = methods;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("class ");
        className.decompile(s);
        s.print(" extends ");
        superClassName.decompile(s);
        s.println(" {");
        s.indent();
        fields.decompile(s);
        methods.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void verifyClass(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify Class : start");
        EnvironmentType environmentType = compiler.getEnvironmentType();
        SymbolTable.Symbol name = className.getName();


        Definition superDefinition = environmentType.get(superClassName.getName());

        if (superDefinition == null) {
            throw new ContextualError("Specified extended class is not declared (1.3)", getLocation());
        }

        if (!(superDefinition).isClass()) {
            throw new ContextualError("Specified extended class is not declared as a class (1.3)", getLocation());
        }

        ClassDefinition superClassDefinition = (ClassDefinition) superDefinition;
        this.superClassName.setDefinition(superClassDefinition);
        if (environmentType.get(className.getName()) != null) {
            throw new ContextualError("Class is already defined (1.3)", getLocation());
        }

        ClassDefinition classDefinition = new ClassDefinition(new ClassType(name, getLocation(), superClassDefinition), getLocation(), superClassDefinition);
        this.className.setDefinition(classDefinition);
        try {
            environmentType.declare(name, classDefinition);
        } catch (EnvironmentType.DoubleDefException e) {
            throw new ContextualError("The class "+ name.getName() + "is already defined (1.3)", getLocation());
        }
        LOG.debug("Decl new class : " + name);
        LOG.trace("verify Class : end");
    }

    @Override
    protected void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError {
        LOG.trace("verify Class Members : start");
        EnvironmentType environmentType = compiler.getEnvironmentType();
        Definition superDefinition = environmentType.get(superClassName.getName());
        if (!(superDefinition.isClass())) {
            throw new ContextualError("Specified extended class is not declared as a class (2.3)", getLocation());
        }

        ClassDefinition superClassDefinition = (ClassDefinition) superDefinition;
        ClassDefinition classDefinition = (ClassDefinition) compiler.getEnvironmentType().get(className.getName());
        EnvironmentExp environmentExp = classDefinition.getMembers();
        int nbField = this.fields.verifyListDeclFieldMembers(compiler, environmentExp, superClassDefinition, className.getClassDefinition());
        int nbMethods = this.methods.verifyListDeclMethodSignature(compiler, environmentExp, superClassDefinition);
        EnvironmentExp environmentExpSuper = superClassDefinition.getMembers();
        environmentExp.empilement(environmentExpSuper);

        classDefinition.setNumberOfFields(nbField);
        classDefinition.setNumberOfMethods(nbMethods);
        LOG.debug("Update class " + className.toString() + " : " + environmentExp);
        LOG.trace("verify Class Members : end");


    }
    
    @Override
    protected void verifyClassBody(DecacCompiler compiler) throws ContextualError {
        ClassDefinition classDefinition = className.getClassDefinition();
        Definition definitionDeclared = compiler.getEnvironmentType().get(className.getName());
        if (!definitionDeclared.isClass()) {
            throw new ContextualError("Specified class is not declared as a class definition (3.5)", getLocation());
        }
        ClassDefinition classDefinitionDeclared = (ClassDefinition) definitionDeclared;
        EnvironmentExp environmentExp = classDefinitionDeclared.getMembers();
        this.fields.verifyListDeclField(compiler, environmentExp, classDefinition);
        this.methods.verifyListDeclMethod(compiler, environmentExp, classDefinition);
    }

    protected void codeGenDeclClassPasse1(DecacCompiler compiler) {
        for (AbstractDeclMethod method : methods.getList()) {
            MethodDefinition def = method.getMethodName().getMethodDefinition();
            def.setLabel(new Label("code." + className.getName().getName() + "." + method.getMethodName().getName()));
        }

        compiler.addComment("Class definition : " + className.getName().getName());
        RegisterOffset reg = compiler.getStackManager().getClassRegisterOffset(superClassName.getClassDefinition());
        compiler.addInstruction(new LEA(reg, Register.R0));

        this.methods.codeGenVirtualMethodTable(compiler, className.getClassDefinition());
    }

    protected void codeGenDeclClassPasse2(DecacCompiler compiler) {
        if (className.getClassDefinition().getNumberOfFields() > 0) {
            this.fields.codeGenListDeclField(compiler, className.getClassDefinition(), superClassName.getClassDefinition());
        }
        this.methods.codeGenListDeclMethod(compiler);
    }
        @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        this.className.prettyPrint(s, prefix, false);
        superClassName.prettyPrint(s, prefix, false);
        fields.prettyPrint(s, prefix, false);
        methods.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        className.iter(f);
        superClassName.iter(f);
        fields.iter(f);
        methods.iter(f);
    }

}
