package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import java.io.PrintStream;

import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import static java.lang.System.exit;

/**
 * Deca Identifier
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Identifier extends AbstractIdentifier implements AbstractTerminalExpr, BooleanExpression {
    private static final Logger LOG = Logger.getLogger(Main.class);

    @Override
    protected void checkDecoration() {
        if (getDefinition() == null) {
            throw new DecacInternalError("Identifier " + this.getName() + " has no attached Definition");
        }
    }

    @Override
    public Definition getDefinition() {
        return definition;
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * ClassDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a class definition.
     */
    @Override
    public ClassDefinition getClassDefinition() {
        try {
            return (ClassDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a class identifier, you can't call getClassDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * MethodDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a method definition.
     */
    @Override
    public MethodDefinition getMethodDefinition() {
        try {
            return (MethodDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a method identifier, you can't call getMethodDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * FieldDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public FieldDefinition getFieldDefinition() {
        try {
            return (FieldDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a field identifier, you can't call getFieldDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * VariableDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public VariableDefinition getVariableDefinition() {
        try {
            return (VariableDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a variable identifier, you can't call getVariableDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a ExpDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public ExpDefinition getExpDefinition() {
        try {
            return (ExpDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a Exp identifier, you can't call getExpDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a ParamDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public ParamDefinition getParamDefinition() {
        try {
            return (ParamDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a Param identifier, you can't call getParamDefinition on it");
        }
    }




    @Override
    public void setDefinition(Definition definition) {
        this.definition = definition;
    }

    @Override
    public Symbol getName() {
        return name;
    }

    private Symbol name;

    public Identifier(Symbol name) {
        Validate.notNull(name);
        this.name = name;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        LOG.trace("verify Identifier (expr): start");

        ExpDefinition expDefinition = localEnv.getRecursive(this.getName());
        if (expDefinition == null) {
            throw new ContextualError("Variable is not declared : " + this.getName().getName() + " (0.1)\nCurrent env :" + localEnv.toString(), getLocation());
        }


        this.setDefinition(expDefinition);
        Type type = expDefinition.getType();
        LOG.trace("verify Identifier (expr): end");
        setType(type);
        return type;
    }

    /**
     * Implements non-terminal "type" of [SyntaxeContextuelle] in the 3 passes
     * @param compiler contains "env_types" attribute
     */
    @Override
    public Type verifyType(DecacCompiler compiler) throws ContextualError {

        LOG.trace("verify Identifier (type): start");
        TypeDefinition typeDefinition = compiler.getEnvironmentType().get(this.getName());
        if (typeDefinition == null) {
            throw new ContextualError("Type is undefined (0.2)", getLocation());
        }
        this.setDefinition(typeDefinition);
        setType(typeDefinition.getType());
        LOG.trace("verify Identifier (type): end");
        return typeDefinition.getType();
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        DAddr addr = getExpDefinition().getOperand();
        compiler.addInstruction(new LOAD(addr, Register.R0));
        compiler.addInstruction(new CMP(0, Register.R0));
        if (expectedValue) {
            compiler.addInstruction(new BNE(branchLabel));
        } else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }

    @Override
    public boolean isIdentifier() {
        return true;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int index = registerManager.getCurrentTopRegister();
        DVal addr = this.codeGenExprDVal(compiler, registerManager);
        compiler.addInstruction(new LOAD(addr, Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager) {
        assert this.getDefinition().isExpression();
        return codeGenExprDAddr(compiler, registerManager);

    }

    @Override
    public DAddr codeGenExprDAddr(DecacCompiler compiler, RegisterManager registerManager) {
        DAddr addr = this.getExpDefinition().getOperand();
        if (this.getDefinition().isField()) { // Ce code ne peut pas etre atteint.
            int fieldIndex = this.getFieldDefinition().getIndex();
            compiler.addInstruction(new LOAD(addr, Register.R1));
            return new RegisterOffset(fieldIndex, Register.R1);
        }
        return addr;
    }

    @Override
    protected void codeGenPrint(DecacCompiler compiler) {
        GPRegister addr = this.codeGenExpr(compiler);
        compiler.addInstruction(new LOAD(addr, Register.R1));
        Type type = this.getType();
        if (type.isFloat()) {
            compiler.addInstruction(new WFLOAT());
        } else if (type.isInt()) {
            compiler.addInstruction(new WINT());
        } else {
            exit(1);
        }
    }

    @Override
    protected void codeGenPrintx(DecacCompiler compiler) {
        if (this.getType().isFloat()) {
            DAddr addr = this.getExpDefinition().getOperand();
            compiler.addInstruction(new LOAD(addr, Register.R1));
            compiler.addInstruction(new WFLOATX());
        } else {
            this.codeGenPrint(compiler);
        }

    }

    private Definition definition;


    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(name.toString());
    }

    @Override
    String prettyPrintNode() {
        return "Identifier (" + getName() + ")";
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Definition d = getDefinition();
        if (d != null) {
            s.print(prefix);
            s.print("definition: ");
            s.print(d);
            s.println();
        }
    }

}
