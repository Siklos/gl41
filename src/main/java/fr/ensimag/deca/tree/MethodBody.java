package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Line;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class MethodBody extends AbstractMethodBody {
    private static final Logger LOG = Logger.getLogger(Main.class);


    private ListDeclVar declVariables;
    private ListInst insts;

    public MethodBody(ListDeclVar declVariables,
                ListInst insts) {
        Validate.notNull(declVariables);
        Validate.notNull(insts);
        this.declVariables = declVariables;
        this.insts = insts;
    }

    @Override
    protected void verifyMethodBody(DecacCompiler compiler, EnvironmentExp environmentExpParams, ClassDefinition currentClass, Type returnType) throws ContextualError {
        LOG.trace("verify Bloc Method: start");
        declVariables.verifyListDeclVariable(compiler, environmentExpParams, currentClass);
        insts.verifyListInst(compiler, environmentExpParams, currentClass, returnType);
        LOG.trace("verify Bloc Method: end");
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler, Type returnType) {
        StackManager stackManager = compiler.getStackManager();
        RegisterManager registerManager = compiler.getRegisterManager();
        registerManager.resetMaxUsedRegister();

        stackManager.resetNextVariableOffset();
        int oldCurrentSp = stackManager.getNextAllocationOffset();
        stackManager.setMaxStackAllocation(oldCurrentSp - 1);

        if (!compiler.isNoCheck())
            compiler.addTSTOLine();

        int sp = declVariables.size();

        compiler.getStackManager().allocate(sp);
        compiler.addADDSPLine();
        compiler.setADDSP(sp);

        // Sauvegarde des registres
        int lineIndex = compiler.getLinesSize();

        declVariables.codeGenListDeclVariables(compiler);
        insts.codeGenListInst(compiler);

        // fin.
        if (!returnType.isVoid()) {
            compiler.addInstruction(new BRA(new Label("non_void_return_error")));
        }
        compiler.addLabel(LabelManager.getFinLabel(compiler.getLabelManager().getCurrentMethod()));

        registerManager.saveAndRestoreRegisters(compiler, lineIndex);

        if (sp > 0)
            compiler.addInstruction(new SUBSP(sp));

        if (!compiler.isNoCheck()) {
            int maxSp = compiler.getStackManager().getMaxStackAllocation() + registerManager.getMaxUsedRegister() - 1;
            compiler.setTSTO(maxSp - (oldCurrentSp - 1));
        }
        compiler.getStackManager().setNextAllocationOffset(oldCurrentSp);
        compiler.addInstruction(new RTS());
    }

    @Override
    public int getNumberOfVariables() {
        return declVariables.size();
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.println("{");
        s.indent();
        declVariables.decompile(s);
        insts.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        declVariables.iter(f);
        insts.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        declVariables.prettyPrint(s, prefix, false);
        insts.prettyPrint(s, prefix, true);
    }
}
