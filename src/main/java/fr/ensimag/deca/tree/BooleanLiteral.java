package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class BooleanLiteral extends AbstractExpr implements AbstractTerminalExpr, BooleanExpression {

    private boolean value;

    public BooleanLiteral(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public boolean isBooleanLiteral() {
        return true;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        SymbolTable fact = new SymbolTable();
        Type _boolean = new BooleanType(fact.create("boolean"));
        this.setType(_boolean);
        return _boolean;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int regValue = value ? 1 : 0;
        int index = registerManager.getCurrentTopRegister();
        compiler.addInstruction(new LOAD(regValue, Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(Boolean.toString(value));
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    String prettyPrintNode() {
        return "BooleanLiteral (" + value + ")";
    }

    @Override
    public DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager) {
        return new ImmediateInteger(this.getValue() ? 1 : 0);
    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        if (getValue() == expectedValue) {
            compiler.addInstruction(new BRA(branchLabel));
        }
    }
}
