package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.DVal;

/**
 * @author gl41
 * @date 01/01/2020
 */
public class Plus extends AbstractOpArith {
    public Plus(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExpressionRegister, DVal rightExpr) {
        compiler.addInstruction(new ADD(rightExpr, leftExpressionRegister));
        if (!compiler.isNoCheck())
            compiler.addInstruction(new BOV(new Label("overflow")));
        return leftExpressionRegister;
    }

    @Override
    protected String getOperatorName() {
        return "+";
    }
}
