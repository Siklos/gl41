package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.log4j.Logger;

import java.util.HashSet;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractOpCmp extends AbstractBinaryExpr implements BooleanExpression {
    private static final Logger LOG = Logger.getLogger(Main.class);

    public AbstractOpCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        LOG.trace("verify OpCmp : start");
        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);

        HashSet<String> operators = new HashSet<>();
        operators.add("==");
        operators.add("!=");
        operators.add("<=");
        operators.add(">=");
        operators.add(">");
        operators.add("<");

        if (!operators.contains(this.getOperatorName())) {
            throw new ContextualError("Operator is not defined for any class. (3.33)", getLocation());
        }
        SymbolTable factory = new SymbolTable();

        Type booleanType = compiler.getEnvironmentType().get(factory.create("boolean")).getType();
        setType(booleanType);
        if (((type1.isInt() || type1.isFloat()) && (type2.isInt() || type2.isFloat()))) {
            ConvFloat.verifyDomainOpArith(compiler, localEnv, currentClass, type1, type2, this);
            LOG.trace("verify OpCmp : end");
            return booleanType;
        } else {
            if (getOperatorName() == "==" || getOperatorName() == "!=") {
                if (type1.isClassOrNull() && type2.isClassOrNull()) {
                    LOG.trace("verify OpCmp : end");
                    return booleanType;
                } else {
                    if (type1.isBoolean()) {
                        if (type2.isBoolean()) {
                            LOG.trace("verify OpCmp : end");
                            return booleanType;
                        }
                        throw new ContextualError("Expression : " + this.getLeftOperand().decompile()
                                + " is not comparable : "
                                + this.getRightOperand().decompile() +" (3.33)", getLocation());
                    }
                    throw new ContextualError("Expression : " + this.getLeftOperand().decompile() + " is not comparable : " + this.getRightOperand().decompile() +" (3.33)", getLocation());
                }
            }
            throw new ContextualError("Incorrect operator (3.33)", getLocation());
        }
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = Register.getR(registerManager.getCurrentTopRegister());
        compiler.addInstruction(new LOAD(1, register));
        int idCond = compiler.getLabelManager().useLabelCounter();
        Label labelEnd = new Label("E_Bool_End." + idCond);
        if (registerManager.areRegistersAvailable()) {
            registerManager.incrementCurrentUsableRegister();
            this.codeBooleanExpression(compiler, registerManager, true, labelEnd);
            registerManager.decrementCurrentUsableRegister();
        } else {
            compiler.addInstruction(new PUSH(register, compiler.getStackManager()));
            this.codeBooleanExpression(compiler, registerManager, true, labelEnd);
            compiler.addInstruction(new POP(register, compiler.getStackManager()));
        }
        compiler.addInstruction(new LOAD(0, register));
        compiler.addLabel(labelEnd);
        return register;

    }

    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        GPRegister leftExprRegister = this.getLeftOperand().codeGenExpr(compiler, registerManager);
        DVal rightExpr;
        AbstractExpr rightOp = this.getRightOperand();
        if (rightOp.isIdentifier() || rightOp.isIntLiteral() || rightOp.isFloatLiteral() || rightOp.isBooleanLiteral()) {
            rightExpr = ((AbstractTerminalExpr) rightOp).codeGenExprDVal(compiler, registerManager);
        } else {
            rightExpr = this.getdVal(compiler, registerManager, leftExprRegister, rightOp);
        }
        compiler.addInstruction(new CMP(rightExpr, leftExprRegister));
    }

    protected abstract GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExpressionRegister,
                                                  DVal rightExpr);

}
