package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;

import java.util.ListIterator;

import org.apache.log4j.Logger;

public class ListDeclParam extends TreeList<AbstractDeclParam> {
    private static final Logger LOG = Logger.getLogger(Main.class);

    @Override
    public void decompile(IndentPrintStream s) {
    	ListIterator<AbstractDeclParam> iter = this.getList().listIterator();
    	while (iter.hasNext()) {
    		iter.next().decompile(s);
    		if (iter.hasNext()) {
    			s.print(",");
    		}
    	}
    }

    /**
     * Pass 2 of [SyntaxeContextuelle]
     */
    Signature verifyListDeclParamSignature(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify ListDeclParam Signatures: start");
        Signature signature = new Signature();
        for (AbstractDeclParam declParam : getList()) {
            Type type = declParam.verifyDeclParamSignature(compiler);
            signature.add(type);
        }
        LOG.trace("verify ListDeclParam Signatures: end");
        return signature;
    }

    /**
     * Pass 3 of [SyntaxeContextuelle]
     */
    EnvironmentExp verifyListDeclParam(DecacCompiler compiler) throws ContextualError {
        EnvironmentExp environmentParams = new EnvironmentExp(null);
        LOG.trace("verify ListDeclParam: start");
        for (AbstractDeclParam declParam : getList()) {
            declParam.verifyDeclParam(compiler, environmentParams);
        }
        LOG.trace("verify ListDeclParam: end");
        return  environmentParams;
    }

    void codeGenListDeclParam(DecacCompiler compiler) {
        int cpt = -3;
        for (AbstractDeclParam declParam : getList()) {
            declParam.codeGenDeclParam(compiler, cpt);
            cpt--;
        }
    }
}
