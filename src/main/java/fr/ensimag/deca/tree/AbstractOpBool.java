package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.log4j.Logger;

import java.util.HashSet;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractOpBool extends AbstractBinaryExpr implements BooleanExpression {
    private static final Logger LOG = Logger.getLogger(Main.class);

    public AbstractOpBool(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        LOG.trace("verify OpBool : start");

        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);

        HashSet<String> operators = new HashSet<>();
        operators.add("||");
        operators.add("&&");

        if (!operators.contains(this.getOperatorName())) {
            throw new ContextualError("Operator is not defined for any class. (3.33)", getLocation());
        }

        if (!type1.isBoolean() || !type2.isBoolean()) {
            throw new ContextualError("Expression is not a boolean expression (3.33)", getLocation());
        }

        LOG.trace("verify OpBool : end");
        SymbolTable factory = new SymbolTable();
        Type type = compiler.getEnvironmentType().get(factory.create("boolean")).getType();
        setType(type);
        return type;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        GPRegister register = Register.getR(registerManager.getCurrentTopRegister());
        compiler.addInstruction(new LOAD(1, register));
        int idCond = compiler.getLabelManager().useLabelCounter();
        Label labelEnd = new Label("E_Bool_End." + idCond);
        if (registerManager.areRegistersAvailable()) {
            registerManager.incrementCurrentUsableRegister();
            this.codeBooleanExpression(compiler, registerManager, true, labelEnd);
            registerManager.decrementCurrentUsableRegister();
        } else {
            compiler.addInstruction(new PUSH(register, compiler.getStackManager()));
            this.codeBooleanExpression(compiler, registerManager, true, labelEnd);
            compiler.addInstruction(new POP(register, compiler.getStackManager()));
        }
        compiler.addInstruction(new LOAD(0, register));
        compiler.addLabel(labelEnd);
        return register;
    }
}