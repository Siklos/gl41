package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.FloatType;
import fr.ensimag.deca.context.IntType;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * Print statement (print, println, ...).
 *
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractPrint extends AbstractInst {
    private static final Logger LOG = Logger.getLogger(Main.class);

    private boolean printHex;
    private ListExpr arguments = new ListExpr();
    
    abstract String getSuffix();

    public AbstractPrint(boolean printHex, ListExpr arguments) {
        Validate.notNull(arguments);
        this.arguments = arguments;
        this.printHex = printHex;
        this.setLocation(Location.BUILTIN);
    }

    public ListExpr getArguments() {
        return arguments;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        LOG.trace("verify print : start");

        HashSet<String> typesAllowed = new HashSet<>();
        typesAllowed.add("int");
        typesAllowed.add("float");
        typesAllowed.add("string");
        for (AbstractExpr expr : this.arguments.getList()) {
            Type type = expr.verifyExpr(compiler, localEnv, currentClass);
            if (!typesAllowed.contains(type.getName().getName())) {
                throw new ContextualError("Expression in a print must be : int, float or string (3.31)", getLocation());
            }
        }
        LOG.trace("verify print : end");
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        for (AbstractExpr a : getArguments().getList()) {
            if(getPrintHex()) {
                a.codeGenPrintx(compiler);
            } else {
                a.codeGenPrint(compiler);
            }
        }
    }

    private boolean getPrintHex() {
        return printHex;
    }

    @Override
    public void decompile(IndentPrintStream s) {

    	s.print("print"+this.getSuffix());
    	if (this.getPrintHex()) {
    		s.print("x");
    	}
    	s.print("(");
    	this.getArguments().decompile(s);
    	s.print(");");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        arguments.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        arguments.prettyPrint(s, prefix, true);
    }

}
