package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.log4j.Logger;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class ListDeclClass extends TreeList<AbstractDeclClass> {
    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);
    
    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclClass c : getList()) {
            c.decompile(s);
            s.println();
        }
    }

    /**
     * Pass 1 of [SyntaxeContextuelle]
     */
    void verifyListClass(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify listClass A: start");
        for (AbstractDeclClass declClass : getList()) {
            declClass.verifyClass(compiler);
        }
        LOG.trace("verify listClass A: end");
    }

    /**
     * Pass 2 of [SyntaxeContextuelle]
     */
    public void verifyListClassMembers(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify listClass B: start");
        for (AbstractDeclClass declClass : getList()) {
            declClass.verifyClassMembers(compiler);
        }
        LOG.trace("verify listClass B: end");
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle]
     */
    public void verifyListClassBody(DecacCompiler compiler) throws ContextualError {
        LOG.trace("verify listClass C: start");
        for (AbstractDeclClass declClass : getList()) {
            declClass.verifyClassBody(compiler);
        }
        LOG.trace("verify listClass C: end");
    }

    void codeGenListDeclClassPasse1(DecacCompiler compiler) {
        compiler.addComment("Method table build : start");

        this.declareEquals(compiler);
        for (AbstractDeclClass declClass : getList()) {
            declClass.codeGenDeclClassPasse1(compiler);
        }

        compiler.addComment("Method table build : end");
    }

    void codeGenListDeclClassPasse2(DecacCompiler compiler) {
        compiler.addComment("Method body : start");
        // On utilise que les registres scratch, on a pas besoin de les sauvegarder
        compiler.addLabel(new Label("code.Object.equals"));
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));
        compiler.addInstruction(new LOAD(new RegisterOffset(-3, Register.LB), Register.R1));
        compiler.addInstruction(new CMP(Register.R0, Register.R1));
        compiler.addInstruction(new SEQ(Register.R0));
        compiler.addInstruction(new RTS());

        for (AbstractDeclClass declClass : getList()) {
            declClass.codeGenDeclClassPasse2(compiler);
        }
        compiler.addComment("Method body : end");
    }

    private void declareEquals(DecacCompiler compiler) {
        SymbolTable fact = new SymbolTable();
        SymbolTable.Symbol objectSymbol = fact.create("Object");
        ClassDefinition objectClassDefinition = (ClassDefinition) compiler.getEnvironmentType().get(objectSymbol);
        EnvironmentExp objectMethods = objectClassDefinition.getMembers();
        SymbolTable.Symbol equalsSymbol = fact.create("equals");
        MethodDefinition equalsDef = (MethodDefinition) objectMethods.get(equalsSymbol);
        equalsDef.setLabel(new Label("code." + objectSymbol.getName() + "." + equalsSymbol.getName()));

        compiler.addInstruction(new LOAD(new NullOperand(), Register.R0));
        RegisterOffset reg = compiler.getStackManager().allocateClassDefinition(objectClassDefinition);
        compiler.addInstruction(new STORE(Register.R0, reg));

        compiler.addInstruction(new LOAD(new LabelOperand(LabelManager.getCodeLabel(equalsDef)), Register.R0));
        RegisterOffset methodReg = new RegisterOffset(1 + reg.getOffset(), Register.GB);
        compiler.addInstruction(new STORE(Register.R0, methodReg));

        compiler.getStackManager().allocateVariableOffset();

    }

}
