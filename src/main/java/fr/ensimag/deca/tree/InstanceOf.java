package fr.ensimag.deca.tree;

import com.sun.tools.javac.resources.compiler;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

public class InstanceOf extends AbstractExpr implements BooleanExpression {
    private static final Logger LOG = Logger.getLogger(InstanceOf.class);

    private AbstractExpr expression;
    private AbstractIdentifier type;

    public InstanceOf(AbstractExpr expression, AbstractIdentifier type) {
        Validate.notNull(expression);
        Validate.notNull(type);
        this.expression = expression;
        this.type = type;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type type1 = this.expression.verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.type.verifyType(compiler);
        if (!(type1.isClassOrNull() && type2.isClass())) {
            throw new ContextualError("InstanceOf of non-class objects are not permitted (3.40)", getLocation());
        }
        SymbolTable symbolTable = new SymbolTable();
        SymbolTable.Symbol boolSymbol = symbolTable.create("boolean");
        BooleanType booleanType = new BooleanType(boolSymbol);
        setType(booleanType);
        return booleanType;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        LOG.debug("codeGenExpr InstanceOf : start");
        LabelManager labelManager = compiler.getLabelManager();
        int labelIndex = labelManager.useLabelCounter();

        Label labelFalse = new Label("instanceof." + labelIndex + ".false");
        Label labelTrue = new Label("instanceof." + labelIndex + ".true");
        Label labelFin = new Label("instanceof." + labelIndex + ".fin");
        GPRegister register = expression.codeGenExpr(compiler);
        register = InstanceOf.doAssembly(compiler, register, type.getClassDefinition(), labelTrue, labelFalse, labelIndex);
        // Evaluate return
        compiler.addLabel(labelTrue);
        compiler.addInstruction(new LOAD(1, register));
        compiler.addInstruction(new BRA(labelFin));
        compiler.addLabel(labelFalse);
        compiler.addInstruction(new LOAD(0, register));
        compiler.addLabel(labelFin);
        LOG.debug("codeGenExpr InstanceOf : end");
        return register;
    }



    @Override
    public void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel) {
        LOG.debug("codeBooleanExpression InstanceOf : start");
        LabelManager labelManager = compiler.getLabelManager();
        int labelIndex = labelManager.useLabelCounter();
        GPRegister register = expression.codeGenExpr(compiler);
        Label labelFalse = new Label("instanceof." + labelIndex + ".false");
        if (expectedValue)
            InstanceOf.doAssembly(compiler, register, type.getClassDefinition(), branchLabel, labelFalse, labelIndex);
        else
            InstanceOf.doAssembly(compiler, register, type.getClassDefinition(), labelFalse, branchLabel, labelIndex);
        compiler.addLabel(labelFalse);
        LOG.debug("codeBooleanExpression InstanceOf : end");
    }

    public static GPRegister doAssembly(DecacCompiler compiler, GPRegister register, ClassDefinition classDefinition, Label labelTrue, Label labelFalse, int labelIndex) {
        RegisterOffset registerOffset = compiler.getStackManager().getClassRegisterOffset(classDefinition);
        compiler.addInstruction(new LEA(registerOffset, Register.R1));
        if (registerOffset.getOffset() == 1) {
            compiler.addComment("instanceOf Object always return true");
            compiler.addInstruction(new BRA(labelTrue));
            return register;
        }
        Label labelBoucle = new Label("instanceof." + labelIndex + ".loop");
        Label labelCond = new Label("instanceof." + labelIndex + ".cond");
        compiler.addComment("Debut instanceof");
        compiler.addInstruction(new CMP(new NullOperand(), register));
        compiler.addInstruction(new BEQ(labelTrue));
        compiler.addInstruction(new LOAD(new RegisterOffset(0, register), register));
        compiler.addInstruction(new BRA(labelCond));

        // while ()
        compiler.addLabel(labelBoucle);
        compiler.addInstruction(new LOAD(Register.R0, register));
        compiler.addLabel(labelCond);
        compiler.addInstruction(new CMP(Register.R1, register));
        compiler.addInstruction(new BEQ(labelTrue));
        compiler.addInstruction(new LOAD(new RegisterOffset(0, register), Register.R0));
        compiler.addInstruction(new CMP(new NullOperand(), Register.R0));
        compiler.addInstruction(new BEQ(labelFalse));
        compiler.addInstruction(new BRA(labelBoucle));
        return register;
    }


    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        this.expression.decompile(s);
        s.print(" instanceof ");
        this.type.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
    	expression.prettyPrint(s, prefix, false);
        type.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.expression.iter(f);
        this.type.iter(f);
    }
}
