package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.Label;

public interface BooleanExpression {
    /**
     * Construit le flowchart correspondante à l'expression
     * @param compiler
     * @param registerManager
     * @param expectedValue
     * @param branchLabel
     */
    void codeBooleanExpression(DecacCompiler compiler, RegisterManager registerManager, boolean expectedValue, Label branchLabel);
}
