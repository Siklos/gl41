package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

public class New extends AbstractExpr {
    private AbstractIdentifier identifier;

    public New(AbstractIdentifier identifier) {
        Validate.notNull(identifier);
        this.identifier = identifier;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type type = this.identifier.verifyType(compiler);
        if (!type.isClass()) {
            throw new ContextualError("New class object is not defined as a class", getLocation());
        }
        setType(type);
        return type;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        ClassDefinition classDefinition = this.identifier.getClassDefinition();
        int nbFields = classDefinition.getNumberOfFields();
        compiler.addInstruction(new NEW(nbFields + 1, Register.R1));
        if (!compiler.isNoCheck())
            compiler.addInstruction(new BOV(new Label("heap_overflow")));
        RegisterOffset classRegisterOffset = compiler.getStackManager().getClassRegisterOffset(classDefinition);
        compiler.addInstruction(new LEA(classRegisterOffset, Register.R0));
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(0, Register.R1)));
        if (classDefinition.getNumberOfFields() > 0) {
            compiler.addInstruction(new PUSH(Register.R1, compiler.getStackManager()));
            compiler.addInstruction(new BSR(new Label("init." + identifier.getType().getName().getName())));
            compiler.addInstruction(new POP(Register.R1, compiler.getStackManager()));
        }
        return Register.R1;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("new ");
        this.identifier.decompile(s);
        s.print("()");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        identifier.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        this.identifier.iter(f);
    }
}
