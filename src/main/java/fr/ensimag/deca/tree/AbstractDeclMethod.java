package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

public abstract class AbstractDeclMethod extends Tree {
    protected abstract boolean verifyDeclMethodSignature(DecacCompiler compiler,
                                             EnvironmentExp localEnv, ClassDefinition superClass, int index)
            throws ContextualError;

    protected abstract void verifyDeclMethod(DecacCompiler compiler,
                                          EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    protected abstract void codeGenDeclMethod(DecacCompiler compiler);

    public abstract AbstractIdentifier getMethodName();
}
