package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;

import java.io.PrintStream;

import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.WFLOAT;
import fr.ensimag.ima.pseudocode.instructions.WINT;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import static java.lang.System.exit;

/**
 * Expression, i.e. anything that has a value.
 *
 * @author gl41
 * @date 01/01/2020
 */
public abstract class AbstractExpr extends AbstractInst {
    private static final Logger LOG = Logger.getLogger(Main.class);

    /**
     * @return true if the expression does not correspond to any concrete token
     * in the source code (and should be decompiled to the empty string).
     */
    boolean isImplicit() {
        return false;
    }

    /**
     * Get the type decoration associated to this expression (i.e. the type computed by contextual verification).
     */
    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        Validate.notNull(type);
        this.type = type;
    }
    private Type type;

    @Override
    protected void checkDecoration() {
        if (getType() == null) {
            throw new DecacInternalError("Expression " + decompile() + " has no Type decoration");
        }
    }

    /**
     * Verify the expression for contextual error.
     * 
     * implements non-terminals "expr" and "lvalue" 
     *    of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  (contains the "env_types" attribute)
     * @param localEnv
     *            Environment in which the expression should be checked
     *            (corresponds to the "env_exp" attribute)
     * @param currentClass
     *            Definition of the class containing the expression
     *            (corresponds to the "class" attribute)
     *             is null in the main bloc.
     * @return the Type of the expression
     *            (corresponds to the "type" attribute)
     */
    public abstract Type verifyExpr(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Verify the expression in right hand-side of (implicit) assignments 
     * 
     * implements non-terminal "rvalue" of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  contains the "env_types" attribute
     * @param localEnv corresponds to the "env_exp" attribute
     * @param currentClass corresponds to the "class" attribute
     * @param expectedType corresponds to the "type1" attribute            
     * @return this with an additional ConvFloat if needed...
     */
    public AbstractExpr verifyRValue(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass, 
            Type expectedType)
            throws ContextualError {
        LOG.trace("verify Rvalue : start");

        Type type2  = this.verifyExpr(compiler, localEnv, currentClass);
        if (compiler.getEnvironmentType().get(expectedType.getName()) == null) {
            throw new ContextualError("Expected type does not exist (3.28)", getLocation());
        }

        if (!type2.isNull() && compiler.getEnvironmentType().get(type2.getName()) == null) {
            throw new ContextualError("Type of assignation expression does not exist (3.28)", getLocation());
        }


        if (!expectedType.isAssignableFrom(type2)) {
            throw new ContextualError("Type of expression ("+ type2.getName().getName() + ") is not compatible with expected type ("+ expectedType.getName().getName() +") (3.28)", getLocation());
        }

        setType(type2);

        if (expectedType.isFloat()) {
            if (type2.isInt()) {
                ConvFloat convFloat = new ConvFloat(this);
                convFloat.verifyExpr(compiler, localEnv, currentClass);
                return convFloat;
            }
        }
        LOG.trace("verify Rvalue : end");
        return this;
    }
    
    
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {

        Type type = this.verifyExpr(compiler, localEnv, currentClass);
        this.setType(type);
    }

    /**
     * Verify the expression as a condition, i.e. check that the type is
     * boolean.
     *
     * @param localEnv
     *            Environment in which the condition should be checked.
     * @param currentClass
     *            Definition of the class containing the expression, or null in
     *            the main program.
     */
    void verifyCondition(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        Type type = this.verifyExpr(compiler, localEnv, currentClass);
        if(!type.isBoolean()) {
            throw new ContextualError("Condition is not a boolean expression (3.29)", getLocation());
        }
    }

    public boolean isIntLiteral() {
        return false;
    };

    public boolean isBooleanLiteral() {
        return false;
    };

    public boolean isFloatLiteral() {
        return false;
    };

    public boolean isIdentifier() {
        return false;
    }

    /**
     * Generate code to print the expression
     *
     * @param compiler
     */
    protected void codeGenPrint(DecacCompiler compiler) {

        GPRegister register = codeGenExpr(compiler);
        compiler.addInstruction(new LOAD(register, Register.R1));
        Type type = this.getType();
        if (type.isFloat()) {
            compiler.addInstruction(new WFLOAT());
        } else if (type.isInt()) {
            compiler.addInstruction(new WINT());
        } else {
            exit(1);
        }
    }

    protected void codeGenPrintx(DecacCompiler compiler) {
        codeGenPrint(compiler);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {

        this.codeGenExpr(compiler);
    }

    protected GPRegister codeGenExpr(DecacCompiler compiler) {
        RegisterManager registerManager = compiler.getRegisterManager();
        return this.codeGenExpr(compiler, registerManager);
    }

    protected abstract GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager);

    @Override
    protected void decompileInst(IndentPrintStream s) {
        decompile(s);
        s.print(";");
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Type t = getType();
        if (t != null) {
            s.print(prefix);
            s.print("type: ");
            s.print(t);
            s.println();
        }
    }
}
