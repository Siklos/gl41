package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.ImmediateFloat;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.DIV;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.QUO;

/**
 *
 * @author gl41
 * @date 01/01/2020
 */
public class Divide extends AbstractOpArith {
    public Divide(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }


    @Override
    protected GPRegister codeGenOperator(DecacCompiler compiler, GPRegister leftExprRegister, DVal rightExpr) {
        if (this.getLeftOperand().getType().isFloat() || this.getRightOperand().getType().isFloat()) {
            compiler.addInstruction(new DIV(rightExpr, leftExprRegister));
            if (!compiler.isNoCheck())
                compiler.addInstruction(new BOV(new Label("overflow")));
        } else {
            compiler.addInstruction(new QUO(rightExpr, leftExprRegister));
            if (!compiler.isNoCheck())
                compiler.addInstruction(new BOV(new Label("division_by_zero")));
        }
        return leftExprRegister;
    }

    @Override
    protected String getOperatorName() {
        return "/";
    }

}
