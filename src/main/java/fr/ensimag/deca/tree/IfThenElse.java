package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;

import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import org.apache.commons.lang.Validate;

/**
 * Full if/else if/else statement.
 *
 * @author gl41
 * @date 01/01/2020
 */
public class IfThenElse extends AbstractInst {
    
    private final AbstractExpr condition; 
    private final ListInst thenBranch;
    private ListInst elseBranch;

    public IfThenElse(AbstractExpr condition, ListInst thenBranch, ListInst elseBranch) {
        Validate.notNull(condition);
        Validate.notNull(thenBranch);
        Validate.notNull(elseBranch);
        this.condition = condition;
        this.thenBranch = thenBranch;
        this.elseBranch = elseBranch;
    }
    
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        this.condition.verifyCondition(compiler, localEnv, currentClass);
        this.thenBranch.verifyListInst(compiler, localEnv, currentClass, returnType);
        this.elseBranch.verifyListInst(compiler, localEnv, currentClass, returnType);
    }

    @Override
    public boolean isIfThenElse() {
        return true;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        LabelManager labelManager = compiler.getLabelManager();
        int idEndIf = labelManager.useLabelCounter();
        Label endLabel = new Label("E_End." + idEndIf);

        this.codeGenInstAux(compiler, endLabel);
        //at any given moment: if the elsebranch is empty: (no else) : make label to end, do condition branch to end, run code, branch straight to end, plonk the end label
            // if the elsebranch has something but not an iftehnelse: (else) : make a label to it, do condition branch to that, run code, branch to the end, then plonk the created label, then plonk the elsebranch's code, then plonk the end label
            // if the elsebranch has an ifthenelse: ([else] if) : make else label, do condition branch to else, run code, branch to end, plonk else label, run else code recursively;


    }

    protected void codeGenInstAux(DecacCompiler compiler, Label endLabel) {
        RegisterManager registerManager = compiler.getRegisterManager();
        LabelManager labelManager = compiler.getLabelManager();
        Label branchLabel;

        if (elseBranch.isEmpty()) { // si on finit sur un if
            branchLabel = endLabel;
        } else { // sinon il reste un else
            int idElse = labelManager.useLabelCounter();
            branchLabel = new Label("E_Else." + idElse);
        }
        ((BooleanExpression) condition).codeBooleanExpression(compiler, registerManager, false, branchLabel);
        thenBranch.codeGenListInst(compiler);
        if (elseBranch.isEmpty()) { // on finit sur un if (donc pas de else)
            compiler.addLabel(endLabel);
        } else { // ie: il reste un else
            compiler.addInstruction(new BRA(endLabel));
            compiler.addLabel(branchLabel);
            if ((!elseBranch.getList().get(0).isIfThenElse()) || (elseBranch.getList().size() > 1)) { // on finit sur un else (qui peut commencer par un if)
                elseBranch.codeGenListInst(compiler);
                compiler.addLabel(endLabel);
            } else {
                ((IfThenElse) elseBranch.getList().get(0)).codeGenInstAux(compiler, endLabel);
            }
        }

    }

    @Override
    public void decompile(IndentPrintStream s) {

    	s.print("if (");
        this.condition.decompile(s);
        s.println(") {");
        s.indent();
        this.thenBranch.decompile(s);
        s.unindent();
        s.print("}");
        s.println(" else {");
        s.indent();
        this.elseBranch.decompile(s);
        s.unindent();
        s.print("}");
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        condition.iter(f);
        thenBranch.iter(f);
        elseBranch.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        condition.prettyPrint(s, prefix, false);
        thenBranch.prettyPrint(s, prefix, false);
        elseBranch.prettyPrint(s, prefix, true);
    }
}
