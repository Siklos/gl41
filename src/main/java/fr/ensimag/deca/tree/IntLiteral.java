package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.instructions.WINT;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.*;


import java.io.PrintStream;

/**
 * Integer literal
 *
 * @author gl41
 * @date 01/01/2020
 */
public class IntLiteral extends AbstractExpr implements AbstractTerminalExpr {
    public int getValue() {
        return value;
    }

    private int value;

    public IntLiteral(int value) {
        this.value = value;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        SymbolTable fact = new SymbolTable();
        Type integer = new IntType(fact.create("int"));
        this.setType(integer);
        return integer;
    }

    @Override
    protected GPRegister codeGenExpr(DecacCompiler compiler, RegisterManager registerManager) {
        int index = registerManager.getCurrentTopRegister();
        compiler.addInstruction(new LOAD(this.getValue(), Register.getR(index)));
        return Register.getR(index);
    }

    @Override
    public DVal codeGenExprDVal(DecacCompiler compiler, RegisterManager registerManager) {
        return new ImmediateInteger(this.getValue());
    }

    @Override
    String prettyPrintNode() {
        return "Int (" + getValue() + ")";
    }
    
    @Override
    protected void codeGenPrint(DecacCompiler compiler) {
        compiler.addInstruction(new LOAD(getValue(), Register.R1));
        compiler.addInstruction(new WINT());
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(Integer.toString(value));
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
