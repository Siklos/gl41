/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunSoft, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice
 * is preserved.
 * ====================================================
 *
 */

class MathDeca{

    float
            PS0 = 0.16666666666666666f, // Long bits 0x3fc5555555555555L.
            PS1 = -0.3255658186224009f, // Long bits 0xbfd4d61203eb6f7dL.
            PS2 = 0.20121253213486293f, // Long bits 0x3fc9c1550e884455L.
            PS3 = -0.04005553450067941f, // Long bits 0xbfa48228b5688f3bL.
            PS4 = 7.915349942898145e-4f, // Long bits 0x3f49efe07501b288L.
            PS5 = 3.479331075960212e-5f, // Long bits 0x3f023de10dfdf709L.
            QS1 = -2.403394911734414f, // Long bits 0xc0033a271c8a2d4bL.
            QS2 = 2.0209457602335057f, // Long bits 0x40002ae59c598ac8L.
            QS3 = -0.6882839716054533f, // Long bits 0xbfe6066c1b8d0159L.
            QS4 = 0.07703815055590194f; // Long bits 0x3fb3b8c5b12e9282L.


    float
            PI_L = 1.2246467991473532e-16f, // Long bits 0x3ca1a62633145c07L
            PIO2_1 = (float)1.5707963267341256, // Long bits 0x3ff921fb54400000L.
            PIO2_1L = (float)6.077100506506192e-11, // Long bits 0x3dd0b4611a626331L.
            PIO2_2 = (float)6.077100506303966e-11, // Long bits 0x3dd0b4611a600000L.
            PIO2_2L = (float)2.0222662487959506e-21, // Long bits 0x3ba3198a2e037073L.
            PIO2_3 = (float)2.0222662487111665e-21, // Long bits 0x3ba3198a2e000000L.
            PIO2_3L = (float)8.4784276603689e-32; // Long bits 0x397b839a252049c1L.

    float
            S1 = (float)-0.16666666666666632, // Long bits 0xbfc5555555555549L.
            S2 = (float)8.33333333332249e-3, // Long bits 0x3f8111111110f8a6L.
            S3 = (float)-1.984126982985795e-4, // Long bits 0xbf2a01a019c161d5L.
            S4 = (float)2.7557313707070068e-6, // Long bits 0x3ec71de357b1fe7dL.
            S5 = (float)-2.5050760253406863e-8, // Long bits 0xbe5ae5e68a2b9cebL.
            S6 = (float)1.58969099521155e-10; // Long bits 0x3de5d93a5acfd57cL.

    float
            C1 = (float)0.0416666666666666, // Long bits 0x3fa555555555554cL.
            C2 = (float)-1.388888888887411e-3, // Long bits 0xbf56c16c16c15177L.
            C3 = (float)2.480158728947673e-5, // Long bits 0x3efa01a019cb1590L.
            C4 = (float)-2.7557314351390663e-7, // Long bits 0xbe927e4f809c52adL.
            C5 = (float)2.087572321298175e-9, // Long bits 0x3e21ee9ebdb4b1c4L.
            C6 = (float)-1.1359647557788195e-11; // Long bits 0xbda8fae9be8838d4L.

    float PI = (float)3.1415926;

    float abs(float f){
        if (f<0) return -f;
        return f;
    }

    //Return the square root of a given float argument
    float sqrt(float f){
        float x0 = 1;
        int  i = 0;
        while (i < 1000){
            x0 = (float)0.5*(x0 + f/x0);
            i = i + 1;
        }
        return x0;
    }
    //Returns the value of a float argument to the exponent
    float ulp(float f) {
        if (f < 0) f =-f;

        if (f == 0) return(0);
        else{
            int precision = 24;
            int exponent= 0;
            while (1 == 1){
                if (power(2.f,exponent) <= f && power(2.f,exponent + 1) > f){
                    return power(2,(exponent - precision + 1));
                }
                else if (power(2.f,exponent + 1) > f){
                    exponent = exponent - 1;
                }
                else if (power(2.f,exponent) < f){
                    exponent = exponent + 1;
                }
            }
        }
    }

    //Returns the value of a float argument to the exponent
    float power(float nbr, int exponent){
        int sign = 0;
        float result = 1;
        if (exponent == 0) {return 1;}
        else if (nbr == 0) {return 0;}
        else{
            if (exponent < 0 ) {
                sign = 1;
                exponent = (-1) * exponent;
            }

            while (exponent > 0){
                result = result * nbr;
                exponent = exponent - 1;
            }
            if (sign == 1){
                result = 1/result;
            }
            return(result);
        }
    }

    protected int remPiOver2n(float x, float y0, float y1) {
        int sign = 1;
        float z;
        int n = 0;
        float w ;
        float t;

        if (x<0){
            x = -x;
            sign = -1;
        }

        if (x < 3 * PI / 4) // If |x| is small.
        {
            z = x - PIO2_1;
            if ( x !=  (PI / 2)) // 33+53 bit pi is good enough.
            {
                y0 = z - PIO2_1L;
                y1 = z - y0 - PIO2_1L;
            }
            else // Near pi/2, use 33+33+53 bit pi.
            {
                z -= PIO2_2;
                y0 = z - PIO2_2L;
                y1 = z - y0 - PIO2_2L;
            }
            n = 1;
        }
        else
        {
            n = (int) (2 / PI * x + 0.5);
            z = x - n * PIO2_1;
            w = n * PIO2_1L; // First round good to 85 bits.
            y0 = z - w;
            if (n >= 32 ||  x ==  (w))
            {
                if (x / y0 >= power(2.f,16)) // Second iteration, good to 118 bits.
                {
                    t = z;
                    w = n * PIO2_2;
                    z = t - w;
                    w = n * PIO2_2L - (t - z - w);
                    y0 = z - w;
                    if (x / y0 >= power(2.f,49)) // Third iteration, 151 bits accuracy.
                    {
                        t = z;
                        w = n * PIO2_3;
                        z = t - w;
                        w = n * PIO2_3L - (t - z - w);
                        y0 = z - w;
                    }
                }
            }
            y1 = z - y0 - w;
        }


        if (sign == -1){
            y0 = -y0;
            y1 = -y1;
            n = -n;
        }
        return n;
    }

    protected float remPiOver2y0(float x, float y0, float y1) {
        int sign = 1;
        float z;
        int n = 0;
        float w ;
        float t;

        if (x<0){
            x = -x;
            sign = -1;
        }

        if (x < 3 * PI / 4) // If |x| is small.
        {
            z = x - PIO2_1;
            if ( x !=  (PI / 2)) // 33+53 bit pi is good enough.
            {
                y0 = z - PIO2_1L;
                y1 = z - y0 - PIO2_1L;
            }
            else // Near pi/2, use 33+33+53 bit pi.
            {
                z -= PIO2_2;
                y0 = z - PIO2_2L;
                y1 = z - y0 - PIO2_2L;
            }
            n = 1;
        }
        else
        {
            n = (int) (2 / PI * x + 0.5);
            z = x - n * PIO2_1;
            w = n * PIO2_1L; // First round good to 85 bits.
            y0 = z - w;
            if (n >= 32 ||  x ==  (w))
            {
                if (x / y0 >= power(2.f,16)) // Second iteration, good to 118 bits.
                {
                    t = z;
                    w = n * PIO2_2;
                    z = t - w;
                    w = n * PIO2_2L - (t - z - w);
                    y0 = z - w;
                    if (x / y0 >= power(2.f,49)) // Third iteration, 151 bits accuracy.
                    {
                        t = z;
                        w = n * PIO2_3;
                        z = t - w;
                        w = n * PIO2_3L - (t - z - w);
                        y0 = z - w;
                    }
                }
            }
            y1 = z - y0 - w;
        }

        if (sign == -1){
            y0 = -y0;
            y1 = -y1;
            n = -n;
        }
        return y0;
    }

    protected float remPiOver2y1(float x, float y0, float y1) {
        int sign = 1;
        float z;
        int n = 0;
        float w ;
        float t;

        if (x<0){
            x = -x;
            sign = -1;
        }

        if (x < 3 * PI / 4) // If |x| is small.
        {
            z = x - PIO2_1;
            if ( x !=  (PI / 2)) // 33+53 bit pi is good enough.
            {
                y0 = z - PIO2_1L;
                y1 = z - y0 - PIO2_1L;
            }
            else // Near pi/2, use 33+33+53 bit pi.
            {
                z -= PIO2_2;
                y0 = z - PIO2_2L;
                y1 = z - y0 - PIO2_2L;
            }
            n = 1;
        }
        else if (x <= power(2.f, 19) * PI / 2) // Medium size.
        {
            n = (int) (2 / PI * x + 0.5);
            z = x - n * PIO2_1;
            w = n * PIO2_1L; // First round good to 85 bits.
            y0 = z - w;
            if (n >= 32 ||  x ==  (w))
            {
                if (x / y0 >= power(2.f,16)) // Second iteration, good to 118 bits.
                {
                    t = z;
                    w = n * PIO2_2;
                    z = t - w;
                    w = n * PIO2_2L - (t - z - w);
                    y0 = z - w;
                    if (x / y0 >= power(2.f,49)) // Third iteration, 151 bits accuracy.
                    {
                        t = z;
                        w = n * PIO2_3;
                        z = t - w;
                        w = n * PIO2_3L - (t - z - w);
                        y0 = z - w;
                    }
                }
            }
            y1 = z - y0 - w;
        }

        else {
            n = (int) (2 / PI * x + 0.5);
            z = x - n * PIO2_2;
            w = n * PIO2_2L; // First round good to 85 bits.
            y0 = z - w;
            if (n >= 32 ||  x ==  (w))
            {
                if (x / y0 >= power(2.f,16)) // Second iteration, good to 118 bits.
                {
                    t = z;
                    w = n * PIO2_3;
                    z = t - w;
                    w = n * PIO2_3L - (t - z - w);
                    y0 = z - w;

                }
            }
            y1 = z - y0 - w;
        }

        if (sign == -1){
            y0 = -y0;
            y1 = -y1;
            n = -n;
        }
        return y1;
    }

    public float sin(float a)
    {

        float y0 = 0.f;
        float y1 = 0.f;
        int n;
        if (abs(a) <= PI / 4)
            return _sin(a, 0);

        n = remPiOver2n(a, y0, y1);
        y1 = remPiOver2y1(a, y0, y1);
        y0 = remPiOver2y0(a, y0, y1);
        int k = n%4;
        {
            if (k == 0)
                return _sin(y0, y1);
            else if (k == 1)
                return _cos(y0, y1);
            else if (k == 2)
                return -_sin(y0, y1);
            else
                return -_cos(y0, y1);
        }
    }

    public float cos(float a)
    {
        float y0 = 0.f;
        float y1 = 0.f;
        int n;
        int k;
        if (abs(a) <= PI / 4)
            return _cos(a, 0);


        n = remPiOver2n(a, y0, y1);
        y1 = remPiOver2y1(a, y0, y1);
        y0 = remPiOver2y0(a, y0, y1);
        k = n & 3;
        if (k == 0){
            return _cos(y0, y1);}
        else if (k == 1){
            return -_sin(y0, y1);}
        else if (k == 2){
            return -_cos(y0, y1);}
        else{
            return _sin(y0, y1);}
    }

    float asin(float x) {
        boolean negative = x < 0.f;
        if (negative)
             x = -x;
        if (x == 1)
            return negative ? -PI / 2.f : PI / 2.f;
        if (x < 0.5f)
        {
            if (x < 1.f / power(2.f,27))
                return negative ? -x : x;
            float t = x * x;
            float p = t * (PS0 + t * (PS1 + t * (PS2 + t * (PS3 + t
                                                                     * (PS4 + t * PS5)))));
            float q = 1.f + t * (QS1 + t * (QS2 + t * (QS3 + t * QS4)));
            return negative ? -x - x * (p / q) : x + x * (p / q);
        }
        float w = 1.f - x; // 1>|x|>=0.5.
        float t = w * 0.5f;
        float p = t * (PS0 + t * (PS1 + t * (PS2 + t * (PS3 + t
                                                            * (PS4 + t * PS5)))));
        float q = 1.f + t * (QS1 + t * (QS2 + t * (QS3 + t * QS4)));
        float s = sqrt(t);
        if (x >= 0.975f)
        {
            w = p / q;
            t = PI / 2.f - (2.f * (s + s * w) - PI_L / 2.f);
        }
        else
        {
            w = (float) s;
            float c = (t - w * w) / (s + w);
            p = 2.f * s * (p / q) - (PI_L / 2.f - 2.f * c);
            q = PI / 4.f - 2.f * w;
            t = PI / 4.f - (p - q);
        }
        return negative ? -t : t;
    }

    //We use the fact that atan(x) = asin(x/sqrt(1+x^2)
    float atan(float f) {
        return asin(f /sqrt(1 + f * f));
    }

    float _sin(float x, float y)
    {
        float z;
        float v;
        float r;
        if (abs(x) < 1 / power(2.f,27))
            return x;  // If |x| ~< 2**-27, already know answer.

        z = x * x;
        v = z * x;
        r = S2 + z * (S3 + z * (S4 + z * (S5 + z * S6)));
        if (y == 0)
            return x + v * (S1 + z * r);
        return x - ((z * ((float)0.5 * y - v * r) - y) - v * S1);
    }

    float _cos(float x, float y)
    {
        float z;
        float r;
        x = abs(x);
        if (x < 1 / power(2.f,27))
            return 1;  // If |x| ~< 2**-27, already know answer.

        z = x * x;
        r = z * (C1 + z * (C2 + z * (C3 + z * (C4 + z * (C5 + z * C6)))));

        if (x < 0.3)
            return 1.f - ((float)0.5 * z - (z * r - x * y));

        float qx = (x > (float)0.78125) ? (float)0.28125 : (x * (float)0.25);
        return 1.f - qx - (((float)0.5 * z - qx) - (z * r - x * y));
    }
}
