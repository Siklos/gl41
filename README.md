# Projet Génie Logiciel, Ensimag.

Equipe gl41, le 01/01/2020.

Ce projet à pour but de développer un compilateur et son langage de programmation orienté objet appelé déca.
Il compile le code pour en assembly pour un processeur MIPS avec d'être encore compilé en langage code.

Ce projet met donc en place 3 parties principales :
- Un lexer
- Un analyseur contextuel (aussi dit parser)
- Un générateur de code

Le projet est développé sous Java avec Maven pour la gestion des options de génération.

Le processus d'intégration continue se fait via gitlab-ci.


#  À propos de tests.

La base des tests ".deca" développés pour les trois parties A, B, C contient plus 555 tests se trouvant dans les répertoires: 

- `./src/test/deca/codegen/{invalid, valid}/{developed, interactive}/{noObject, Object}`
- `./src/test/deca/context/{invalid, valid}/developed/{noObject, Object}`
- `./src/test/deca/syntax/{invalid, valid}/{developed}/{Lexer, Parser}/{noObject, Object}`


Plus de 200 tests unitaires étaient développés durant le projet se trouvent dans les différents sous répertoires du répertoire: 

- /home/ensimag/GL/gl41/src/test/java/fr/ensimag. Ces tests viennent évaluer en plus des tests précedents la couverture de notre code.


# Exécution des tests automatisés.

 le fichier : 
 - `./src/test/script/all_test.sh`, est un script shell qui se base sur 6 scripts shell, 
 pour éxecuter tous les tests développés de syntax lexer/parser, de context et de l'option de décompilation.
 - `./src/test/script/execute-tests-codegen.py`, est script python qui vérifient l'exécution de decac 
 et ima pour comparer les résultats trouvés avec ceux attendus et qui sont stockés dans le fichier json 
 `expectedOutputs.json` de chaque dossier de test de génération de code
 (soit `./src/test/deca/codegen/{invalid, valid}/{developed, interactive}/{noObject, Object}`)

# Couverture avec COBERTURA

 Pour visualiser la couverture de notre code, veuillez s'il vous plait suivre les instructions suivantes:
 
```shell script
mvn compile
mvn cobertura:cobertura
mvn cobertura:instrument
```
Lancez tous les scripts automatisés à l'aide de la partie précedente :
```shell script
./all_test.sh
```
et
```shell script
python3 ./execute-tests-codegen.py
```
Puis pour générer le rapport, lancez le script :  
```shell script
./src/test/script/cobertura-report.sh
```
Ça devrait avoir généré dans le dossier `./target/site/cobertura`
Ouvrez ensuite le fichier `index.html` avec votre navigateur préféré (ex : `firefox index.html`)
 
Un rapport en images est aussi disponible dans le répertoire docs.

 
 
 
